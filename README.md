# MSC-DPOD Simulator

The MSC-DPOD Simulator is a collection of Docker containers that simulate a complete live runtime environment.

The simulator deploys all required configuration, services and applications and invokes demo transactions, so you can experience and demonstrate the full power and value of IBM(C) DataPower(R) Operations Dashboard.


* **Version:** 1.0
* **Source:** [MSC-DPOD-Simulator](https://bitbucket.org/montier/msc-dpod-simulator)
* **License:** [Artistic License 2.0](LICENSE.md)
* **Copyright:** Copyright (C) 2015-2018 by [MonTier Software (2015) LTD](http://www.mon-tier.com/)


# Demonstration scenarios

The MSC-DPOD Simulator supports several scenarios to demonstrate some of the most valuable features. If you want to demonstrate DPOD or to learn by example, use the link below:

**[Demonstration Scenarios](./stable/scenarios/ScenariosList.md)**


# Included containers

* IBM(C) DataPower(R) Operations Dashboard Docker container
	* This container runs a DPOD Light Edition with a limited time license.
* IBM(C) DataPower(R) Gateway Docker container(s)
	* These containers run IDG with different types of services configured. They are automatically added to the DPOD instance as monitored devices.
* Splunk(R) Enterprise Docker container
	* This container runs Splunk Enterprise with DPOD Splunk App installed, which demonstrates DPOD's integration with APM/Splunk.
* IBM(C) MQ Docker container
	* This container runs a QM with a few local queues used by the services deployed on the IDG instance.
* CentOS Docker container
	* This container runs a CentOS with the simulator's tools that deploy, configure and generate transactions on the IDG instance(s).

All containers are configured using a docker-compose.yml file. There is no need to change this file.


# Prerequisites

* A laptop / workstation with the following requierments:
	* For mac laptops make sure your docker assigned with 8 cores and 16GB of Memory
	* For Intel based workstation minimum requirements is an Intel i7 CPU with 12GB memory
	* You will need at least 25 GB of free disk.
* CentOS/RHEL/Ubuntu/MacOS Operating System. You may use VMWare to create a CentOS/RHEL/Ubuntu virtual machine for running the simulator.
	* We test this tool on VMWare(R) Workstation 14+
* The laptop/VM date and time must be synchronized (it is **Mandatory** to use/install an NTP service).
	* For CentOS/RHEL you can run the following commands
		- `sudo yum -q -y install ntp`
		- `sudo systemctl enable ntpd`
		- `sudo systemctl start ntpd`
	* For Ubuntu you can run the following commands
		- `sudo apt-get -y install ntp`
		- `sudo systemctl enable ntp`
		- `sudo systemctl start ntp`
* Make sure the following packages exists to support installation
	* For CentOS/RHEL you can run the following commands
        - `sudo yum -q -y install curl`
	* For Ubuntu you can run the following commands
        - `sudo apt-get -y install curl`

# Downloading and setting up Docker environment

## __CentOS / RHEL / Ubuntu__

The following commands will download the simulator, install Docker and configure the environment. For more details, see the relevant section below.

Copy the command line below and paste it in your SSH terminal (you may need to enter ap password for the sudo command):
```bash
sudo rm -fR ~/msc-dpod-simulator
```

Copy the command line below and paste it in your SSH terminal:
```bash
mkdir -p ~/msc-dpod-simulator/src/msc-dpod-simulator ~/msc-dpod-simulator/resources
cd ~/msc-dpod-simulator/src/msc-dpod-simulator
curl -s -L -o msc-dpod-simulator.tgz https://bitbucket.org/montier/msc-dpod-simulator/get/master.tar.gz
tar -xzf msc-dpod-simulator.tgz -C . --strip-components=1
cd ~/msc-dpod-simulator
find . -name '*.sh' -type f | xargs chmod +x
sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/setupDockerEnv.sh
```

### List of changes made by the script setupDockerEnv.sh

The script installs all prerequisites for Docker and the simulator:

* Installs yum-utils, curl, wget
* Creates docker-ce repository and installs docker-ce
* Starts Docker service
* Configures Docker service to autostart at boot time
* Installs docker-compose
* Installs iptables-services and configures it to support the simulator

## __MacOS__

Docker should be installed and started by following these [instructions](https://docs.docker.com/docker-for-mac/install/).

After Docker is installed, copy the command lines below and paste it in your SSH terminal:
```bash
sudo rm -fR ~/msc-dpod-simulator
mkdir -p ~/msc-dpod-simulator/src/msc-dpod-simulator ~/msc-dpod-simulator/resources
cd ~/msc-dpod-simulator/src/msc-dpod-simulator
curl -s -L -o msc-dpod-simulator.tgz https://bitbucket.org/montier/msc-dpod-simulator/get/master.tar.gz
tar -xzf msc-dpod-simulator.tgz -C . --strip-components=1
cd ~/msc-dpod-simulator
find . -name '*.sh' -type f | xargs chmod +x
```

# Building the Docker containers

| Environment | Commmand |
| --- | --- |
| **DPOD + 1 IDG** | `sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/dpodSimulator.sh -a build` |
| **DPOD + 2 IDG** | `sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/dpodSimulator.sh -a build -i` |
| **DPOD + 1 IDG + Splunk** | `sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/dpodSimulator.sh -a build -k` |
| **DPOD + 2 IDG + Splunk** | `sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/dpodSimulator.sh -a build -i -k` |

**Once the operation is complete, the simulator environment is up and running!**

This operation:

* Cleans existing simulators builds
* Downloads the latest images and builds local images
* Starts the containers
* Adds the IDG device(s) to DPOD
* Runs the CentOS commands that simulate traffic on IDG and DPOD

This operation may take a while to complete.
Make sure that the SSH session does not get disconnected during the command execution.

# Stopping the simulator

```bash
sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/dpodSimulator.sh -a stop
```

This operation stops all containers, but keeps the images ready for rerunning.

# Starting the simulator (after it has been stopped)

```bash
sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/dpodSimulator.sh -a start
```

This operation:

* Starts all containers
* Runs the CentOS commands that simulate traffic on IDG and DPOD
* (This is a subset of the 'build' operation)

The services that will be started are the ones that were built during the 'build' action.
For examples, if the '-k' flag was raised in the 'build' action, the splunk service that was built, will be started.

# Cleaning the simulator environment

```bash
sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/dpodSimulator.sh -a clean
```

This operation:

* Stops and removes all simulator docker containers
* Removes all simulator docker images
* Removes all simulator docker networks

Note that the build operation also activates the 'clean' operation before starting the 'build'.

# Login to DPOD / IDG / Splunk

After the simulator is up and running, you may login using the following table:

| Service | URL / Port | User Name | Password |
| --- | --- | --- | --- |
| DPOD Web | https://<VM/Local_IP_Address>:4433/op | admin | adminuser |
| IDG1 Web | https://<VM/Local_IP_Address>:9090 | admin | admin |
| IDG2 Web | https://<VM/Local_IP_Address>:9091 | admin | admin |
| Splunk Web | http://<VM/Local_IP_Address>:8000  | -  | - |
| DPOD SSH | <VM/Local_IP_Address>:4022 | root | dpod |
| IDG1 SSH | <VM/Local_IP_Address>:7022 | admin | admin |

# Logs

All scripts log files are located under ~/msc-dpod-simulator/resources/stable/logs directory.

# Upgrading simulator version

To upgrade to the latest simulator version, run :

```bash
sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/dpodSimulator.sh -a upgrade
```

This operation:

* Removes the ~/msc-dpod-simulator/src/msc-dpod-simulator directory
* Recreates the directories and downloads the latest version from the Web.

# Limitations

* Since the IDG in the simulation is an IBM(C) DataPower(R) Gateway Docker container, DPOD dashboards will not display the IDG memory details.
* Since the simulator does not include an API-Connect server, the API transactions are imitated. Therefore, memory and latency shown in the 'Transaction Analysis' will not match the time that the transaction imitation took.


# Troubleshooting build warning

In some cases, the 'build' operation might complete with a warning:
```
2018-05-06_20-27-44: WARN: Device REST call response does not contain a success indication. You may need to add device to dpod manually.
```

This means that DPOD and IDG are up, but the process of adding the IDG device to DPOD took a long time, and may have not completed.

To check if the IDG device was actually added successfully to DPOD, login to DPOD Web Console and check if the device appears under "Manage -> Monitored Devices".
If it does not, add it manually, using the standard process documented in [DPOD admin guide](https://montier.atlassian.net/wiki/spaces/DPOD010704/pages/53999193/Device+Management).
The parameters for manual device addition:

* Name: The IDG device name is one of the mandatory parameters when adding a device to DPOD.

	In a docker container environment, the IDG device name will always be the ID of the IDG container.
	This ID will change every time the simulator is built, since every build creates a new container.

	To get the id of the "idg1" container, run the command:

		docker ps | grep idg1 | awk '{print $1}'

* Host Address: 172.77.77.3
* SOMA Port: 5550
* SOMA User Name: admin
* SOMA Password: admin
* Log Target Source Address: 172.77.77.3

If you chose to create two IDG services, and the WARN is regarding the "idg2" service:

	Get the id of the "idg2" container by running:
		docker ps | grep idg2 | awk '{print $1}'

* Host Address: 172.77.77.6
* SOMA Port: 5550
* SOMA User Name: admin
* SOMA Password: admin
* Log Target Source Address: 172.77.7.6


# Splunk alert on license expiration

Splunk Free license allows indexing up to 500 MB/day.

After 3 warnings on a Free license in a rolling period of 30 days, you are in violation of your license and search is disabled.

In such a case you can either rebuild your environment, or remove all the data that had been indexed this far by executing within the Splunk container the following commands:
```
/opt/splunk/bin/splunk stop
/opt/splunk/bin/splunk clean eventdata -f
/opt/splunk/bin/splunk start
```

# Changing IDG/DPOD docker image version

* To change the version of the DataPower image that the simulator will use, just change the tag environment variable assignment in the dpodSimulator.sh code:
	Look for the line "export IDG2_IMAGE_TAG=" and edit the value (be sure to check that the new tag actually exists in ibmcom/datapower repository).


# Pulling from the Git repository

In case you have been provided with special instructions to pull the Git repository instead of downloading the latest repository, use the following commands :

## __CentOS / RHEL / Ubuntu__

Copy the command lines below and paste it in your SSH terminal (you may need to enter ap password for the sudo command):
```bash
sudo which yum > /dev/null 2>&1 && sudo yum -q -y install yum-plugin-ovl git
sudo which apt-get > /dev/null 2>&1 && sudo apt-get install -y git
```

Copy the command lines below and paste it in your SSH terminal:
```bash
sudo rm -fR ~/msc-dpod-simulator
mkdir -p ~/msc-dpod-simulator/src  ~/msc-dpod-simulator/resources
cd ~/msc-dpod-simulator/src
git clone https://bitbucket.org/montier/msc-dpod-simulator.git
cd ~/msc-dpod-simulator
find . -name '*.sh' -type f | xargs chmod +x
sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/setupDockerEnv.sh
```

## __MacOS__

Docker should be installed and started by following these [instructions](https://docs.docker.com/docker-for-mac/install/).

Git is part of Command Line Developer Tools. Command Line Developer Tools installation will be initiated if needed using the following command line:
```bash
git
```

After Git and Docker are installed, copy the command lines below and paste it in your SSH terminal:
```bash
sudo rm -fR ~/msc-dpod-simulator
mkdir -p ~/msc-dpod-simulator/src  ~/msc-dpod-simulator/resources
cd ~/msc-dpod-simulator/src
git clone https://bitbucket.org/montier/msc-dpod-simulator.git
cd ~/msc-dpod-simulator
find . -name '*.sh' -type f | xargs chmod +x
```
