#!/bin/bash

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


 
#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -a|--address -p|--port -u|--uri -s|--soap-action -c|--content-type -d|--data-file -m|--http-method -t|--transport-protocol -o|--scenario"
	echo "	Or :  $0 -h|--help"
	echo " "
	echo " "
	echo "-h|--help              : Display help                      "	
	echo " "
	echo "	For Example: "
	echo "			$0 -a 172.77.77.3 -p 1080 -u \"/ProxyToWrongCipher/Service.asmx\" -c \"text/xml\" -m POST -t http -d \"DataPowerPing_request.xml\" -o wrongCipherSpec "
	echo " "
	return 0
}


###########################
# variables and constants #
###########################


NOW=$( date +"%F_%H-%M-%S")

LOCAL_FILES_DIR=`pwd`

LOG_DIR="${LOCAL_FILES_DIR}/logs"
LOG_FILE_NAME="EarlyCompClient_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

GENERSTED_FILES_DIR="${LOCAL_FILES_DIR}/GeneratedFiles"

SOAP_ACTION_FLAG_UP=0
DATA_FLAG_UP=0


#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-a|--address)	
			ADDRESS=$2
			if [[ -z ${ADDRESS} ]]; then
				echo "ERROR : Invalid parameters. Address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-p|--port)
			PORT=$2
			if [[ -z ${PORT} ]]; then
				echo "ERROR : Invalid parameters. Port is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-u|--uri)	
			URI=$2
			if [[ -z ${URI} ]]; then
				echo "ERROR : Invalid parameters. URI proxy is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-s|--soap-action)
			SOAP_ACTION_FLAG_UP=1
			SOAP_ACTION=$2
			if [[ -z ${SOAP_ACTION} ]]; then
				echo "ERROR : Invalid parameters. SOAPAction is missing"
				show_help
				exit 1
			fi
			shift 2
			;;     
		-c|--content-type)
			CONTENT_TYPE=$2
			if [[ -z ${CONTENT_TYPE} ]]; then
				echo "ERROR : Invalid parameters. Content-Type is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-d|--data-file)
			DATA_FLAG_UP=1
			DATA_FILE=$2
			if [[ -z ${DATA_FILE} ]]; then
				echo "ERROR : Invalid parameters. Data file is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-m|--http-method)
			HTTP_METHOD=$2
			if [[ -z ${HTTP_METHOD} ]]; then
				echo "ERROR : Invalid parameters. HTTP method is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-t|--transport-protocol)
			PROTOCOL=$2
			if [[ -z ${PROTOCOL} ]]; then
				echo "ERROR : Invalid parameters. Protocol is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-o|--scenario)	
			SCENARIO=$2
			if [[ -z ${SCENARIO} ]]; then
				echo "ERROR : Invalid parameters. Scenario is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


if [[ -z ${ADDRESS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--address' is missing "
	exit 2
fi

if [[ -z ${PORT} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--port' is missing "
	exit 2
fi

if [[ -z ${URI} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--uri' is missing "
	exit 2
fi

if [[ -z ${CONTENT_TYPE} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--content-type' is missing "
	exit 2
fi

if [[ -z ${HTTP_METHOD} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--http-method' is missing "
	exit 2
fi

if [[ -z ${PROTOCOL} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--transport-protocol' is missing "
	exit 2
fi

if [[ -z ${SCENARIO} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--scenario' is missing "
	exit 2
fi

############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}
mkdir -p ${GENERSTED_FILES_DIR}
  
CURL_RESPONSE="${GENERSTED_FILES_DIR}/CURL_${ADDRESS}_${SCENARIO}_Response.xml"
  
if [[ ${SOAP_ACTION_FLAG_UP} -eq 1 ]]; then
	SOAP_ACTION_HEADER="-H \"SOAPAction: ${SOAP_ACTION}\""
else
	SOAP_ACTION_HEADER=""
fi   
    
if [[ ${DATA_FLAG_UP} -eq 1 ]]; then
	curl -s --user ${USER}:${PASS} -H "Content-Type: ${CONTENT_TYPE}" ${SOAP_ACTION_HEADER} -X ${HTTP_METHOD} -d "@${DATA_FILE}" --insecure -w "\nresultCode:%{http_code}\n" ${PROTOCOL}://${ADDRESS}:${PORT}${URI} > ${CURL_RESPONSE} 2>> ${LOG_FILE}	
else
	curl -s --user ${USER}:${PASS} -H "Content-Type: ${CONTENT_TYPE}" ${SOAP_ACTION_HEADER} -X ${HTTP_METHOD} --insecure -w "\nresultCode:%{http_code}\n" ${PROTOCOL}://${ADDRESS}:${PORT}${URI} > ${CURL_RESPONSE} 2>> ${LOG_FILE}	
fi	

# Check for success indication in response
# SUCCESS_INDICATION=`grep "resultCode:200" ${CURL_RESPONSE} | wc -l`
# if [[ ${SUCCESS_INDICATION} -eq 0 ]]; then
#fi
