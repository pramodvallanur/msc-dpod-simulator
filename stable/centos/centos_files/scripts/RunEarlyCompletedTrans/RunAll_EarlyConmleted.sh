#!/bin/sh

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 

#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -a|--address"
	echo "	Or :  $0 -h|--help"
	echo " "
	echo "-a|--address      : The address of the idg. This is a mandatory parameter."
	echo "	For Example: "
	echo "			$0 -a 172.77.77.3"
	echo " "
	return 0
}

###########################
# variables and constants #
###########################


NOW=$( date +"%F_%H-%M-%S")

LOCAL_FILES_DIR=`pwd`

LOG_DIR="${LOCAL_FILES_DIR}/logs"
LOG_FILE_NAME="RunAll_EarlyConmleted_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}
SLEEP_TIME_SCENARIO=10
SLEEP_TIME_CYCLE=60

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-a|--address)	
			DP_ADDRESS=$2
			if [[ -z ${DP_ADDRESS} ]]; then
				echo "ERROR : Invalid parameters. IDG address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


if [[ -z ${DP_ADDRESS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--address' is missing "
	exit 2
fi


############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}

while true
do
	# wrong CypherSpec
	./EarlyCompClient.sh -a ${DP_ADDRESS} -p 1080 -u "/ProxyToWrongCipher/Service.asmx" -c "text/xml" -m POST -t http -d "DataPowerPing_request.xml" -o wrongCypherSpec
	sleep ${SLEEP_TIME_SCENARIO}
	# wrong SSL version  
	./EarlyCompClient.sh -a ${DP_ADDRESS} -p 1080 -u "/ProxyToWrongSSLVersion/Service.asmx" -c "text/xml" -m POST -t http -d "DataPowerPing_request.xml" -o wrongSSLversion
	sleep ${SLEEP_TIME_SCENARIO}
	# wrong URI
	./EarlyCompClient.sh -a ${DP_ADDRESS} -p 1080 -u "/wrongURI/Service.asmx" -c "text/xml" -m POST -t http -d "DataPowerPing_request.xml" -o wrongURI
	sleep ${SLEEP_TIME_SCENARIO}
	# no ClientCert
	./EarlyCompClient.sh -a ${DP_ADDRESS} -p 10443 -u "/EarlySSLFailures/Service.asmx" -c "text/xml" -m POST -t https -d "DataPowerPing_request.xml" -o noClientCert
	sleep ${SLEEP_TIME_SCENARIO}
	# non-SSL call
	./EarlyCompClient.sh -a ${DP_ADDRESS} -p 10443 -u "/EarlySSLFailures/Service.asmx" -c "text/xml" -m POST -t http -d "DataPowerPing_request.xml" -o nonSSLcall
	sleep ${SLEEP_TIME_CYCLE}
	# APIC non-SSL call
	#./EarlyCompClient.sh -a ${DP_ADDRESS} -p 10443 -u "/montierorg/montiercatalog/AccountService" -c "text/xml" -m POST -t http -d "DataPowerPing_request.xml" -o APICnonSSLcall
	#sleep 10
done
