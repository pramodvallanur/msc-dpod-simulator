#!/bin/sh


#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


 
#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -a|--dp-address -p|--soma-port -u|--soma-user -s|--soma-pass "
	echo "	Or :  $0 -h|--help"
	echo ""
	echo "-a|--dp-address			: Address of the idg. This is a mandatory parameter."
	echo "-p|--soma-port			: Port of the idg SOMA interface. This is a mandatory parameter."
	echo "-u|--soma-user			: SOMA interface user. This is a mandatory parameter."
	echo "-s|--soma-pass			: SOMA interface password. This is a mandatory parameter."
	echo "-h| --help                : Display help                      "	
	echo " "
	echo "For Example: "
	echo "		$0 -a 192.168.1.104 -p 5550 -u admin -s 123456"
	echo " "
	return 0
}


###########################
# variables and constants #
###########################

NOW=$( date +"%F_%H-%M-%S")

SCRIPTS_DIR="/MonTier/scripts/ModifyObjectConfig"

LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="ModifyObjectConfig_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

GENERSTED_FILES_DIR="${SCRIPTS_DIR}/GeneratedFiles"
TEMPLATE_FILE="Template_ModifyObjectConfig_SomaRequest.xml"

TIME_TO_SLEEP_BEFORE_CHANGE=1200
TIME_TO_SLEEP_AFTER_CHANGE=3000

ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-u|--soma-user)	
			SOMA_USER=$2
			if [[ -z ${SOMA_USER} ]]; then
				echo "ERROR : Invalid parameters. SOMA user is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-s|--soma-pass)	
			SOMA_PASS=$2
			if [[ -z ${SOMA_PASS} ]]; then
				echo "ERROR : Invalid parameters. SOMA password path is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-a|--dp-address)	
			DP_ADDRESS=$2
			if [[ -z ${DP_ADDRESS} ]]; then
				echo "ERROR : Invalid parameters. DP address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-p|--soma-port)	
			SOMA_PORT=$2
			if [[ -z ${SOMA_PORT} ]]; then
				echo "ERROR : Invalid parameters. SOMA port is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


if [[ -z ${SOMA_USER} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-user' is missing "
	exit 2
fi

if [[ -z ${SOMA_PASS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-pass' is missing "
	exit 2
fi

if [[ -z ${DP_ADDRESS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--dp-address' is missing "
	exit 2
fi

if [[ -z ${SOMA_PORT} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-port' is missing "
	exit 2
fi


############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}
mkdir -p ${GENERSTED_FILES_DIR}


info "Sleeping ${TIME_TO_SLEEP_BEFORE_CHANGE} seconds before first change"
echo "`current_date_time`: INFO: Sleeping ${TIME_TO_SLEEP_BEFORE_CHANGE} seconds before first change"
sleep ${TIME_TO_SLEEP_BEFORE_CHANGE}

while true; do
	while read p; do
		
		# do the change
		if [[ ${ABORT} -eq 0 ]]; then
		
			DOMAIN_NAME=`echo ${p} | awk -F: '{print $1}'`
			SERVICE_NAME=`echo ${p} | awk -F: '{print $2}'`
			
			REQUEST_FILE="${SERVICE_NAME}_${DOMAIN_NAME}_Request.xml"
			CURL_RESPONSE_FILE="${GENERSTED_FILES_DIR}/${SERVICE_NAME}_${DOMAIN_NAME}_Response.xml"	
			SAVE_DOMAIN_CONFIG_REQUEST="SaveConfig_${DOMAIN_NAME}_Request.xml"
			SAVE_DOMAIN_CONFIG_RESPONSE="${GENERSTED_FILES_DIR}/SaveConfig_${DOMAIN_NAME}_Response.xml"

			info "Modifying configuration of service ${SERVICE_NAME} on domain: ${DOMAIN_NAME}"
			echo "`current_date_time`: INFO: Modifying configuration of service ${SERVICE_NAME} on domain: ${DOMAIN_NAME}"
			curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${REQUEST_FILE}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${CURL_RESPONSE_FILE}
			if [ $? -ne 0 ]; then 
				error "Failed send CURL request for service ${SERVICE_NAME}. Abort"
				echo "`current_date_time`: ERROR: Failed send CURL request for service ${SERVICE_NAME}. Abort"
				ABORT=1
			fi
		fi
		
		if [[ ${ABORT} -eq 0 ]]; then
			info "Saving Domain ${DOMAIN_NAME} Config"
			echo "`current_date_time`: INFO: Saving Domain ${DOMAIN_NAME} Config"
			curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${SAVE_DOMAIN_CONFIG_REQUEST}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${SAVE_DOMAIN_CONFIG_RESPONSE}
			if [ $? -ne 0 ]; then 
				error "Failed to save domain ${DOMAIN_NAME} config. Abort"
				echo "`current_date_time`: ERROR: Failed to save domain ${DOMAIN_NAME} config. Abort"
				ABORT=1
			fi
		fi
		
		# revert the change
		if [[ ${ABORT} -eq 0 ]]; then
			
			info "Sleeping ${TIME_TO_SLEEP_AFTER_CHANGE} seconds before reverting change"
			echo "`current_date_time`: INFO: Sleeping ${TIME_TO_SLEEP_AFTER_CHANGE} seconds before reverting change"
			sleep ${TIME_TO_SLEEP_AFTER_CHANGE}
			
			REQUEST_FILE="Revert_${SERVICE_NAME}_${DOMAIN_NAME}_Request.xml"
			CURL_RESPONSE_FILE="${GENERSTED_FILES_DIR}/Revert_${SERVICE_NAME}_${DOMAIN_NAME}_Response.xml"	
			SAVE_DOMAIN_CONFIG_RESPONSE="${GENERSTED_FILES_DIR}/SaveConfig_Revert_${DOMAIN_NAME}_Response.xml"

			info "Reverting configuration of service ${SERVICE_NAME} on domain: ${DOMAIN_NAME}"
			echo "`current_date_time`: INFO: Reverting configuration of service ${SERVICE_NAME} on domain: ${DOMAIN_NAME}"
			curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${REQUEST_FILE}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${CURL_RESPONSE_FILE}
			if [ $? -ne 0 ]; then 
				error "Failed to revert service ${SERVICE_NAME}. Abort"
				echo "`current_date_time`: ERROR: Failed to revert service ${SERVICE_NAME}. Abort"
				ABORT=1
			fi
		fi
		
		if [[ ${ABORT} -eq 0 ]]; then
			info "Saving Domain ${DOMAIN_NAME} reverted config"
			echo "`current_date_time`: INFO: Saving Domain ${DOMAIN_NAME} reverted config"
			curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${SAVE_DOMAIN_CONFIG_REQUEST}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${SAVE_DOMAIN_CONFIG_RESPONSE}
			if [ $? -ne 0 ]; then 
				error "Failed to save domain ${DOMAIN_NAME} reverted config. Abort"
				echo "`current_date_time`: ERROR: Failed to save domain ${DOMAIN_NAME} reverted config. Abort"
				ABORT=1
			fi
		fi
		
		info "Sleeping ${TIME_TO_SLEEP_AFTER_CHANGE} seconds before next change"
		echo "`current_date_time`: INFO: Sleeping ${TIME_TO_SLEEP_AFTER_CHANGE} seconds before next change"
		sleep ${TIME_TO_SLEEP_AFTER_CHANGE}
		
	done < ServicesList.txt
done 

exit ${ABORT}