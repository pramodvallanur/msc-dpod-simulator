#!/bin/sh


#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


 
#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -i|--idg-name -a|--action"
	echo "	Or :  $0 -h|--help"
	echo ""
	echo "-i|--idg-name			    : IDG service name. This is a mandatory parameter."
	echo "-a|--action			    : The action to take. Can be 'start' or 'stop'. This is a mandatory parameter."
	echo "-h| --help                : Display help                      "	
	echo " "
	echo "For Example: "
	echo "		$0 -i idg2"
	echo " "
	return 0
}


###########################
# variables and constants #
###########################

NOW=$( date +"%F_%H-%M-%S")

SCRIPTS_DIR="/MonTier/scripts/RunJmeterTransactions"

LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="RunJmeterTransactions_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

JMETER_BIN_DIR="/MonTier/tools/jmeter/apache-jmeter-4.0/bin"
JMETER_LOGS_DIR="/MonTier/tools/jmeter/logs"

ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-a|--action)	
			ACTION=$2
			if [[ -z ${ACTION} ]]; then
				echo "ERROR : Invalid parameters. Action is missing"
				show_help
				exit 1
			fi
			shift 2
			;; 
		-i|--idg-name)	
			IDG_SEVICE_NAME=$2
			if [[ -z ${IDG_SEVICE_NAME} ]]; then
				echo "ERROR : Invalid parameters. IDG service name is missing"
				show_help
				exit 1
			fi
			shift 2
			;; 
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


if [[ -z ${IDG_SEVICE_NAME} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '-i|--idg-name' is missing "
	exit 2
fi

if [[ -z ${ACTION} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '-a|--action' is missing "
	exit 2
fi

if [ ! ${ACTION} == "start" ] && [ ! ${ACTION} == "stop" ]; then
	echo "ERROR : Invalid parameters. Mandatory parameter '--action' value '${ACTION}' is not valid. "
	exit 2
fi

############# 	Main Logic   ##############

cd ${JMETER_BIN_DIR}
mkdir -p ${LOG_DIR}
mkdir -p ${JMETER_LOGS_DIR}

PLANS_DIR="/MonTier/tools/jmeter/Jmeter_plans/${IDG_SEVICE_NAME}"

if [[ ${ACTION} == "start" ]]; then
	info "Starting Jmeter: ${PLANS_DIR}/JmeterTest_${IDG_SEVICE_NAME}.jmx"
	echo "`current_date_time`: INFO: Starting Jmeter: ${PLANS_DIR}/JmeterTest_${IDG_SEVICE_NAME}.jmx"
	nohup ./jmeter -n -t "${PLANS_DIR}/JmeterTest_${IDG_SEVICE_NAME}.jmx" > "${JMETER_LOGS_DIR}/JmeterTest_${IDG_SEVICE_NAME}.log" 2>&1 &
	
	info "Starting Jmeter: ${PLANS_DIR}/sideCalls_${IDG_SEVICE_NAME}.jmx"
	echo "`current_date_time`: INFO: Starting Jmeter: ${PLANS_DIR}/sideCalls_${IDG_SEVICE_NAME}.jmx"
	nohup ./jmeter -n -t "${PLANS_DIR}/sideCalls_${IDG_SEVICE_NAME}.jmx" > "${JMETER_LOGS_DIR}/sidecalls_${IDG_SEVICE_NAME}.log" 2>&1 &
	
	info "Starting Jmeter: ${PLANS_DIR}/B2B_${IDG_SEVICE_NAME}.jmx"
	echo "`current_date_time`: INFO: Starting Jmeter: ${PLANS_DIR}/B2B_${IDG_SEVICE_NAME}.jmx"
	nohup ./jmeter -n -t "${PLANS_DIR}/B2B_${IDG_SEVICE_NAME}.jmx" > "${JMETER_LOGS_DIR}/B2B_${IDG_SEVICE_NAME}.log" 2>&1 &
	
	info "Starting Jmeter: ${PLANS_DIR}/HospitalA_Domain_${IDG_SEVICE_NAME}.jmx"
	echo "`current_date_time`: INFO: Starting Jmeter: ${PLANS_DIR}/HospitalA_Domain_${IDG_SEVICE_NAME}.jmx"
	nohup ./jmeter -n -t "${PLANS_DIR}/HospitalA_Domain_${IDG_SEVICE_NAME}.jmx" > "${JMETER_LOGS_DIR}/HospitalA_Domain_${IDG_SEVICE_NAME}.log" 2>&1 &
	
	info "Starting Jmeter: ${PLANS_DIR}/tcp-ssl-proxies_${IDG_SEVICE_NAME}.jmx"
	echo "`current_date_time`: INFO: Starting Jmeter: ${PLANS_DIR}/tcp-ssl-proxies_${IDG_SEVICE_NAME}.jmx"
	nohup ./jmeter -n -t "${PLANS_DIR}/tcp-ssl-proxies_${IDG_SEVICE_NAME}.jmx" > "${JMETER_LOGS_DIR}/tcp-ssl-proxies_${IDG_SEVICE_NAME}.log" 2>&1 &
else
	info "Stopping Jmeters for ${IDG_SEVICE_NAME}"
	echo "`current_date_time`: INFO: Stopping Jmeters for ${IDG_SEVICE_NAME}"
	ps -ef | grep -i jmet | grep -i jar | grep ${IDG_SEVICE_NAME} | awk '{print $2}' | xargs kill -9
fi

if [ $? -ne 0 ]; then 
	ABORT=1
fi

exit ${ABORT}
