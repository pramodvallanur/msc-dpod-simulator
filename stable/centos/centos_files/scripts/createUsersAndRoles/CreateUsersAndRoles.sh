#!/bin/bash

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


 
#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -a|--dpod-address -u|--dpod-user -p|--dpod-pass"
	echo "	Or :  $0 -h|--help"
	echo " "
	echo "-u|--dpod-user 	 	: User for DPOD REST commands. This is a mandatory parameter.                 "
	echo "-a|--dpod-address  	: User for DPOD REST commands. This is a mandatory parameter.                 "
	echo "-p|--dpod-pass  	 	: User for DPOD REST commands. This is a mandatory parameter.                 "	
	echo "-h|--help     		: Display help                      "	
	echo " "
	return 0
}

###########################
# variables and constants #
###########################


NOW=$( date +"%F_%H-%M-%S")

LOCAL_FILES_DIR="/MonTier/scripts/createUsersAndRoles"

LOG_DIR="${LOCAL_FILES_DIR}/logs"
LOG_FILE_NAME="CreateUsersAndRoles_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

GENERSTED_FILES_DIR="${LOCAL_FILES_DIR}/GeneratedFiles"

REST_URI="/op/api/v1/simulator/populate"

ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-a|--dpod-address)	
			DPOD_ADDRESS=$2
			if [[ -z ${DPOD_ADDRESS} ]]; then
				echo "ERROR : Invalid parameters. DPOD address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-u|--dpod-user)	
			DPOD_USER=$2
			if [[ -z ${DPOD_USER} ]]; then
				echo "ERROR : Invalid parameters. DPOD user is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-p|--dpod-pass)	
			DPOD_PASS=$2
			if [[ -z ${DPOD_PASS} ]]; then
				echo "ERROR : Invalid parameters. DPOD password is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


if [[ -z ${DPOD_ADDRESS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--dpod-address' is missing "
	exit 2
fi

if [[ -z ${DPOD_USER} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--dpod-user' is missing "
	exit 2
fi

if [[ -z ${DPOD_PASS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--dpod-pass' is missing "
	exit 2
fi

############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}
mkdir -p ${GENERSTED_FILES_DIR}

CURL_RESPONSE_FILE="${GENERSTED_FILES_DIR}/CreateUsersAndRoles_Response.json"	
REST_URL="https://${DPOD_ADDRESS}${REST_URI}"

info "Using DPOD REST interface to create users and roles"
echo "`current_date_time`: INFO: Using DPOD REST interface to create users and roles"
curl --user ${DPOD_USER}:${DPOD_PASS} -X POST  --header 'dpod-simulator: 922cc43d39844d22873bfc85bf1dcdac' --insecure -w "\n" ${REST_URL} > ${CURL_RESPONSE_FILE}
if [ $? -ne 0 ]; then 
	error "Failed to send CURL request. Abort"
	echo "`current_date_time`: ERROR: Failed to send CURL request. Abort"
	ABORT=1
fi

exit ${ABORT}

