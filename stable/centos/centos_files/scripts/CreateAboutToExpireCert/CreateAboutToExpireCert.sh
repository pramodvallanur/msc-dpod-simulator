#!/bin/sh

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


 
#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -a|--dp-address -p|--soma-port -u|--soma-user -s|--soma-pass "
	echo "	Or :  $0 -h|--help"
	echo ""
	echo "-a|--dp-address			: Address of the idg. This is a mandatory parameter."
	echo "-p|--soma-port			: Port of the idg SOMA interface. This is a mandatory parameter."
	echo "-u|--soma-user			: SOMA interface user. This is a mandatory parameter."
	echo "-s|--soma-pass			: SOMA interface password. This is a mandatory parameter."
	echo "-h| --help                : Display help                      "	
	echo " "
	echo "For Example: "
	echo "		$0 -a 192.168.1.104 -p 5550 -u admin -s 123456"
	echo " "
	return 0
}


###########################
# variables and constants #
###########################

NOW=$( date +"%F_%H-%M-%S")

SCRIPTS_DIR="/MonTier/scripts/CreateAboutToExpireCert"

LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="CreateAboutToExpireCert_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

GENERSTED_FILES_DIR="${SCRIPTS_DIR}/GeneratedFiles"
TEMPLATES_DIR="${SCRIPTS_DIR}/Templates"

UPLOAD_FILE_TEMPLATE="${TEMPLATES_DIR}/Template_UploadFile_SomaRequest.xml"

UPLOAD_FILE_SOMA_REQUEST="${GENERSTED_FILES_DIR}/UploadFile_SomaRequest.xml"
STOP_MONITOR_SOMA_REQUEST="StopCertMonitor_SomaRequest.xml"
START_MONITOR_SOMA_REQUEST="StartCertMonitor_SomaRequest.xml"
UPDATE_CERT_OBJECT_SOMA_REQUEST="UpdateCertObject_SomaRequest.xml"
SAVE_DOMAIN_CONFIG_REQUEST="SaveConfig_default_SomaRequest.xml"
SAVE_DEF_DOMAIN_CONFIG_REQUEST="SaveConfig_HotelsA_SomaRequest.xml"

UPLOAD_FILE_SOMA_RESPONSE="${GENERSTED_FILES_DIR}/UploadFile_SomaResponse.xml"
STOP_MONITOR_SOMA_RESPONSE="${GENERSTED_FILES_DIR}/StopCertMontitor_SomaResponse.xml"
START_MONITOR_SOMA_RESPONSE="${GENERSTED_FILES_DIR}/StartCertMontitor_SomaResponse.xml"
UPDATE_CERT_OBJECT_SOMA_RESPONSE="${GENERSTED_FILES_DIR}/UpdateCertObject_SomaResponse.xml"
SAVE_DOMAIN_CONFIG_RESPONSE="${GENERSTED_FILES_DIR}/SaveConfig_default_SomaResponse.xml"
SAVE_DEF_DOMAIN_CONFIG_RESPONSE="${GENERSTED_FILES_DIR}/SaveConfig_HotelsA_SomaResponse.xml"
CERT_FILE="${GENERSTED_FILES_DIR}/HotelsA.pem"
BASE64_CERT="${GENERSTED_FILES_DIR}/base64Cert"

DAYS_TILL_EXPIRY=50

ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-u|--soma-user)	
			SOMA_USER=$2
			if [[ -z ${SOMA_USER} ]]; then
				echo "ERROR : Invalid parameters. SOMA user is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-s|--soma-pass)	
			SOMA_PASS=$2
			if [[ -z ${SOMA_PASS} ]]; then
				echo "ERROR : Invalid parameters. SOMA password path is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-a|--dp-address)	
			DP_ADDRESS=$2
			if [[ -z ${DP_ADDRESS} ]]; then
				echo "ERROR : Invalid parameters. DP address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-p|--soma-port)	
			SOMA_PORT=$2
			if [[ -z ${SOMA_PORT} ]]; then
				echo "ERROR : Invalid parameters. SOMA port is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


if [[ -z ${SOMA_USER} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-user' is missing "
	exit 2
fi

if [[ -z ${SOMA_PASS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-pass' is missing "
	exit 2
fi

if [[ -z ${DP_ADDRESS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--dp-address' is missing "
	exit 2
fi

if [[ -z ${SOMA_PORT} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-port' is missing "
	exit 2
fi


############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}
mkdir -p ${GENERSTED_FILES_DIR}

# Create cert, using openSSL
info "Creating certificate, that will expire in ${DAYS_TILL_EXPIRY} days"
echo "`current_date_time`: INFO: Creating certificate, that will expire in ${DAYS_TILL_EXPIRY} days"
#cd ${GENERSTED_FILES_DIR}
openssl req -x509 -newkey rsa:4096 -keyout ${GENERSTED_FILES_DIR}/key.pem -out ${CERT_FILE} -days ${DAYS_TILL_EXPIRY} -subj "/CN=HotelsA_Domain" -nodes
if [ $? -ne 0 ]; then 
	error "Failed to create certificate. Abort"
	echo "`current_date_time`: ERROR: Failed to create certificate. Abort"
	ABORT=1
fi

# Encode cert in base64 
if [[ ${ABORT} -eq 0 ]]; then
	info "Encoding Cert in base64"
	echo "`current_date_time`: INFO: Encoding Cert in base64"
	cat ${CERT_FILE} | base64 > ${BASE64_CERT}
	if [ $? -ne 0 ]; then 
		error "Failed to encode certificate. Abort"
		echo "`current_date_time`: ERROR: Failed to encode certificate. Abort"
		ABORT=1
	fi
fi

# Create SOMA Request from template
if [[ ${ABORT} -eq 0 ]]; then
	info "Creating SOMA Request"
	echo "`current_date_time`: INFO: Creating SOMA Request"
	cp ${UPLOAD_FILE_TEMPLATE} ${UPLOAD_FILE_SOMA_REQUEST}
	sed -i "/__BASE64__/r ${BASE64_CERT}" ${UPLOAD_FILE_SOMA_REQUEST}
	sed -i "/__BASE64__/d" ${UPLOAD_FILE_SOMA_REQUEST}
	if [ $? -ne 0 ]; then 
		error "Failed to create SOMA request. Abort"
		echo "`current_date_time`: ERROR: Failed to create SOMA request. Abort"
		ABORT=1
	fi
fi

# upload cert to idg
if [[ ${ABORT} -eq 0 ]]; then
	info "Uploading certificate to idg"
	echo "`current_date_time`: INFO: Uploading certificate to idg"
	curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${UPLOAD_FILE_SOMA_REQUEST}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${UPLOAD_FILE_SOMA_RESPONSE}
	if [ $? -ne 0 ]; then 
		error "Failed to upload certificate to idg. Abort"
		echo "`current_date_time`: ERROR: Failed to upload certificate to idg. Abort"
		ABORT=1
	fi
fi

# update CryptoCert object to point to the cert
if [[ ${ABORT} -eq 0 ]]; then
	info "Updating CryptoCert object to point to new certificate"
	echo "`current_date_time`: INFO: Updating CryptoCert object to point to new certificate"
	curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${UPDATE_CERT_OBJECT_SOMA_REQUEST}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${UPDATE_CERT_OBJECT_SOMA_RESPONSE}
	if [ $? -ne 0 ]; then 
		error "Failed to update CryptoCert object. Abort"
		echo "`current_date_time`: ERROR: Failed to update CryptoCert object. Abort"
		ABORT=1
	fi
fi

# stop/start the certificate monitor object
if [[ ${ABORT} -eq 0 ]]; then
	info "Stopping CertMonitor object"
	echo "`current_date_time`: INFO: Stopping CertMonitor object"
	curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${STOP_MONITOR_SOMA_REQUEST}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${STOP_MONITOR_SOMA_RESPONSE}
	if [ $? -ne 0 ]; then 
		error "Failed to stop CertMonitor object. Abort"
		echo "`current_date_time`: ERROR: Failed to stop CertMonitor object. Abort"
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Starting CertMonitor object"
	echo "`current_date_time`: INFO: Starting CertMonitor object"
	curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${START_MONITOR_SOMA_REQUEST}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${START_MONITOR_SOMA_RESPONSE}
	if [ $? -ne 0 ]; then 
		error "Failed to start CertMonitor object. Abort"
		echo "`current_date_time`: ERROR: Failed to start CertMonitor object. Abort"
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Saving Domain Config"
	echo "`current_date_time`: INFO: Saving Domain Config"
	curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${SAVE_DOMAIN_CONFIG_REQUEST}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${SAVE_DOMAIN_CONFIG_RESPONSE}
	if [ $? -ne 0 ]; then 
		error "Failed to save domain config. Abort"
		echo "`current_date_time`: ERROR: Failed to save domain config. Abort"
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Saving Default Domain Config"
	echo "`current_date_time`: INFO: Saving Default Domain Config"
	curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${SAVE_DEF_DOMAIN_CONFIG_REQUEST}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${SAVE_DEF_DOMAIN_CONFIG_RESPONSE}
	if [ $? -ne 0 ]; then 
		error "Failed to save default domain config. Abort"
		echo "`current_date_time`: ERROR: Failed to save default domain config. Abort"
		ABORT=1
	fi
fi

exit ${ABORT}