#!/bin/sh


#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


 
#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -a|--dp-address -p|--soma-port -u|--soma-user -s|--soma-pass -t|--time-offset"
	echo "	Or :  $0 -h|--help"
	echo ""
	echo "-a|--dp-address			: Address of the idg. This is a mandatory parameter."
	echo "-p|--soma-port			: Port of the idg SOMA interface. This is a mandatory parameter."
	echo "-u|--soma-user			: SOMA interface user. This is a mandatory parameter."
	echo "-s|--soma-pass			: SOMA interface password. This is a mandatory parameter."
	echo "-t|--time-offset			: Time offset from UTC. This is a mandatory parameter."
	echo "-h| --help                : Display help                      "	
	echo " "
	echo "For Example: "
	echo "		$0 -a 192.168.1.104 -p 5550 -u admin -s 123456 -t +002"
	echo " "
	return 0
}



###########################
# variables and constants #
###########################

NOW=$( date +"%F_%H-%M-%S")

SCRIPTS_DIR="/MonTier/scripts/ModifyTimeZone"

LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="ModifyTimeZone_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

GENERSTED_FILES_DIR="${SCRIPTS_DIR}/GeneratedFiles"
TEMPLATE_FILE="${SCRIPTS_DIR}/Templates/ModifyTimeZone_Request.xml"
DP_TIMEZONES_OFFSET_LIST_FILE="${SCRIPTS_DIR}/DataPowerTimezoneOffsets.txt"


ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-u|--soma-user)	
			SOMA_USER=$2
			if [[ -z ${SOMA_USER} ]]; then
				echo "ERROR : Invalid parameters. SOMA user is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-s|--soma-pass)	
			SOMA_PASS=$2
			if [[ -z ${SOMA_PASS} ]]; then
				echo "ERROR : Invalid parameters. SOMA password path is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-a|--dp-address)	
			DP_ADDRESS=$2
			if [[ -z ${DP_ADDRESS} ]]; then
				echo "ERROR : Invalid parameters. DP address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-p|--soma-port)	
			SOMA_PORT=$2
			if [[ -z ${SOMA_PORT} ]]; then
				echo "ERROR : Invalid parameters. SOMA port is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-t|--time-offset)	
			SERVER_OFFSET_FROM_UTC=$2
			if [[ -z ${SERVER_OFFSET_FROM_UTC} ]]; then
				echo "ERROR : Invalid parameters. Time offset port is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


if [[ -z ${SOMA_USER} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-user' is missing "
	exit 2
fi

if [[ -z ${SOMA_PASS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-pass' is missing "
	exit 2
fi

if [[ -z ${DP_ADDRESS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--dp-address' is missing "
	exit 2
fi

if [[ -z ${SOMA_PORT} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-port' is missing "
	exit 2
fi


############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}
mkdir -p ${GENERSTED_FILES_DIR}
			
REQUEST_FILE="${GENERSTED_FILES_DIR}/${DP_ADDRESS}_Request.xml"
CURL_RESPONSE_FILE="${DP_ADDRESS}_Response.xml"	
SAVE_DOMAIN_CONFIG_REQUEST="SaveConfig_default_domain_Request.xml"
SAVE_DOMAIN_CONFIG_RESPONSE="${GENERSTED_FILES_DIR}/SaveConfig_${DP_ADDRESS}_default_domain_Response.xml"

info "Creating SOMA request"
echo "`current_date_time`: INFO: Creating SOMA request"

DIRECTION_SIGN=`date +"%z" | cut -c 1`
if [[ ${DIRECTION_SIGN} = "+" ]]; then
	DIRECTION="East"
else
	DIRECTION="West"
fi

HOURS_OFFSET=`date +"%z" | cut -c 2-3`
MINUTES_OFFSET=`date +"%z" | cut -c 4-5`
CURRENT_MONTH=`date -d "$D" '+%m'`

case "$CURRENT_MONTH" in
	01)	
		DAYLIGHT_START_MONTH="February"
		DAYLIGHT_STOP_MONTH="March"
		;;
	02)	
		DAYLIGHT_START_MONTH="March"
		DAYLIGHT_STOP_MONTH="April"
		;;
	03)	
		DAYLIGHT_START_MONTH="April"
		DAYLIGHT_STOP_MONTH="May"
		;;
	04)	
		DAYLIGHT_START_MONTH="May"
		DAYLIGHT_STOP_MONTH="June"
		;;
	05)	
		DAYLIGHT_START_MONTH="June"
		DAYLIGHT_STOP_MONTH="July"
		;;
	06)	
		DAYLIGHT_START_MONTH="July"
		DAYLIGHT_STOP_MONTH="August"
		;;
	07)	
		DAYLIGHT_START_MONTH="August"
		DAYLIGHT_STOP_MONTH="September"
		;;
	08)	
		DAYLIGHT_START_MONTH="September"
		DAYLIGHT_STOP_MONTH="October"
		;;
	09)	
		DAYLIGHT_START_MONTH="October"
		DAYLIGHT_STOP_MONTH="November"
		;;
	10)	
		DAYLIGHT_START_MONTH="November"
		DAYLIGHT_STOP_MONTH="December"
		;;
	11)	
		DAYLIGHT_START_MONTH="December"
		DAYLIGHT_STOP_MONTH="January"
		;;
	12)	
		DAYLIGHT_START_MONTH="January"
		DAYLIGHT_STOP_MONTH="February"
		;;
	*)
		info "Failed to locate next month for : $CURRENT_MONTH ! " 	
		echo "`current_date_time`: ERROR: Failed to locate next month for : $CURRENT_MONTH ! " 	
		ABROT=1
esac			

if [[ ${ABORT} -eq 0 ]]; then
	cp ${TEMPLATE_FILE} ${REQUEST_FILE}
	sed -i "s/__DIRECTION__/${DIRECTION}/g" ${REQUEST_FILE}
	sed -i "s/__HOURS_OFFSET__/${HOURS_OFFSET}/g" ${REQUEST_FILE}
	sed -i "s/__MINUTES_OFFSET__/${MINUTES_OFFSET}/g" ${REQUEST_FILE}
	sed -i "s/__DAYLIGHT_START_MONTH__/${DAYLIGHT_START_MONTH}/g" ${REQUEST_FILE}
	sed -i "s/__DAYLIGHT_STOP_MONTH__/${DAYLIGHT_STOP_MONTH}/g" ${REQUEST_FILE}
	if [ $? -ne 0 ]; then 
		error "Failed create SOMA request. Abort"
		echo "`current_date_time`: ERROR: Failed create SOMA request. Abort"
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Modifying Timezone configuration of device ${DP_ADDRESS}"
	echo "`current_date_time`: INFO: Modifying Timezone configuration of device ${DP_ADDRESS}"
	curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${REQUEST_FILE}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${CURL_RESPONSE_FILE}
	if [ $? -ne 0 ]; then 
		error "Failed to send CURL request for device ${DP_ADDRESS}. Abort"
		echo "`current_date_time`: ERROR: Failed to send CURL request for device ${DP_ADDRESS}. Abort"
		ABORT=1
	fi
fi
		
if [[ ${ABORT} -eq 0 ]]; then
	info "Saving device ${DP_ADDRESS} Config"
	echo "`current_date_time`: INFO: Saving device ${DP_ADDRESS} Config"
	curl --user ${SOMA_USER}:${SOMA_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${SAVE_DOMAIN_CONFIG_REQUEST}" --insecure -w "\n" https://${DP_ADDRESS}:${SOMA_PORT}/service/mgmt/current > ${SAVE_DOMAIN_CONFIG_RESPONSE}
	if [ $? -ne 0 ]; then 
		error "Failed to save device ${DP_ADDRESS} config. Abort"
		echo "`current_date_time`: ERROR: Failed to save device ${DP_ADDRESS} config. Abort"
		ABORT=1
	fi
fi
		
exit ${ABORT}