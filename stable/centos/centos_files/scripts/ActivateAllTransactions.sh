#!/bin/sh

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -i|--idg-service -n|-idg-name -a|--idg-address -p|--soma-port -u|--soma-user -s|--soma-pass -d|--dpod-address -r|--dpod-user -o|--dpod-pass"
	echo "	Or :  $0 -h|--help"
	echo " "
	echo "-i|-idg-service 	: Name of the IDG service                  "	
	echo "-n|-idg-name    	: Name of the IDG                   "	
	echo "-a|--idg-address	: Address of the IDG			"
	echo "-p|--soma-port  	: SOMA port                   "	
	echo "-u|--soma-user  	: SOMA user                   "	
	echo "-s|--soma-pass  	: SOMA password                   "	
	echo "-d|--dpod-address	: Address of the DPOD			"
	echo "-r|--dpod-user  	: DPOD REST user                   "	
	echo "-o|--dpod-pass  	: DPOD REST password                   "	
	echo " "
	echo "-h|--help       	: Display help                      "	
	echo " "
	return 0
}

###########################
# variables and constants #
###########################

NOW=$( date +"%F_%H-%M-%S")

SCRIPTS_DIR="/MonTier/scripts"

LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="ActivateAllTransactions_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-i|--idg-service)	
			IDG_SEVICE_NAME=$2
			if [[ -z ${IDG_SEVICE_NAME} ]]; then
				echo "ERROR : Invalid parameters. IDG service name is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-n|--idg-name)	
			DP_NAME=$2
			if [[ -z ${DP_NAME} ]]; then
				echo "ERROR : Invalid parameters. DP name is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-a|--idg-address)	
			DP_ADDRESS=$2
			if [[ -z ${DP_ADDRESS} ]]; then
				echo "ERROR : Invalid parameters. DP address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-p|--soma-port)	
			SOMA_PORT=$2
			if [[ -z ${SOMA_PORT} ]]; then
				echo "ERROR : Invalid parameters. SOMA port is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-u|--soma-user)	
			SOMA_USER=$2
			if [[ -z ${SOMA_USER} ]]; then
				echo "ERROR : Invalid parameters. SOMA user is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-s|--soma-pass)	
			SOMA_PASS=$2
			if [[ -z ${SOMA_PASS} ]]; then
				echo "ERROR : Invalid parameters. SOMA password is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-d|--dpod-address)	
			DPOD_ADDRESS=$2
			if [[ -z ${DPOD_ADDRESS} ]]; then
				echo "ERROR : Invalid parameters. DPOD address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-r|--dpod-user)	
			DPOD_USER=$2
			if [[ -z ${DPOD_USER} ]]; then
				echo "ERROR : Invalid parameters. DPOD user is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-o|--dpod-pass)	
			DPOD_PASS=$2
			if [[ -z ${DPOD_PASS} ]]; then
				echo "ERROR : Invalid parameters. DPOD password is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done

if [[ -z ${IDG_SEVICE_NAME} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--idg-service' is missing "
	exit 2
fi

if [[ -z ${DP_NAME} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--idg-name' is missing "
	exit 2
fi

if [[ -z ${DP_ADDRESS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--idg-address' is missing "
	exit 2
fi

if [[ -z ${SOMA_PORT} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-port' is missing "
	exit 2
fi

if [[ -z ${SOMA_USER} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-user' is missing "
	exit 2
fi

if [[ -z ${SOMA_PASS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--soma-pass' is missing "
	exit 2
fi

if [[ -z ${DPOD_USER} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--dpod-user' is missing "
	exit 2
fi

if [[ -z ${DPOD_PASS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--dpod-pass' is missing "
	exit 2
fi

if [[ -z ${DPOD_ADDRESS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--dpod-address' is missing "
	exit 2
fi


############# 	Main Logic   ##############
 

info "Activating Jmeter plans"
echo "`current_date_time`: INFO: Activating Jmeter plans"
cd "${SCRIPTS_DIR}/RunJmeterTransactions"
nohup ./RunJmeterTransactions.sh -a start -i ${IDG_SEVICE_NAME} > "${LOG_DIR}/RunJmeterTransactions_${IDG_SEVICE_NAME}.log" 2>&1 &
if [ $? -ne 0 ]; then 
	error "Failed to activate Jmeter transactions. Abort"
	echo "`current_date_time`: ERROR: Failed to activate Jmeter transactions. Abort"
	ABORT=1
fi

info "Activating EarlyCompleted transactions"
echo "`current_date_time`: INFO: Activating EarlyCompleted transaction"
cd "${SCRIPTS_DIR}/RunEarlyCompletedTrans"
nohup ./RunAll_EarlyConmleted.sh -a ${DP_ADDRESS} > "${LOG_DIR}/EarlyCompletedTrans_${IDG_SEVICE_NAME}.log" 2>&1 &
if [ $? -ne 0 ]; then 
	error "Failed to activate EarlyCompleted transactions. Abort"
	echo "`current_date_time`: ERROR: Failed to activate EarlyCompleted transactions. Abort"
	ABORT=1
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Activating WSM subscriptions"
	echo "`current_date_time`: INFO: Activating WSM subscriptions"
	cd ${SCRIPTS_DIR}/SubscribeToWSM
	nohup ./SubscribeToWSM.sh -n ${DP_NAME} -a ${DPOD_ADDRESS} -u ${DPOD_USER} -p ${DPOD_PASS} > "${LOG_DIR}/SubscribeToWSM_${IDG_SEVICE_NAME}.log" 2>&1 &
	if [ $? -ne 0 ]; then 
		error "Failed to activate WSM subscriptions for device ${DP_NAME}. Abort"
		echo "`current_date_time`: ERROR: Failed to activate WSM subscriptions for device ${DP_NAME}. Abort"
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Activating Probes"
	echo "`current_date_time`: INFO: Activating Probes"
	cd ${SCRIPTS_DIR}/ActivateProbes
	nohup ./ActivateProbes.sh -a ${DP_ADDRESS} -p ${SOMA_PORT} -u ${SOMA_USER} -s ${SOMA_PASS} > "${LOG_DIR}/ActivateProbes_${IDG_SEVICE_NAME}.log" 2>&1 &
	if [ $? -ne 0 ]; then 
		error "Failed to activate probes. Abort"
		echo "`current_date_time`: ERROR: Failed to activate probes. Abort"
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Creating Certificates that are about to expire"
	echo "`current_date_time`: INFO: Creating Certificates that are about to expire"
	cd ${SCRIPTS_DIR}/CreateAboutToExpireCert
	nohup ./CreateAboutToExpireCert.sh -a ${DP_ADDRESS} -p ${SOMA_PORT} -u ${SOMA_USER} -s ${SOMA_PASS} > "${LOG_DIR}/CreateAboutToExpireCert_${IDG_SEVICE_NAME}.log" 2>&1 &
	if [ $? -ne 0 ]; then 
		error "Failed to create certificates. Abort"
		echo "`current_date_time`: ERROR: Failed to create certificates. Abort"
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Running Object configuration modifications"
	echo "`current_date_time`: INFO: Running Object configuration modifications"
	cd ${SCRIPTS_DIR}/ModifyObjectConfig
	nohup ./ModifyObjectConfig.sh -a ${DP_ADDRESS} -p ${SOMA_PORT} -u ${SOMA_USER} -s ${SOMA_PASS} > "${LOG_DIR}/ModifyObjectConfig_${IDG_SEVICE_NAME}.log" 2>&1 &
	if [ $? -ne 0 ]; then 
		error "Failed to modify objects configuration. Abort"
		echo "`current_date_time`: ERROR: Failed to modify objects configuration. Abort"
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Enabling B2B"
	echo "`current_date_time`: INFO: Enabling B2B"
	cd ${SCRIPTS_DIR}/ActivateB2B
	nohup ./ActivateB2B.sh -a ${DP_ADDRESS} -p ${SOMA_PORT} -u ${SOMA_USER} -s ${SOMA_PASS} > "${LOG_DIR}/ActivateB2B_${IDG_SEVICE_NAME}.log" 2>&1 &
	if [ $? -ne 0 ]; then 
		error "Failed to enable B2B. Abort"
		echo "`current_date_time`: ERROR: Failed to enable B2B. Abort"
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Creating DPOD users and roles"
	echo "`current_date_time`: INFO: Creating DPOD users and roles"
	cd ${SCRIPTS_DIR}/createUsersAndRoles
	nohup ./CreateUsersAndRoles.sh -a ${DPOD_ADDRESS} -u ${DPOD_USER} -p ${DPOD_PASS} > "${LOG_DIR}/CreateUsersAndRoles_${IDG_SEVICE_NAME}.log" 2>&1 &
	if [ $? -ne 0 ]; then 
		error "Failed to create DPOD users and roles. Abort"
		echo "`current_date_time`: ERROR: Failed to create DPOD users and roles. Abort"
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Restarting Domains"
	echo "`current_date_time`: INFO: Restarting Domains"
	cd ${SCRIPTS_DIR}/RestartDomains
	nohup ./RestartDomains.sh -a ${DP_ADDRESS} -p ${SOMA_PORT} -u ${SOMA_USER} -s ${SOMA_PASS} > "${LOG_DIR}/RestartDomains_${IDG_SEVICE_NAME}.log" 2>&1 &
	if [ $? -ne 0 ]; then 
		error "Failed to restart domains. Abort"
		echo "`current_date_time`: ERROR: Failed to restart domains. Abort"
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Modifying device ${IDG_SEVICE_NAME} timezone"
	echo "`current_date_time`: INFO: Modifying device ${IDG_SEVICE_NAME} timezone"
	cd ${SCRIPTS_DIR}/ModifyTimeZone
	nohup ./ModifyTimeZone.sh -a ${DP_ADDRESS} -p ${SOMA_PORT} -u ${SOMA_USER} -s ${SOMA_PASS} > "${LOG_DIR}/ModifyTimeZone_${IDG_SEVICE_NAME}.log" 2>&1 &
	if [ $? -ne 0 ]; then 
		error "Failed to set timezone. Abort"
		echo "`current_date_time`: ERROR: Failed to set timezone. Abort"
		ABORT=1
	fi
fi

exit ${ABORT}


