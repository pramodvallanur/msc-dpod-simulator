#!/bin/sh

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -a|--dpod-address -p|--dpod-pass"
	echo "	Or :  $0 -h|--help"
	echo " "
	echo "-a|--dpod-address	: Address of the DPOD server			"
	echo "-p|--dpod-pass  	: DPOD SSH password                   "	
	echo " "
	echo "-h|--help       	: Display help                      "	
	echo " "
	return 0
}

###########################
# variables and constants #
###########################

NOW=$( date +"%F_%H-%M-%S")


INSTALLS_DIR="/MonTier/installs"
INSTALLS_DIR_SRC="/MonTier/dpodSimulatorInst"
CENTOS_JDK_SRC="${INSTALLS_DIR_SRC}/jdk-8u171-linux-x64.rpm"  
CENTOS_JDK_DST="${INSTALLS_DIR}/jdk-8u171-linux-x64.rpm"
CENTOS_JMETER_SRC="${INSTALLS_DIR_SRC}/apache-jmeter-4.0.tgz"
CENTOS_JMETER_DST="${INSTALLS_DIR}/apache-jmeter-4.0.tgz"
TOOLS_DIR="/MonTier/tools"

SCRIPTS_DIR="/MonTier/scripts"
LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="InstallPrereq_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

ABORT=0
DPOD_CONTAINER_IS_READY=1

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-a|--dpod-address)	
			DPOD_ADDRESS=$2
			if [[ -z ${DPOD_ADDRESS} ]]; then
				echo "ERROR : Invalid parameters. DPOD address is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-p|--dpod-pass)	
			DPOD_PASS=$2
			if [[ -z ${DPOD_PASS} ]]; then
				echo "ERROR : Invalid parameters. DPOD password is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


if [[ -z ${DPOD_PASS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--dpod-pass' is missing "
	exit 2
fi

if [[ -z ${DPOD_ADDRESS} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--dpod-address' is missing "
	exit 2
fi

############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}
mkdir -p ${INSTALLS_DIR}
if [ $? -ne 0 ]; then 
	error "Failed to create directory ${INSTALLS_DIR}. Abort"
	echo "`current_date_time`: ERROR: Failed to create directory ${INSTALLS_DIR}. Abort"
	ABORT=1
fi

while [[ ${DPOD_CONTAINER_IS_READY} -ne 0 ]] ; do
        info "Testing connection to dpod container"
        echo "`current_date_time`: INFO: Testing connection to dpod container"
        nc -z ${DPOD_ADDRESS} 22
        DPOD_CONTAINER_IS_READY=$?
        sleep 10
done

# Get the java install from dpod container
if [[ ${ABORT} -eq 0 ]]; then
	info "Transferring java RPM from dpod container"
	echo "`current_date_time`: INFO: Transferring java RPM from dpod container"
	sshpass -p "${DPOD_PASS}" scp -o "StrictHostKeyChecking no" ${DPOD_ADDRESS}:${CENTOS_JDK_SRC} ${CENTOS_JDK_DST} >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then 
		error "Failed to transfer java RPM. Abort"
		echo "`current_date_time`: ERROR: Failed to transfer java RPM. Abort"
		error "sshpass -p \"${DPOD_PASS}\" scp -o \"StrictHostKeyChecking no\" ${DPOD_ADDRESS}:${CENTOS_JDK_SRC} ${CENTOS_JDK_DST}"
		ABORT=1
	fi
fi

# Get the jmeter install from dpod container
if [[ ${ABORT} -eq 0 ]]; then
	info "Transferring jmeter install file from dpod container"
	echo "`current_date_time`: INFO: Transferring jmeter install file from dpod container"
	sshpass -p "${DPOD_PASS}" scp -o "StrictHostKeyChecking no" ${DPOD_ADDRESS}:${CENTOS_JMETER_SRC} ${CENTOS_JMETER_DST} >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then 
		error "Failed to transfer jmeter file. Abort"
		echo "`current_date_time`: ERROR: Failed to transfer jmeter file. Abort"
		ABORT=1
	fi
fi

# Install java
if [[ ${ABORT} -eq 0 ]]; then
	info "Installing Java"
	echo "`current_date_time`: INFO: Installing Java"
	mkdir -p ${TOOLS_DIR}/java >> ${LOG_FILE} 2>&1
	rpm -Uvh ${CENTOS_JDK_DST} >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then 
		error "Failed to install java. Abort"
		echo "`current_date_time`: ERROR: Failed to install java. Abort"
		ABORT=1
	fi
fi

# Install Jmeter
if [[ ${ABORT} -eq 0 ]]; then
	info "Installing Jmeter"
	echo "`current_date_time`: INFO: Installing Jmeter"
	mkdir -p ${TOOLS_DIR}/jmeter >> ${LOG_FILE} 2>&1
	tar -xf ${CENTOS_JMETER_DST} -C ${TOOLS_DIR}/jmeter >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then 
		error "Failed to install jmeter. Abort"
		echo "`current_date_time`: ERROR: Failed to install jmeter. Abort"
		ABORT=1
	fi
fi

exit ${ABORT}
