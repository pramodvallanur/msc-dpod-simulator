<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns:env11="http://schemas.xmlsoap.org/soap/envelope/" 
		xmlns:env12="http://www.w3.org/2001/12/soap-envelope"
		xmlns:dp="http://www.datapower.com/extensions" 
		xmlns:dpconfig="http://www.datapower.com/param/config" 
		xmlns:func="http://exslt.org/functions"
		xmlns:dpfunc="http://www.datapower.com/extensions/functions"
		extension-element-prefixes="dp" 
		exclude-result-prefixes="dp dpconfig env11 env12 func dpfunc" version="1.0">

	<xsl:output method="xml"/>

	<xsl:param name="dpconfig:dataSource" select="''" />
	<dp:param name="dpconfig:dataSource" xmlns="">
		<display>SQL Data Source</display>
		<description>Name of SQL Data Source object</description>
		<tab-override>basic</tab-override>
		<default/>
		<required-when>
			<condition evaluation="logical-true" />
		</required-when>
	</dp:param>



	<xsl:template match="/">

		<xsl:variable name="statement">SELECT * FROM DEPT</xsl:variable>    

		<xsl:variable name="result">
			<dp:sql-execute source="$dpconfig:dataSource" statement="$statement"/>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$result/sql/row" >
				<xsl:message dp:priority="info">
					<xsl:text>sql-execute results: </xsl:text>
					<xsl:copy-of select="$result"/>
				</xsl:message>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message dp:priority="error" >
					<xsl:text>Something went wrong with the sql-execute. No response recieved.</xsl:text>
					<xsl:copy-of select="$result"/>
				</xsl:message>	
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>		
</xsl:stylesheet>