<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:env11="http://schemas.xmlsoap.org/soap/envelope/"
				xmlns:env12="http://www.w3.org/2001/12/soap-envelope"
				xmlns:dp="http://www.datapower.com/extensions" 
				xmlns:dpconfig="http://www.datapower.com/param/config"
			    xmlns:dpquery="http://www.datapower.com/param/query"
				xmlns:mmi="http://ca.com/unicenter/wsdm/wsdm31mmi"
				xmlns:regexp="http://exslt.org/regular-expressions"	
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="dp"
				exclude-result-prefixes="dp dpconfig env11 env12 exsl regexp"
				version="1.0">


	<xsl:param name="dpconfig:MQ-URL" select="''" />
	<dp:param name="dpconfig:MQ-URL" type="dmURL" xmlns="">
		<display>MQ target URL</display>
		<description>
            Full mq url. For examlpe:
			dpmq://SOME-QM-GROUP.G-QMGR/?RequestQueue=SOME.QUEUE.NAME;PMO=2116
		</description>
		<tab-override>basic</tab-override>
		<default/>
		<required-when>
			<condition evaluation="logical-true" />
		</required-when>
	</dp:param>

	<xsl:template match="/">

		<xsl:variable name="sendmessage">
			<dp:url-open           
					  target="{$dpconfig:MQ-URL}"
					  response="responsecode-ignore">
				<xsl:copy-of select="."/>
			</dp:url-open> 
		</xsl:variable>
		
		<xsl:message dp:priority="info">
			<xsl:text>MQ url-open results: </xsl:text>
			<xsl:copy-of select="$sendmessage" />
		</xsl:message>

	</xsl:template>
</xsl:stylesheet>