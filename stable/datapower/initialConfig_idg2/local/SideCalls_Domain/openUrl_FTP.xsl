<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"   version="1.0" 
	xmlns:dp="http://www.datapower.com/extensions" 
	xmlns:dpconfig="http://www.datapower.com/param/config" 
	xmlns:ser="http://ServerManagmentWS/" 
	extension-element-prefixes="dp" 
	exclude-result-prefixes="dp dpconfig" >

	<xsl:output method="xml"/>

	<xsl:param name="dpconfig:URL" select="''" />
	<dp:param name="dpconfig:URL" type="dmURL" xmlns="">
		<display>Target URL</display>
		<description>
            Full ftp url. For examlpe:
			ftp://ftpuser:123456@192.168.72.133/test.txt?Encoding=base64
		</description>
		<tab-override>basic</tab-override>
		<default/>
		<required-when>
			<condition evaluation="logical-true" />
		</required-when>
	</dp:param>

	<xsl:template match="/">

<!--		<xsl:variable name="targetURL" select="'ftp://dpftp:123456@192.168.0.121/%2F/test.txt?Encoding=base64'" />   -->

		<xsl:variable name="targetURL" select="$dpconfig:URL"/>

		<xsl:variable name="ftp-result">
			<dp:url-open target="{$targetURL}" response="ignore">
				<xsl:copy-of select="."/>
			</dp:url-open>
		</xsl:variable>

		<xsl:message dp:priority="info">
			<xsl:text>FTP url-open results: </xsl:text>
			<xsl:copy-of select="$ftp-result"/>
		</xsl:message>

	</xsl:template>		
</xsl:stylesheet>