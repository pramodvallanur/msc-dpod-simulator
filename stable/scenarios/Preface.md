#Preface

The following section describes the steps for a full demonstration of **IBM(R) DataPower(R) Operations Dashboard** capabilities (shortly called DPOD). 
During the demonstration, we will walk through scenarios using the Web Console, while describing DPOD's features and strengths.

In order to demonstrate DPOD, you should setup a running DPOD simulation environment on your laptop.


Setting up DPOD Simulation Environment on your Laptop
This will include everything you need to demonstrate DPOD on your own laptop with 8 cores , 16 GB memory, 100 GB space.
Installing a fresh simulation environment on your laptop should take up to 1 hour. See installation instructions.

