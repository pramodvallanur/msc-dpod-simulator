#Scenarios List 

This is the list of Scenarios we prpose to show to customers.
You may present some of them based on time and customer preferneces.
After the first three sceanrios please consult customers audience how to proceed.

1.  [Preface](Preface.md)
2.  [Web Console Introduction](WebConsoleIntroduction.md)
3.  [Troubleshooting a back-end latency issue](TroubleshootingBack-endLatencyIssue.md) - [video](https://youtu.be/7JdQ9rsr5oo)
4.  [Troubleshooting a side-call latency issue](TroubleshootingSideCallLatencyIssue.md) - [video](https://youtu.be/cQrp39KpOV8)
5.  [Troubleshooting an API error as a Developer using self-service](TroubleshootingAPIwithAnErrorAsDeveloperUsingSelfService.md) - [video](https://youtu.be/fdCNXh72lL4)
6.  [Troubleshooting malfunctioning service](TroubleshootingMalfunctioningServices.md) - [video](https://youtu.be/NyVFi3NUavI) 
7.  [Troubleshooting a major out-of-service issue](TroubleshootingMajorOutOfServiceIssue.md) - [video]() 
9.  [APM / Splunk integration](APMSplunkIntegration.md) - TODO
10. [Maintenance plans: backup, sync, firmware upgrade](MaintenancePlansBackupSyncFirmwareUpgrade.md) - TODO
11. [DevOps Services Portal](DevOpsServicesPortal.md) - TODO
12. [Architecture](Architecture.md)  - Advise offering Management
13. [Roadmap](Roadmap.md) - Advise offering Management
