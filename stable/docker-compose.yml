version: "3.5"


#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0

#################################################################################################################


services:
  splunk:
    build:
      context: ./splunk
      dockerfile: Dockerfile
    image: splunk:dpodsimulator
    depends_on:
      - ${SPLUNK_DEPENDENCY}
    container_name: splunk_dpodsimulator
    networks:
      dpodsimulator_net:
        ipv4_address: 172.77.77.7
        aliases:
          - app_splunk
    volumes:
      - ../../../resources/${SIMULATOR_VERSION}/splunk/volumes_splunk/introspection.app/apps.conf:/opt/splunk/etc/apps/introspection_generator_addon/local/apps.conf
      - ../../../resources/${SIMULATOR_VERSION}/splunk/volumes_splunk/splunk_httpinput.app/inputs.conf:/opt/splunk/etc/apps/splunk_httpinput/local/inputs.conf
      - ../../../resources/${SIMULATOR_VERSION}/splunk/volumes_splunk/prefs/user-prefs.conf:/opt/splunk/etc/users/admin/user-prefs/local/user-prefs.conf
      - ../../../resources/${SIMULATOR_VERSION}/splunk/volumes_splunk/dpod.app:/opt/splunk/etc/apps/dpod
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "8000:8000"
      - "8088:8088"
      - "60030:60030"
      - "60031:60031"
    environment:
      - SPLUNK_START_ARGS=--accept-license
    cap_add:
      - SYS_RESOURCE
      - SYS_TIME
    devices:
      - /dev/rtc
    tty: true
    stdin_open: true
  idg2:
    build:
      context: ./datapower
      args:
        IDG_IMAGE_TAG: ${IDG2_IMAGE_TAG}
    image: idg2:dpodsimulator
    depends_on:
      - centos
    container_name: idg2_dpodsimulator
    networks:
      dpodsimulator_net:
        ipv4_address: 172.77.77.6
        aliases:
          - app_idg
    volumes:
      - ../../../resources/${SIMULATOR_VERSION}/datapower/volumes_idg2/config:/drouter/config
      - ../../../resources/${SIMULATOR_VERSION}/datapower/volumes_idg2/local:/drouter/local
    ports:
      - "9091:9090"
      - "9022:22"
      - "5551:5550"
    environment:
     - DATAPOWER_ACCEPT_LICENSE=true
     - DATAPOWER_INTERACTIVE=true
    tty: true
    stdin_open: true
  centos:
    build:
      context: ./centos
      dockerfile: ${CENTOS_DOCKERFILE}
      args:
        HTTP_PROXY_URL: ${HTTP_PROXY_URL}
        HTTPS_PROXY_URL: ${HTTPS_PROXY_URL}
    image: centos:dpodsimulator
    networks:
      dpodsimulator_net:
        ipv4_address: 172.77.77.5
        aliases:
          - app_centos
    depends_on:
      - dpod
    container_name: centos_dpodsimulator
    volumes:
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "8022:22"
    cap_add:
      - SYS_RESOURCE
      - SYS_TIME
    devices:
      - /dev/rtc
    tty: true
    stdin_open: true
  dpod:
    build:
      context: ./dpod
    image: dpod:dpodsimulator
    security_opt:
      - seccomp:unconfined
    cap_add:
      - SYS_ADMIN
      - SYS_RESOURCE
      - SYS_TIME
    devices:
      - /dev/rtc
    tmpfs:
      - /run
      - /tmp:exec
      - /run/lock
    volumes:
      - /sys/fs/cgroup:/sys/fs/cgroup:ro
      - /etc/localtime:/etc/localtime:ro
    networks:
      dpodsimulator_net:
        ipv4_address: 172.77.77.4
        aliases:
          - app_dpod
    depends_on:
      - idg1
    container_name: dpod_dpodsimulator
    ports:
      - "4433:443"
      - "4022:22"
      - "60000:60000"
      - "60020:60020"
    tty: true
#    privileged: true
    hostname: dpod
    stdin_open: true
  idg1:
    build:
      context: ./datapower
      args:
        IDG_IMAGE_TAG: "7.7.1"
    image: idg1:dpodsimulator
    depends_on:
      - mq
    container_name: idg1_dpodsimulator
    networks:
      dpodsimulator_net:
        ipv4_address: 172.77.77.3
        aliases:
          - app_idg
    volumes:
      - ../../../resources/${SIMULATOR_VERSION}/datapower/volumes_idg1/config:/drouter/config
      - ../../../resources/${SIMULATOR_VERSION}/datapower/volumes_idg1/local:/drouter/local
    ports:
      - "9090:9090"
      - "7022:22"
      - "5550:5550"
    environment:
     - DATAPOWER_ACCEPT_LICENSE=true
     - DATAPOWER_INTERACTIVE=true
    tty: true
    stdin_open: true
  mq:
    build:
      context: ./mq
    image: mq:dpodsimulator
    container_name: mq_dpodsimulator
    networks:
      dpodsimulator_net:
        ipv4_address: 172.77.77.2
        aliases:
          - app_mq
    ports:
      - "1414:1414"
      - "1422:22"
    environment:
      LICENSE: accept
      MQ_QMGR_NAME: QM1
      MQSNOAUT: "yes"
    tty: true
    stdin_open: true

networks:
  dpodsimulator_net:
    name: dpodsimulator_net
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 172.77.77.0/24
