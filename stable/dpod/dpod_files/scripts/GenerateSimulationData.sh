#!/bin/sh

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


 
#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -s|--sleep-duration -n|--idg-name -r|--run-sql"
	echo "	Or :  $0 -h|--help"
	echo " "
	echo "-s|--sleep-duration  	: Sleep duration in seconds for txGenerator        "	
	echo "-n|--idg-name  		: Name of the IDG                   "	
	echo "-r|--run-sql  		: Whether to run the sql files or not (true/false)                  "	
	echo "-h|--help      		: Display help                      "	
	echo " "
	return 0
}

###########################
# variables and constants #
###########################

NOW=$( date +"%F_%H-%M-%S")

SCRIPTS_DIR="/MonTier/scripts"

LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="GenerateSimulationData_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

ABORT=0
ADDITIONAL_IDG_FLAG_UP=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-i|--idg-service)	
			IDG_SERVICE_NAME=$2
			if [[ -z ${IDG_SERVICE_NAME} ]]; then
				echo "ERROR : Invalid parameters. IDG service name is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-n|--idg-name)	
			DP_NAME=$2
			if [[ -z ${DP_NAME} ]]; then
				echo "ERROR : Invalid parameters. DP name is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-s|--sleep-duration)	
			SLEEP_DURATION=$2
			if [[ -z ${SLEEP_DURATION} ]]; then
				echo "ERROR : Invalid parameters. Sleep duration is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-r|--run-sql)	
			RUN_SQL_FILES=$2
			if [[ -z ${RUN_SQL_FILES} ]]; then
				echo "ERROR : Invalid parameters. Run sql files is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


if [[ -z ${IDG_SERVICE_NAME} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--idg-service' is missing "
	exit 2
fi

if [[ -z ${DP_NAME} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--idg-name' is missing "
	exit 2
fi

if [[ -z ${SLEEP_DURATION} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--sleep-duration' is missing "
	exit 2
fi

if [[ -z ${RUN_SQL_FILES} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--run-sql' is missing "
	exit 2
fi

############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}

info "Activating txGenerator"
echo "`current_date_time`: INFO: Activating txGenerator"
cd "${SCRIPTS_DIR}/txGenerator_${IDG_SERVICE_NAME}"
nohup ./runTxGenerator.sh -n ${DP_NAME} -s ${SLEEP_DURATION} > "${LOG_DIR}/runTxGenerator_${DP_NAME}.log" 2>&1 &
if [ $? -ne 0 ]; then 
	error "Failed to activate txGenerator for device: ${DP_NAME}. Abort"
	echo "`current_date_time`: ERROR: Failed to activate txGenerator for device: ${DP_NAME}. Abort"
	ABORT=1
fi

if [[ ${ABORT} -eq 0 ]]; then
	if [[ ${RUN_SQL_FILES} == "true" ]]; then
		info "Running SQL files"
		echo "`current_date_time`: INFO: Running SQL files"
		cd ${SCRIPTS_DIR}/RunSqlCommands
		nohup ./RunSqlCommands.sh > ${LOG_DIR}/RunSqlCommands.log 2>&1 &
		if [ $? -ne 0 ]; then 
			error "Failed to run SQL files. Abort"
			echo "`current_date_time`: ERROR: Failed to run SQL files. Abort"
			ABORT=1
		fi
	fi
fi


exit ${ABORT}


