#!/bin/sh

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


 
#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -t|--tx-number -g|--global-tid -d|--device-name -b|--branch-value"
	echo "	Or :  $0 -h|--help"
	echo " "
	echo "-t|--tx-number  	    : The transaction id           "	
	echo "-g|--global-tid  	    : The global tid           "	
	echo "-d|--device-name  	: The idg device name           "	
	echo "-b|--branch-value  	: The value of the 'if' branch of the API : true/false           "	
	echo "-h|--help      		: Display help                      "	
	echo " "
	return 0
}

###########################
# variables and constants #
###########################

NOW=$( date +"%F_%H-%M-%S")

LOCAL_FILES_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
LOG_DIR="${LOCAL_FILES_DIR}/logs"
GENERATED_FILES_FOLDER="${LOCAL_FILES_DIR}/GeneratedFiles"
LOG_FILE_NAME="GeneratePolicyVariables_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

DPOD_USER="admin"
DPOD_PASS="adminuser"
DPOD_ADDRESS="172.77.77.4"
WSM_PORT="60020" 
SOAP_TEMPLATE_FILE="${LOCAL_FILES_DIR}/SOAPRequest.Template"
R_RECORDS_FILE_BRANCH_TRUE="${LOCAL_FILES_DIR}/R_Records_branchTrue.txt"
R_RECORDS_FILE_BRANCH_FALSE="${LOCAL_FILES_DIR}/R_Records_branchFalse.txt"
R_RECORDS_FILE="noChoice"

ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-t|--tx-id)	
			TID=$2
			if [[ -z ${TID} ]]; then
				echo "ERROR : Invalid parameters. Transaction number is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-g|--global-tid)	
			GTID=$2
			if [[ -z ${GTID} ]]; then
				echo "ERROR : Invalid parameters. Global transaction number is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-d|--device-name)	
			DEVICE_NAME=$2
			if [[ -z ${DEVICE_NAME} ]]; then
				echo "ERROR : Invalid parameters. Device name is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-b|--branch-value)	
			BRANCH_VALUE=$2
			if [[ -z ${BRANCH_VALUE} ]]; then
				echo "ERROR : Invalid parameters. Branch value is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


if [[ -z ${TID} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--tx-id' is missing "
	exit 2
fi

if [[ -z ${GTID} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--global-tid' is missing "
	exit 2
fi

if [[ -z ${DEVICE_NAME} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--device-name' is missing "
	exit 2
fi

if [[ -z ${BRANCH_VALUE} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--branch-value' is missing "
	exit 2
fi

if [ ! ${BRANCH_VALUE} == "true" ] && [ ! ${BRANCH_VALUE} == "false" ]; then
	echo "ERROR : Invalid parameters. Mandatory parameter '--branch-value' value '${BRANCH_VALUE}' is not valid. Must be one of:  'true' , 'false'"
	exit 2
fi

############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}
mkdir -p ${GENERATED_FILES_FOLDER}


info "Getting current epoch time"
echo "`current_date_time`: INFO: Getting current epoch time"
CURRENT_EPOCH_TIME=`date +%s%N | cut -b1-13`

DPOD_URL="http://${DPOD_ADDRESS}:${WSM_PORT}/logTarget?deviceName=${DEVICE_NAME}&domainName=simulator"

REQUEST_SERIAL_NUM=1

if [[ ${BRANCH_VALUE} == "true" ]]; then
	R_RECORDS_FILE=${R_RECORDS_FILE_BRANCH_TRUE}
else
	R_RECORDS_FILE=${R_RECORDS_FILE_BRANCH_FALSE}
fi

while read p; do
	
	info "Creating SOAP message ${REQUEST_SERIAL_NUM}"
	echo "`current_date_time`: INFO: Creating SOAP message ${REQUEST_SERIAL_NUM}"
	REQUEST_FILE="${GENERATED_FILES_FOLDER}/SOAPRequest_${REQUEST_SERIAL_NUM}.xml"
	CURL_RESPONSE_FILE="${GENERATED_FILES_FOLDER}/SOAPResponse_${REQUEST_SERIAL_NUM}.xml"
	
	cp ${SOAP_TEMPLATE_FILE} ${REQUEST_FILE}
	sed -i "s/__SERIAL__/${REQUEST_SERIAL_NUM}/g" ${REQUEST_FILE}
	sed -i "s/__UTC_TIME__/${CURRENT_EPOCH_TIME}/g" ${REQUEST_FILE}
	sed -i "s/__TID__/${TID}/g" ${REQUEST_FILE}
	sed -i "s/__GTID__/${GTID}/g" ${REQUEST_FILE}
	sed -i "s/__R_RECORD__/${p}/g" ${REQUEST_FILE}
	
	info "Sending SOAP message"
	echo "`current_date_time`: INFO: Sending SOAP message"
	curl --user ${DPOD_USER}:${DPOD_PASS} -H "Content-Type: text/xml;charset=UTF-8" -X POST -d "@${REQUEST_FILE}" --insecure -w "\n" ${DPOD_URL} > ${CURL_RESPONSE_FILE}
	if [ $? -ne 0 ]; then 
		info "Failed to send SOAP message ${REQUEST_FILE}."
		echo "`current_date_time`: INFO: Failed to send SOAP message ${REQUEST_FILE}."
		ABORT=1
	fi

	# CURL_SUCCESS_INDICATION=`grep "\"resultCode\":\"SUCCESS\"" ${CURL_RESPONSE_FILE} | wc -l`
	# if [[ ${CURL_SUCCESS_INDICATION} -eq 0 ]]; then 
		# error "DPOD response did not contain a success indication."
		# echo "`current_date_time`: ERROR: DPOD response did not contain a success indication."
		# ABORT=1
	# else
		# CURL_SUCCESS_INDICATION=1
	# fi
	
	REQUEST_SERIAL_NUM=$((REQUEST_SERIAL_NUM+1))
	
done < ${R_RECORDS_FILE}



info "Finished sending all SOAP messages"
echo "`current_date_time`: INFO: Finished sending all SOAP messages"

exit ${ABORT}
