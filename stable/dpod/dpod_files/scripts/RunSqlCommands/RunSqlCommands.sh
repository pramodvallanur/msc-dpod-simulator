#!/bin/sh

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


 
#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0"
	echo "	Or :  $0 -h|--help"
	echo " "
	return 0
}


###########################
# variables and constants #
###########################

NOW=$( date +"%F_%H-%M-%S")

SCRIPTS_DIR="/MonTier/scripts/RunSqlCommands"

LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="RunSqlCommands_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

SLQ_DIR="${SCRIPTS_DIR}/SqlFiles/*"
SLQ_RESPONSE_DIR="${SCRIPTS_DIR}/SqlResponses"

ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}
mkdir -p ${SLQ_RESPONSE_DIR}

for SQL_FILE in ${SLQ_DIR}
do
	if [[ ${ABORT} -eq 0 ]]; then
		
		SQL_FILENAME=`basename ${SQL_FILE}`
	
		SQL_RESPONSE="${SLQ_RESPONSE_DIR}/${SQL_FILENAME}_Response.xml"		
		info "Running SQL commands: ${SQL_FILENAME}"
		echo "`current_date_time`: INFO: Running SQL commands: ${SQL_FILENAME}"
		ij < ${SQL_FILE} > ${SQL_RESPONSE} 2>&1
		if [ $? -ne 0 ]; then 
			error "Failed to run SQL commands: ${SQL_FILENAME}. Abort"
			echo "`current_date_time`: ERROR: Failed to run SQL commands: ${SQL_FILENAME}. Abort"
			ABORT=1
		fi
	fi
done

info "Finished running all SQL files"
echo "`current_date_time`: INFO: Finished running all SQL files"

exit ${ABORT}