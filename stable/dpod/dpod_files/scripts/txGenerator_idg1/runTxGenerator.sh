#!/bin/bash

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


 
#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo " "
	echo $"Usage: $0 -s|--sleep-duration -n|--idg-name"
	echo "	Or :  $0 -h|--help"
	echo " "
	echo "-s|--sleep-duration  	: Sleep duration in seconds         "	
	echo "-n|--idg-name  		: Name of the IDG                   "	
	echo "-h|--help      		: Display help                      "	
	echo " "
	return 0
}

###########################
# variables and constants #
###########################


NOW=$( date +"%F_%H-%M-%S")

LOCAL_FILES_DIR="/MonTier/scripts/txGenerator_idg1"

LOG_DIR="${LOCAL_FILES_DIR}/logs"
LOG_FILE_NAME="runTxGenerator_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

JAVA_COMMAND="java -jar ${LOCAL_FILES_DIR}/MonTierGenerator.jar"
TIME_TO_SLEEP=300

TEMPLATE_FILE="${LOCAL_FILES_DIR}/Template_main.properties"
MAIN_PROP_FILE="${LOCAL_FILES_DIR}/main.properties"

ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########


while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-s|--sleep-duration)	
			TIME_TO_SLEEP=$2
			if [[ -z ${TIME_TO_SLEEP} ]]; then
				echo "ERROR : Invalid parameters. Sleep duration is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-n|--idg-name)	
			IDG_NAME=$2
			if [[ -z ${IDG_NAME} ]]; then
				echo "ERROR : Invalid parameters. Device name is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done


if [[ -z ${TIME_TO_SLEEP} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--sleep-duration' is missing "
	exit 2
fi

if [[ -z ${IDG_NAME} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--idg-name' is missing "
	exit 2
fi

############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}

info "Creating main.properties from template"
echo "`current_date_time`: INFO: Creating main.properties from template"
cp ${TEMPLATE_FILE} ${MAIN_PROP_FILE}
sed -i "s/__DEVICE__/${IDG_NAME}/g" ${MAIN_PROP_FILE}
if [ $? -ne 0 ]; then
	error "Failed to create main.properties. Abort"
	echo "`current_date_time`: ERROR: Failed to create main.properties. Abort"
	ABORT=1
fi

cd ${LOCAL_FILES_DIR}

while [[ ${ABORT} -eq 0 ]]; do
	info "Running TxGenerator : ${JAVA_COMMAND}"
	echo "`current_date_time`: INFO: Running TxGenerator: ${JAVA_COMMAND}"
	${JAVA_COMMAND}
	if [ $? -ne 0 ]; then
		error "Failed to run TxGenerator. Abort"
		echo "`current_date_time`: ERROR: Failed to run TxGenerator. Abort"
		ABORT=1
	else
		sleep ${TIME_TO_SLEEP}
	fi
done

exit ${ABORT}
