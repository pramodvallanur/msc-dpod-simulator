#!/bin/bash

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0

#################################################################################################################


#############
# functions #
#############


function current_date_time {
	echo `date +%F_%H-%M-%S`
}


function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}

	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}


function resync_ntp() {

	local RESYNC_NTP_RC=0
	local LOW_DIST=`lowercase ${DIST}`
	local NTP_SERVICE_NAME="ntpd"

	info "Resyncing ntp"
	echo "`current_date_time`: INFO: Resyncing ntp"

	if [[ ${LOW_DIST} == "ubuntu" ]]; then
		NTP_SERVICE_NAME="ntp"
	fi

	info "Stopping ${NTP_SERVICE_NAME}"
	systemctl stop ${NTP_SERVICE_NAME} >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		RESYNC_NTP_RC=1
		warn "Failed to stop ${NTP_SERVICE_NAME} service."
		echo "`current_date_time`: WARN: Failed to stop ${NTP_SERVICE_NAME} service."
	fi

	info "Syncing ntp manually"
	ntpd -gq >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		RESYNC_NTP_RC=1
		warn "Failed to sync ntp."
		echo "`current_date_time`: WARN: Failed to sync ntp."
	fi

	info "Starting ${NTP_SERVICE_NAME}"
	systemctl start ${NTP_SERVICE_NAME} >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		RESYNC_NTP_RC=1
		warn "Failed to start ${NTP_SERVICE_NAME} service."
		echo "`current_date_time`: WARN: Failed to start ${NTP_SERVICE_NAME} service."
	fi

	if [[ ${RESYNC_NTP_RC} -eq 1 ]]; then
		warn "You may need to sync ntp manually."
		echo "`current_date_time`: WARN: You may need to sync ntp manually."
	fi

	return ${RESYNC_NTP_RC}
}


function disk_space_check() {

	local DISK_SPACE_CHECK_RC=0
	local DOCKER_RUNTIME_DIR="/var/lib/docker"

	local NEDDED_DISK_FOR_DOCKER_GB=25
	local NEDDED_DISK_FOR_DOCKER_KB=$(( 1000000 * NEDDED_DISK_FOR_DOCKER_GB ))

	local NEDDED_DISK_FOR_RESOURCES_GB=2
	local NEDDED_DISK_FOR_RESOURCES_KB=$(( 1000000 * NEDDED_DISK_FOR_RESOURCES_GB ))

	if [[ "mac" = "${OS}" ]]; then
		DOCKER_RUNTIME_DIR="${HOME}/Library"
	fi

	info "Disk Space check - CRITICAL"
	info "============================"
	echo " "
	echo "`current_date_time`: INFO: Disk Space check - CRITICAL"
	echo "`current_date_time`: INFO: ==========================="

	AVAILABLE_DISK_FOR_DOCKER_KB=`df -k ${DOCKER_RUNTIME_DIR} | grep -iv filesystem | awk '{print $4}'`
	AVAILABLE_DISK_FOR_RESOURCE_DIR_KB=`df -k ${RESOURCES_DIR} | grep -iv filesystem | awk '{print $4}'`

	AVAILABLE_DISK_FOR_DOCKER=`df -h ${DOCKER_RUNTIME_DIR} | grep -iv filesystem | awk '{print $4}'`
	AVAILABLE_DISK_FOR_RESOURCE_DIR=`df -h ${RESOURCES_DIR} | grep -iv filesystem | awk '{print $4}'`

	info "Checking for disk space for docker components directory ${DOCKER_RUNTIME_DIR}. Needed ${NEDDED_DISK_FOR_DOCKER_GB}G, Found ${AVAILABLE_DISK_FOR_DOCKER}"
	echo "`current_date_time`: INFO: Checking for disk space for docker components cores. Needed ${NEDDED_DISK_FOR_DOCKER_GB}G, Found ${AVAILABLE_DISK_FOR_DOCKER}"
	if [[ ${AVAILABLE_DISK_FOR_DOCKER_KB} -lt ${NEDDED_DISK_FOR_DOCKER_KB} ]]; then
		error "System resources error: Insufficient disk space for docker components. check: Failed"
		echo "`current_date_time`: ERROR: System resources error: Insufficient disk space for docker components. check: Failed"
		DISK_SPACE_CHECK_RC=1
		ABORT=1
	else
		info "Found sufficient disk space for docker components. check: Passed"
		echo "`current_date_time`: INFO: Found sufficient disk space for docker components. check: Passed"
	fi

	info "Checking for disk space for resources directory ${RESOURCES_DIR}. Needed ${NEDDED_DISK_FOR_RESOURCES_GB}G, Found ${AVAILABLE_DISK_FOR_RESOURCE_DIR}"
	echo "`current_date_time`: INFO: Checking for disk space for docker components cores. Needed ${NEDDED_DISK_FOR_RESOURCES_GB}G, Found ${AVAILABLE_DISK_FOR_RESOURCE_DIR}"
	if [[ ${AVAILABLE_DISK_FOR_RESOURCE_DIR_KB} -lt ${NEDDED_DISK_FOR_RESOURCES_KB} ]]; then
		error "Insufficient disk space for resources. check: Failed"
		echo "`current_date_time`: ERROR: Insufficient disk space for resources. check: Failed"
		DISK_SPACE_CHECK_RC=1
		ABORT=1
	else
		info "Found sufficient disk space for resources. check: Passed"
		echo "`current_date_time`: INFO: Found sufficient disk space for resources. check: Passed"
	fi

	return ${DISK_SPACE_CHECK_RC}
}


function set_system_time_from_hw_clock() {

	local SET_SYSTEM_TIME_FROM_HW_CLOCK_RC=0

	info "Setting system time from hardware clock"
	echo "`current_date_time`: INFO: Setting system time from hardware clock"

	docker exec -it ${CENTOS_CONTAINER} hwclock -s >> ${LOG_FILE} 2>&1
	docker exec -it ${DPOD_CONTAINER} hwclock -s >> ${LOG_FILE} 2>&1

	if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
		docker exec -it ${SPLUNK_CONTAINER} hwclock -s >> ${LOG_FILE} 2>&1
	fi

	return ${SET_SYSTEM_TIME_FROM_HW_CLOCK_RC}
}


function download_with_md5_test() {

	local DOWNLOAD_WITH_MD5_TEST_RC=0
	local MD5_LOCAL_FILE=$1
	local MD5_URL=$2
	local ARTIFACT_LOCAL_FILE=$3
	local ARTIFACT_URL=$4
	local ARTIFACT_NAME=$5

	info "Downloading ${ARTIFACT_NAME} md5 file"
	echo "`current_date_time`: INFO: Downloading ${ARTIFACT_NAME} md5 file"
	rm -f ${MD5_LOCAL_FILE}

	if [[ "mac" = "${OS}" ]]; then
		curl -s -L -o ${MD5_LOCAL_FILE} ${MD5_URL} >> ${LOG_FILE} 2>&1
	else
		wget -q --output-document=${MD5_LOCAL_FILE} ${MD5_URL} >> ${LOG_FILE} 2>&1
	fi

	if [ $? -ne 0 ]; then
		error "Failed to Download ${ARTIFACT_NAME} md5 file. This may be a network communication issue. Abort."
		error "Try downloading it manually, and run the 'build' action again."
		echo "`current_date_time`: ERROR: Failed to Download ${ARTIFACT_NAME} md5 file. This may be a network communication issue. Abort."
		echo "`current_date_time`: ERROR: 	Try downloading it manually, and run the 'build' action again."
		if [[ "mac" = "${OS}" ]]; then
			error "Manual download commands :  'cd ${LOCAL_FILES_DIR}; curl -s -L -o ${MD5_LOCAL_FILE} ${MD5_URL}' "
			echo "`current_date_time`: ERROR: 	Manual download command is :  'cd ${LOCAL_FILES_DIR}; curl -s -L -o ${MD5_LOCAL_FILE} ${MD5_URL}' "
		else
			error "Manual download command is :  'cd ${LOCAL_FILES_DIR}; wget -q --output-document=${MD5_LOCAL_FILE} ${MD5_URL}' "
			echo "`current_date_time`: ERROR: 	Manual download command is :  'cd ${LOCAL_FILES_DIR}; wget -q --output-document=${MD5_LOCAL_FILE} ${MD5_URL}' "
		fi
		error "This may also accur if the version of 'dpodsimulator' used, is outdated. Please try to download the 'dpodsimulator' again."
		echo "`current_date_time`: ERROR: 	This may also accur if the version of 'dpodsimulator' used, is outdated. Please try to download the 'dpodsimulator' again."
		DOWNLOAD_WITH_MD5_TEST_RC=1
		ABORT=1
	fi

	if [[ ${DOWNLOAD_WITH_MD5_TEST_RC} -eq 0 ]]; then
		info "Comparing local ${ARTIFACT_NAME} md5 signature to the value in md5 file"
		echo "`current_date_time`: INFO: Comparing local ${ARTIFACT_NAME} md5 signature to the value in md5 file"
		test_md5_signature ${ARTIFACT_LOCAL_FILE} ${MD5_LOCAL_FILE}
		if [ $? -ne 0 ]; then
			DOWNLOAD_WITH_MD5_TEST_RC=1
			ABORT=1
		fi
	fi

	if [[ ${DOWNLOAD_WITH_MD5_TEST_RC} -eq 0 ]]; then
		if [[ ${MD5_SIG_MATCH} -eq 0 ]]; then
			info "Local ${ARTIFACT_NAME} md5 signature does not match the value in md5 file."
			echo "`current_date_time`: INFO: Local ${ARTIFACT_NAME} md5 signature does not match the value in md5 file."

			info "Downloading ${ARTIFACT_NAME}. This may take up to 15 minutes."
			echo "`current_date_time`: INFO: Downloading ${ARTIFACT_NAME}. This may take up to 15 minutes."
			if [[ "mac" = "${OS}" ]]; then
				curl -s -L -o ${ARTIFACT_LOCAL_FILE} ${ARTIFACT_URL} >> ${LOG_FILE} 2>&1
			else
				wget -q --output-document=${ARTIFACT_LOCAL_FILE} ${ARTIFACT_URL} >> ${LOG_FILE} 2>&1
			fi

			if [ $? -ne 0 ]; then
				error "Failed to Download ${ARTIFACT_NAME}. This may be a network communication issue. Abort."
				error "Try downloading it manually, and run the 'build' action again."
				echo "`current_date_time`: ERROR: Failed to Download ${ARTIFACT_NAME}. This may be a network communication issue. Abort."
				echo "`current_date_time`: ERROR: 	Try downloading it manually, and run the 'build' action again."
				if [[ "mac" = "${OS}" ]]; then
					error "Manual download command is :  'cd ${LOCAL_FILES_DIR}; curl -s -L -o ${ARTIFACT_LOCAL_FILE} ${ARTIFACT_URL}' "
					echo "`current_date_time`: ERROR: 	Manual download command is :  'cd ${LOCAL_FILES_DIR}; curl -s -L -o ${ARTIFACT_LOCAL_FILE} ${ARTIFACT_URL}' "
				else
					error "Manual download command is :  'cd ${LOCAL_FILES_DIR}; wget -q --output-document=${ARTIFACT_LOCAL_FILE} ${ARTIFACT_URL}' "
					echo "`current_date_time`: ERROR: 	Manual download command is :  'cd ${LOCAL_FILES_DIR}; wget -q --output-document=${ARTIFACT_LOCAL_FILE} ${ARTIFACT_URL}' "
				fi
				error "This may also accur if the version of 'dpodsimulator' used, is outdated. Please try to download the 'dpodsimulator' again."
				echo "`current_date_time`: ERROR: 	This may also accur if the version of 'dpodsimulator' used, is outdated. Please try to download the 'dpodsimulator' again."
				DOWNLOAD_WITH_MD5_TEST_RC=1
				ABORT=1
			fi

			info "Validating new ${ARTIFACT_NAME} md5"
			echo "`current_date_time`: INFO: Validating new ${ARTIFACT_NAME} md5"
			test_md5_signature ${ARTIFACT_LOCAL_FILE} ${MD5_LOCAL_FILE}
			if [ $? -ne 0 ]; then
				DOWNLOAD_WITH_MD5_TEST_RC=1
				ABORT=1
			fi

			if [[ ${DOWNLOAD_WITH_MD5_TEST_RC} -eq 0 ]]; then
				if [[ ${MD5_SIG_MATCH} -eq 0 ]]; then
					error "New ${ARTIFACT_NAME} md5 signature does not match the value in md5 file. This may be a network communication issue. Abort."
					error "Try downloading it manually, and run the 'build' action again."
					echo "`current_date_time`: ERROR: New ${ARTIFACT_NAME} md5 signature does not match the value in md5 file. This may be a network communication issue. Abort."
					echo "`current_date_time`: ERROR: 	Try downloading it manually, and run the 'build' action again."
					if [[ "mac" = "${OS}" ]]; then
						error "Manual download command is :  'cd ${LOCAL_FILES_DIR}; curl -s -L -o ${ARTIFACT_LOCAL_FILE} ${ARTIFACT_URL}' "
						echo "`current_date_time`: ERROR: 	Manual download command is :  'cd ${LOCAL_FILES_DIR}; curl -s -L -o ${ARTIFACT_LOCAL_FILE} ${ARTIFACT_URL}' "
					else
						error "Manual download command is :  'cd ${LOCAL_FILES_DIR}; wget -q --output-document=${ARTIFACT_LOCAL_FILE} ${ARTIFACT_URL}' "
						echo "`current_date_time`: ERROR: 	Manual download command is :  'cd ${LOCAL_FILES_DIR}; wget -q --output-document=${ARTIFACT_LOCAL_FILE} ${ARTIFACT_URL}' "
					fi
					DOWNLOAD_WITH_MD5_TEST_RC=1
					ABORT=1
				else
					info "New ${ARTIFACT_NAME} md5 signature matches the value in md5 file. New ${ARTIFACT_NAME} will be used."
					echo "`current_date_time`: INFO: New ${ARTIFACT_NAME} md5 signature matches the value in md5 file. New ${ARTIFACT_NAME} will be used."
				fi
			fi

		else
			info "Local ${ARTIFACT_NAME} md5 signature matches the value in md5 file. Local ${ARTIFACT_NAME} will be used."
			echo "`current_date_time`: INFO: Local ${ARTIFACT_NAME} md5 signature matches the value in md5 file. Local ${ARTIFACT_NAME} will be used."
		fi
	fi

	return ${DOWNLOAD_WITH_MD5_TEST_RC}
}

function save_user_input_to_file() {

	local SAVE_USER_INPUT_TO_FILE_RC=0

	rm -f ${USER_INPUT_FILE} >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		error "Failed delete user input file. Abort"
		echo "`current_date_time`: ERROR: Failed delete user input file. Abort"
		SAVE_USER_INPUT_TO_FILE_RC=1
		ABORT=1
	fi

	info "Saving build properties to file"
	echo "`current_date_time`: INFO: Saving build properties to file"
	if [[ ${SAVE_USER_INPUT_TO_FILE_RC} -eq 0 ]]; then
		echo "ADDITIONAL_IDG_FLAG_UP:${ADDITIONAL_IDG_FLAG_UP}" >> ${USER_INPUT_FILE} 2>> ${LOG_FILE}
		echo "SPLUNK_FLAG_UP:${SPLUNK_FLAG_UP}" >> ${USER_INPUT_FILE} 2>> ${LOG_FILE}
		echo "CHOSEN_HOST_ADDRESS:${CHOSEN_HOST_ADDRESS}" >> ${USER_INPUT_FILE} 2>> ${LOG_FILE}
		echo "NUMBER_OF_IDG_SERVICES_TO_HANDLE:${NUMBER_OF_IDG_SERVICES_TO_HANDLE}" >> ${USER_INPUT_FILE} 2>> ${LOG_FILE}
	fi

	return ${SAVE_USER_INPUT_TO_FILE_RC}
}


function read_user_input_from_file() {

	local READ_USER_INPUT_FROM_FILE_RC=0
	local ADDITIONAL_IDG_FLAG_UP_FROM_FILE=2
	local SPLUNK_FLAG_UP_FROM_FILE=2
	local CHOSEN_HOST_ADDRESS_FROM_FILE=2
	local NUMBER_OF_IDG_SERVICES_FROM_FILE=0

	if [[ ! -f ${USER_INPUT_FILE} ]]; then
		error "User input file does not exist. Abort"
		echo "`current_date_time`: ERROR: User input file does not exist. Abort"
		READ_USER_INPUT_FROM_FILE_RC=1
		ABORT=1
	fi

	if [[ ${READ_USER_INPUT_FROM_FILE_RC} -eq 0 ]]; then
		ADDITIONAL_IDG_FLAG_UP_FROM_FILE=`grep ADDITIONAL_IDG_FLAG_UP ${USER_INPUT_FILE} | awk -F':' '{ print $2 }'`
		SPLUNK_FLAG_UP_FROM_FILE=`grep SPLUNK_FLAG_UP ${USER_INPUT_FILE} | awk -F':' '{ print $2 }'`
		CHOSEN_HOST_ADDRESS_FROM_FILE=`grep CHOSEN_HOST_ADDRESS ${USER_INPUT_FILE} | awk -F':' '{ print $2 }'`
		NUMBER_OF_IDG_SERVICES_FROM_FILE=`grep NUMBER_OF_IDG_SERVICES_TO_HANDLE ${USER_INPUT_FILE} | awk -F':' '{ print $2 }'`
	fi

	if [ ! "${ADDITIONAL_IDG_FLAG_UP_FROM_FILE}" == "0" ] && [ ! "${ADDITIONAL_IDG_FLAG_UP_FROM_FILE}" == "1" ] ; then
		error "Failed to read value for '-i' flag from file. Abort"
		echo "`current_date_time`: ERROR: Failed to read value for '-i' flag from file. Abort"
		READ_USER_INPUT_FROM_FILE_RC=1
		ABORT=1
	else
		if [[ ${ADDITIONAL_IDG_FLAG_UP_FROM_FILE} -eq 1 ]]; then
			info "The 'build' action was run with a '-i' flag. "
			echo "`current_date_time`: INFO: The 'build' action was run with a '-i' flag."
			if [[ ${ACTION} == "start" ]]; then
				info "The 'start' action will start an additional idg service."
				echo "`current_date_time`: INFO: The 'start' action will start an additional idg service."
			fi
		fi
		ADDITIONAL_IDG_FLAG_UP=${ADDITIONAL_IDG_FLAG_UP_FROM_FILE}
	fi

	if [ ! "${SPLUNK_FLAG_UP_FROM_FILE}" == "0" ] && [ ! "${SPLUNK_FLAG_UP_FROM_FILE}" == "1" ] ; then
		error "Failed to read value for '-k' flag from file. Abort"
		echo "`current_date_time`: ERROR: Failed to read value for '-k' flag from file. Abort"
		READ_USER_INPUT_FROM_FILE_RC=1
		ABORT=1
	else
		if [[ ${SPLUNK_FLAG_UP_FROM_FILE} -eq 1 ]]; then
			info "The 'build' action was run with a '-k' flag."
			echo "`current_date_time`: INFO: The 'build' action was run with a '-k' flag."
			if [[ ${ACTION} == "start" ]]; then
				info "The 'start' action will start a splunk service."
				echo "`current_date_time`: INFO: The 'start' action will start a splunk service."
			fi
		fi
		SPLUNK_FLAG_UP=${SPLUNK_FLAG_UP_FROM_FILE}
	fi

	if [ "${CHOSEN_HOST_ADDRESS_FROM_FILE}" == "2" ] || [ -z ${CHOSEN_HOST_ADDRESS_FROM_FILE} ] ; then
		error "Failed to read value that user chose for host address. Abort"
		echo "`current_date_time`: ERROR: Failed to read value that user chose for host address. Abort"
		READ_USER_INPUT_FROM_FILE_RC=1
		ABORT=1
	else
		info "User chose host address ${CHOSEN_HOST_ADDRESS_FROM_FILE} during 'build' action. "
		echo "`current_date_time`: INFO: User chose host address ${CHOSEN_HOST_ADDRESS_FROM_FILE} during 'build' action. "
		if [[ ${ACTION} == "start" ]]; then
			info "Using this address for Operations Dashboard Web UI."
			echo "`current_date_time`: INFO: Using this address for Operations Dashboard Web UI."
		fi
		CHOSEN_HOST_ADDRESS=${CHOSEN_HOST_ADDRESS_FROM_FILE}
	fi

	if [  "${NUMBER_OF_IDG_SERVICES_FROM_FILE}" == "0" ] && [ -z ${NUMBER_OF_IDG_SERVICES_FROM_FILE} ] ; then
		error "Failed to read value the number of idg services from file."
		echo "`current_date_time`: ERROR: Failed to read value the number of idg services from file."
		READ_USER_INPUT_FROM_FILE_RC=1
		ABORT=1
	else
		info "The 'build' action was run with ${NUMBER_OF_IDG_SERVICES_FROM_FILE} idg services. "
#		echo "`current_date_time`: INFO: The 'build' action was run with ${NUMBER_OF_IDG_SERVICES_FROM_FILE} idg services. "
		if [[ ${ACTION} == "start" ]]; then
			info "The 'start' action will start all of those services."
#			echo "`current_date_time`: INFO: The 'start' action will start all of those services."
		fi
		NUMBER_OF_IDG_SERVICES_TO_HANDLE=${NUMBER_OF_IDG_SERVICES_FROM_FILE}
	fi

	return ${READ_USER_INPUT_FROM_FILE_RC}
}


function handle_iptables() {

	local HANDLE_IPTABLES_RC=0

	info "Checking iptables rules for DPOD syslog agents"
	echo "`current_date_time`: INFO: Checking iptables rules for DPOD syslog agents"
	iptables -C INPUT -p tcp -m tcp -s 0.0.0.0/0 -d 0.0.0.0/0 --dport 60000 -j ACCEPT >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		info "Adding DPOD syslog agents ports to iptables"
		echo "`current_date_time`: INFO: Adding DPOD syslog agents ports to iptables"
		iptables -I INPUT 1  -p tcp -m tcp -s 0.0.0.0/0 -d 0.0.0.0/0 --dport 60000 -j ACCEPT >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to Add DPOD syslog agents ports to iptables. Abort"
			echo "`current_date_time`: ERROR: Failed to Add DPOD syslog agents ports to iptables. Abort"
			HANDLE_IPTABLES_RC=1
			ABORT=1
		fi
	else
		info "iptables rules for DPOD syslog already exist"
		echo "`current_date_time`: INFO: iptables rules for DPOD syslog already exist"
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Checking iptables rules for DPOD wsm agents"
		echo "`current_date_time`: INFO: Checking iptables rules for DPOD wsm agents"
		iptables -C INPUT -p tcp -m tcp -s 0.0.0.0/0 -d 0.0.0.0/0 --dport 60020 -j ACCEPT >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			info "Adding DPOD wsm agents ports to iptables"
			echo "`current_date_time`: INFO: Adding DPOD wsm agents ports to iptables"
			iptables -I INPUT 1 -p tcp -m tcp -s 0.0.0.0/0 -d 0.0.0.0/0 --dport 60020 -j ACCEPT >> ${LOG_FILE} 2>&1
			if [ $? -ne 0 ]; then
				error "Failed to Add DPOD wsm agents ports to iptables. Abort"
				echo "`current_date_time`: ERROR: Failed to Add DPOD wsm agents ports to iptables. Abort"
				HANDLE_IPTABLES_RC=1
				ABORT=1
			fi
		else
			info "iptables rules for DPOD wsm already exist"
			echo "`current_date_time`: INFO: iptables rules for DPOD wsm already exist"
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Saving iptables config"
		echo "`current_date_time`: INFO: Saving iptables config"
		iptables-save >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to save iptables config. Abort"
			echo "`current_date_time`: ERROR: Failed to save iptables config. Abort"
			HANDLE_IPTABLES_RC=1
			ABORT=1
		fi
	fi

	return ${HANDLE_IPTABLES_RC}
}


function ntp_check() {

	# This function checks the following :
	# 1. ntp is installed on system
	# 2. ntp client is configured
	# 3. ntp service is enabled
	# 4. ntp service is running
	#
	#
	# returns following rc:
	# 0 - all valid
	# 1 - ntp is not installed
	# 2 - ntp client is not cnfigured
	# 3 - ntp service is not enabled or / and not running

	local NTP_INSTALLED=0
	local NTP_ENABLED=0
	local NTP_RUNNING=0
	local NTP_CHECK_RC=0
	local NTP_CONF_FILE="/etc/ntp.conf"
	local CHECK_FAILED=0
	local LOW_DIST=`lowercase ${DIST}`
	local NTP_CONFIGURED=0

	info "NTP check - CRITICAL"
	info "===================="
	echo " "
	echo "`current_date_time`: INFO: NTP check - CRITICAL"
	echo "`current_date_time`: INFO: ===================="


	### Find NTP_INSTALLED/NTP_CONFIGURED/NTP_RUNNING/NTP_ENABLED ###

	## This is for macOS
	if [[ "mac" = "${OS}" ]]; then
		NTP_INSTALLED=1
		NTP_CONFIGURED=`systemsetup -getnetworktimeserver | grep "Network Time Server: " | grep -v null | wc -l` >> ${LOG_FILE} 2>&1
		NTP_RUNNING=`systemsetup -getusingnetworktime | grep "Network Time: On" | wc -l` >> ${LOG_FILE} 2>&1
		NTP_ENABLED=1

	else
	## This is for all others (ubuntu/centos/red hat)

		NTP_CONFIGURED=`cat ${NTP_CONF_FILE} 2>/dev/null | grep -v "^$\|^\s*\#" |grep "iburst" |wc -l` >> ${LOG_FILE} 2>&1

		if [[ ${LOW_DIST} == "ubuntu" ]]; then
			NTP_INSTALLED=`apt list --installed 2>/dev/null | grep -i "^ntp" | wc -l` >> ${LOG_FILE} 2>&1
		else
			NTP_INSTALLED=`rpm -qa |grep "^ntp-" |wc -l` >> ${LOG_FILE} 2>&1
		fi

		# Check if this is a systemd based OS
		SYSTEM_STARTUP_TYPE=`ls -l /proc/1/exe | awk '{n=split($NF, N, "/"); { print N[n] }}'` >> ${LOG_FILE} 2>&1

		# check if this is a systemd based OS (CENTOS / REDHAT 7)
		if [[ "${SYSTEM_STARTUP_TYPE}" = "systemd" ]]; then
			if [[ ${LOW_DIST} == "ubuntu" ]]; then
				NTP_ENABLED=`systemctl list-unit-files | grep ntp.service | awk {'print $2'} | head -n1` >> ${LOG_FILE} 2>&1
				NTP_RUNNING=`systemctl status ntp.service |grep Active:|grep running|wc -l` >> ${LOG_FILE} 2>&1
			else
				NTP_ENABLED=`systemctl list-unit-files | grep ntpd.service | awk {'print $2'} | head -n1` >> ${LOG_FILE} 2>&1
				NTP_RUNNING=`systemctl status ntpd.service |grep Active:|grep running|wc -l` >> ${LOG_FILE} 2>&1
			fi
		else
			NTP_ENABLED=`chkconfig --list ntpd 2>/dev/null|wc -l` >> ${LOG_FILE} 2>&1
			if [[ ${NTP_ENABLED} -gt 0 ]]; then
				NTP_ENABLED="enabled"
			else
				NTP_ENABLED="disabled"
			fi

			NTP_RUNNING=`service ntpd status 2>/dev/null|grep running | wc -l` >> ${LOG_FILE} 2>&1
		fi
	fi

	### Make dessicions according to NTP_INSTALLED/NTP_CONFIGURED/NTP_RUNNING/NTP_ENABLED values ###

	if [[ ${NTP_INSTALLED} -gt 0 ]]; then
		info "NTP software is installed. check: Passed"
		echo "`current_date_time`: INFO: NTP software is installed. check: Passed"
	else
		error "NTP software is not installed. check: Failed"
		echo "`current_date_time`: ERROR: NTP software is not installed. check: Failed"
		if [[ ${CHECK_FAILED} -eq 0 ]]; then
			NTP_CHECK_RC=1
			CHECK_FAILED=1
		fi
	fi

	if [[ ${NTP_CONFIGURED} -gt 0 ]]; then
		info "NTP client is configured. check: Passed"
		echo "`current_date_time`: INFO: NTP client is configured. check: Passed"
	else
		error "NTP client is not configured. check: Failed"
		echo "`current_date_time`: ERROR: NTP client is not configured. check: Failed"
		if [[ ${CHECK_FAILED} -eq 0 ]]; then
			NTP_CHECK_RC=2
			CHECK_FAILED=1
		fi
	fi

	# if [[ ! -z ${NTP_ENABLED} && ${NTP_ENABLED} ==  "enabled" ]]; then
		# info "NTP service is enabled. check: Passed"
		# echo "`current_date_time`: INFO: NTP service is enabled. check: Passed"
	# else
		# error "NTP service is disabled. check: Failed"
		# echo "`current_date_time`: ERROR: NTP service is disabled. check: Failed"
		# if [[ ${CHECK_FAILED} -eq 0 ]]; then
			# NTP_CHECK_RC=3
			# CHECK_FAILED=1
		# fi
	# fi

	if [[ ${NTP_RUNNING} -gt 0 ]]; then
		info "NTP service is running. check: Passed"
		echo "`current_date_time`: INFO: NTP service is running. check: Passed"
	else
		error "NTP service is not running. check: Failed"
		echo "`current_date_time`: ERROR: NTP service is not running. check: Failed"
		if [[ ${CHECK_FAILED} -eq 0 ]]; then
			NTP_CHECK_RC=3
			CHECK_FAILED=1
		fi
	fi

	echo " "

	return ${NTP_CHECK_RC}
}

function choose_host_address() {

	local CHOOSE_HOST_ADDRESS_RC=0
	local ADDRESS_LIST=""
	local DOCKER_SUBNET=0
	local FIXED_ADDRESS_LIST=""
	local USER_RESPONSE=""
	local USER_RESP_VALID=0
	local IP_ADDRESS_NO=0

	if [[ "mac" = "${OS}" ]]; then
		ADDRESS_LIST=`ifconfig | grep inet | grep -v inet6 | grep -v 127.0.0.1 | awk '{print $2}'`
	else
		ADDRESS_LIST=`hostname -I`
	fi

	info "Building host address list to use to connect to DPOD Web interface"
	for HOST_ADDRESS_FROM_LIST in ${ADDRESS_LIST}
	do
		if [[ ! ${HOST_ADDRESS_FROM_LIST} == 172.77.77* ]] ; then

			if [[ ! "mac" = "${OS}" ]]; then
				DOCKER_SUBNET=`ip addr | grep ${HOST_ADDRESS_FROM_LIST} | grep docker0 | wc -l`
			else
				DOCKER_SUBNET=0
			fi

			if [[ ${DOCKER_SUBNET} -eq 0 ]]; then
				if [[ ${IP_ADDRESS_NO} -eq 0 ]]; then
					FIXED_ADDRESS_LIST="${HOST_ADDRESS_FROM_LIST}"
				else
					FIXED_ADDRESS_LIST="${FIXED_ADDRESS_LIST} ${HOST_ADDRESS_FROM_LIST}"
				fi
				IP_ADDRESS_NO=$(( IP_ADDRESS_NO + 1 ))
			else
				info "Skipping address ${HOST_ADDRESS_FROM_LIST}. It is a docker internal network."
			fi

		fi
	done

	if [[ ${IP_ADDRESS_NO} -gt 1 ]]; then
		info "waiting for user to choose valid host address from following list : ${FIXED_ADDRESS_LIST}"

		echo ""
		echo -e "\tPlease select the IP address for the Operations Dashboard Web UI"
		echo ""

		echo -e "\tThe current configured IPs on this server are : "
		echo ""

		for CURR_IP in "${FIXED_ADDRESS_LIST[@]}" ; do
			echo -e "\t   ${CURR_IP}"
		done

		while [[ ( x"$USER_RESPONSE" != "xy" &&  x"$USER_RESPONSE" != "xY" &&  x"$USER_RESPONSE" != "xyes" &&  x"$USER_RESPONSE" != "xYES" ) ]]; do
			echo
			echo

			USER_RESP_VALID=0
			while [ "$USER_RESP_VALID" = 0 ] ; do
				 echo
				 echo -ne  "\tPlease enter UI console IP address  :> "; read SERVER_IP

				 # make sure server ip is not empty
				if [[ ! -z ${SERVER_IP} ]]; then
					# make sure the ip address is one for the ip address that are configured on the server
					if [[ " ${FIXED_ADDRESS_LIST[@]} " =~ " ${SERVER_IP} " ]]; then
						USER_RESP_VALID=1;
					 else
						echo
						echo -e  "\tIP address is not one of the configured IPs on this server."
						echo -e  "\tPlease enter configured IP address."
						USER_RESP_VALID=0;

						echo
						echo -e "\tThe current configured IPs on this server are : "
						echo ""

						for CURR_IP in "${FIXED_ADDRESS_LIST[@]}" ; do
							echo -e "\t   ${CURR_IP}"
						done
					 fi
				else
					echo -e  "\tPlease enter IP address !\n"
					USER_RESP_VALID=0;
				fi
			done

			echo ""
			echo -e "\tYou entered:"
			echo -e "\tUI console ip address        : ${SERVER_IP}"
			echo ""

			USER_RESPONSE=""
			while [ "$USER_RESPONSE" != "y" ] && [ "$USER_RESPONSE" != "Y" ] && [ "$USER_RESPONSE" != "YES" ] && [ "$USER_RESPONSE" != "yes" ] && [ "$USER_RESPONSE" != "n" ] && [ "$USER_RESPONSE" != "N" ] && [ "$USER_RESPONSE" != "NO" ] && [ "$USER_RESPONSE" != "no" ] ; do
				echo -ne "\tIs this correct? [y/n] "; read USER_RESPONSE
			done


		done

		info "User chose ${SERVER_IP} for UI console ip address"

	else
		# there is only one ip address configured on the server -> use it
		SERVER_IP=${FIXED_ADDRESS_LIST[0]}
		info "One ip address configured on server, ${SERVER_IP} , using it for UI console ip address"
	fi


	CHOSEN_HOST_ADDRESS=${SERVER_IP}

	return ${CHOOSE_HOST_ADDRESS_RC}
}


function wait_for_ui_to_start() {

	local WAIT_FOR_UI_TO_START_RC=0
	local WAIT_FOR_UI_RESPONSE_FILE="${LOG_DIR}/TestUI_Reponse.log"
	local UI_TEST_REST_URI="/op/api/v1/reports"
	local SUCCESS_INDICATION=0
	local DEVICE_EXISTS_INDICATION=0
	local TRIES_COUNT=0

	while [ ${SUCCESS_INDICATION} -eq 0 ] && [ ${TRIES_COUNT} -lt ${MAX_UI_TESTS} ]
	do
		sleep ${SLEEP_BETWEEN_UI_TESTS}
		docker exec -it ${CENTOS_CONTAINER} curl -s --user ${DPOD_USER}:${DPOD_PASS} -X GET --insecure -w "\nResultCode:%{http_code}" https://${DPOD_ADDRESS}${UI_TEST_REST_URI} > ${WAIT_FOR_UI_RESPONSE_FILE} 2>> ${LOG_FILE}

		# Check for success indication in response
		SUCCESS_INDICATION=`grep "ResultCode:200" ${WAIT_FOR_UI_RESPONSE_FILE} | wc -l`

		TRIES_COUNT=$(( TRIES_COUNT + 1 ))
	done

	if [[ ${SUCCESS_INDICATION} -eq 0 ]]; then
		WAIT_FOR_UI_TO_START_RC=1
	fi

	return ${WAIT_FOR_UI_TO_START_RC}
}

function add_idg_to_dpod() {

	local IDG_SERVICE_NAME=$1
	local IDG_ADDRESS=$2
	local LOG_TAR_ADDRESS=$3
	local ADD_IDG_TO_DPOD_RESPONSE_FILE="${LOG_DIR}/${IDG_SERVICE_NAME}_DeviceRestCallResponse.log"
	local DEVICE_EXISTS_INDICATION=0
	local SUCCESS_INDICATION=0
	local TIMES_TO_TRY=3
	local TRIES_COUNT=0

	# idg name is the container id
	IDG_NAME=`docker ps -aqf "name=${IDG_SERVICE_NAME}_"`

	info "Adding idg device ${IDG_SERVICE_NAME} to dpod:"
	info "	Device Name = ${IDG_NAME}, Device Address: ${IDG_ADDRESS}"
	echo "`current_date_time`: INFO: Adding idg device ${IDG_SERVICE_NAME} to dpod"
	echo "`current_date_time`: INFO: 	Device Name = ${IDG_NAME}, Device Address: ${IDG_ADDRESS}"

	while [ ${SUCCESS_INDICATION} -eq 0 ] && [ ${DEVICE_EXISTS_INDICATION} -eq 0 ] && [ ${TRIES_COUNT} -lt ${TIMES_TO_TRY} ]
	do
		# Try to add and setup a device, using DPOD Device REST API
		docker exec -it ${CENTOS_CONTAINER} curl -s --user ${DPOD_USER}:${DPOD_PASS} -X POST --insecure -w "\n" https://${DPOD_ADDRESS}/op/api/v1/devices?name=${IDG_NAME}\&host=${IDG_ADDRESS}\&somaPort=${SOMA_PORT}\&somaUser=${SOMA_USER}\&somaPassword=${SOMA_PASS}\&logTargetAddress=${LOG_TAR_ADDRESS}\&monitorResources=${MONITOR_RESOURCES}\&monitorServices=${MONITOR_SERVICES}\&syslogAgentName=${SYS_AGNT_NAME}\&setupSyslogForDomains=${SETUP_SYS_AGNT}\&wsmAgentName=${WSM_AGNT_NAME}\&setupWsmForDomains=${SETUP_WSM}\&setupCertificateMonitor=${SETUP_CERT_MON}\&autoSetupPattern=${AUTO_SETUP_PATTERN}\&autoSetupAgentName=${AUTO_SETUP_AGNT_NAME} > ${ADD_IDG_TO_DPOD_RESPONSE_FILE} 2>> ${LOG_FILE}

		# Check for success indication in response
		SUCCESS_INDICATION=`grep "\"resultCode\":\"SUCCESS\"" ${ADD_IDG_TO_DPOD_RESPONSE_FILE} | wc -l`
		DEVICE_EXISTS_INDICATION=`grep "already exists" ${ADD_IDG_TO_DPOD_RESPONSE_FILE} | wc -l`

		TRIES_COUNT=$(( TRIES_COUNT + 1 ))

		if [ ${SUCCESS_INDICATION} -eq 0 ] && [ ${DEVICE_EXISTS_INDICATION} -eq 0 ] ; then
			warn "Device REST call response does not contain a success indication. "
			if [[ ${TRIES_COUNT} -lt ${TIMES_TO_TRY} ]]; then
				warn "Will try again in ${SEC_TO_WAIT_AFTER_DEVICE_ADD} seconds"
				sleep ${SEC_TO_WAIT_AFTER_DEVICE_ADD}
			fi
		fi
	done

	if [ ${SUCCESS_INDICATION} -eq 0 ] && [ ${DEVICE_EXISTS_INDICATION} -eq 0 ] ; then
		ADD_IDG_TO_DPOD_RC=1
		warn "You may need to add device ${IDG_SERVICE_NAME} to dpod manually."
		echo "`current_date_time`: WARN: Device REST call response does not contain a success indication. You may need to add device ${IDG_SERVICE_NAME} to dpod manually."
	fi

	return ${ADD_IDG_TO_DPOD_RC}
}


function test_md5_signature() {

	local TEST_MD5_SIGNATURE_RC=0
	local MD5_SIG
	local LOCAL_ARTIFACT=$1
	local LOCAL_MD5=$2
	MD5_SIG_MATCH=0

	if [[ -f ${LOCAL_ARTIFACT} ]]; then
		if [[ "mac" = "${OS}" ]]; then
		  MD5_SIG=`md5 ${LOCAL_ARTIFACT} | awk '{print $4}'`
		else
		  MD5_SIG=`md5sum ${LOCAL_ARTIFACT} | awk '{print $1}'`
		fi

		if [ $? -ne 0 ]; then
			error "Failed to check md5sum of file. Abort"
			echo "`current_date_time`: ERROR: Failed to check md5sum of file. Abort"
			TEST_MD5_SIGNATURE_RC=1
			ABORT=1
		fi

		if [[ ${TEST_MD5_SIGNATURE_RC} -eq 0 ]]; then
			MD5_SIG_MATCH=`grep -x ${MD5_SIG} ${LOCAL_MD5} | wc -l`
			if [ $? -ne 0 ]; then
				error "Failed to compare md5sum values. Abort"
				echo "`current_date_time`: ERROR: Failed to compare md5sum values. Abort"
				TEST_MD5_SIGNATURE_RC=1
				ABORT=1
			fi
		fi

	fi

	return ${TEST_MD5_SIGNATURE_RC}
}


function generate_simulation_data() {

	local GENERATE_SIMULATION_DATA_RC=0

	local IDG_SERVICE_NAME=$1
	local RUN_SQL_FILES=$2

	IDG_NAME=`docker ps -aqf "name=${IDG_SERVICE_NAME}_"`

	if [[ ${GENERATE_SIMULATION_DATA_RC} -eq 0 ]]; then
		info "Generating simulation data for device: ${IDG_SERVICE_NAME}, RUN_SQL_FILES: ${RUN_SQL_FILES}"
		echo "`current_date_time`: INFO: Generating simulation data for device: ${IDG_SERVICE_NAME}"
		docker exec -d -u root ${DPOD_CONTAINER} /bin/su - -c "${GENERATE_SIMULATION_SCRIPT} -i ${IDG_SERVICE_NAME} -n ${IDG_NAME} -s ${TX_GENERATOR_SLEEP_TIME} -r ${RUN_SQL_FILES} > ${GENERATE_SIMULATION_LOG}_${IDG_SERVICE_NAME}.log"
		if [ $? -ne 0 ]; then
			error "Failed to generate simulation data for device: ${IDG_SERVICE_NAME}. Abort"
			echo "`current_date_time`: ERROR: Failed to generate simulation data for device: ${IDG_SERVICE_NAME}. Abort"
			GENERATE_SIMULATION_DATA_RC=1
			ABORT=1
		fi
	fi

	return ${GENERATE_SIMULATION_DATA_RC}
}


function add_all_idg_to_dpod_and_run_trans() {

	local ADD_ALL_IDG_TO_DPOD_AND_RUN_TRANS_RC=0
	local IDG_SERVICE_NUM=0
	local RUN_SQL_FILES="false"
	local NEED_TO_ADD_IDG_TO_DPOD=$1
	local IDG_ADDRESS=0
	local IDG_LOG_TARGET_ADDRESS=0

	while [ ${ADD_ALL_IDG_TO_DPOD_AND_RUN_TRANS_RC} -eq 0 ] && [ ${IDG_SERVICE_NUM} -lt ${NUMBER_OF_IDG_SERVICES_TO_HANDLE} ]
	do
		IDG_SERVICE_NUM=$(( IDG_SERVICE_NUM + 1 ))

		# the first two idg services addresses are taken from hardcoded vars. they are part of the regular simulator
		# the rest of the addreses are taken from the yml file. they are part of the demo.
		if [[ ${IDG_SERVICE_NUM} -gt 2 ]]; then
			IDG_ADDRESS=`awk "/idg${IDG_SERVICE_NUM}:/,/ipv4_address:/" ${COMPOSE_FILE_PATH} | grep ipv4_address | awk -F' ' '{ print $2 }'`
			IDG_LOG_TARGET_ADDRESS=${IDG_ADDRESS}
		elif [[ ${IDG_SERVICE_NUM} -eq 2 ]]; then
			IDG_ADDRESS=${IDG2_ADDRESS}
			IDG_LOG_TARGET_ADDRESS=${LOG_TAR_ADDRESS_2}
		else
			IDG_ADDRESS=${IDG1_ADDRESS}
			IDG_LOG_TARGET_ADDRESS=${LOG_TAR_ADDRESS_1}
		fi

		if [[ "${NEED_TO_ADD_IDG_TO_DPOD}" == "true" ]]; then
			add_idg_to_dpod idg${IDG_SERVICE_NUM} ${IDG_ADDRESS} ${IDG_LOG_TARGET_ADDRESS}
		fi

		run_transactions idg${IDG_SERVICE_NUM} ${IDG_ADDRESS}
		if [ $? -ne 0 ]; then
			ADD_ALL_IDG_TO_DPOD_AND_RUN_TRANS_RC=1
			ABORT=1
		fi

		if [[ ${ADD_ALL_IDG_TO_DPOD_AND_RUN_TRANS_RC} -eq 0 ]]; then

			if [ ${IDG_SERVICE_NUM} -eq 1 ] && [ "${NEED_TO_ADD_IDG_TO_DPOD}" == "true" ] ; then
				RUN_SQL_FILES="true"
			else
				RUN_SQL_FILES="false"
			fi

			generate_simulation_data idg${IDG_SERVICE_NUM} ${RUN_SQL_FILES}
			if [ $? -ne 0 ]; then
				ADD_ALL_IDG_TO_DPOD_AND_RUN_TRANS_RC=1
				ABORT=1
			fi
		fi
	done

	return ${ADD_ALL_IDG_TO_DPOD_AND_RUN_TRANS_RC}
}


function generate_idg_volumes() {

	local GENERATE_IDG_VOLUMES_RC=0
	local IDG_SERVICE_NUM=0

	while [ ${GENERATE_IDG_VOLUMES_RC} -eq 0 ] && [ ${IDG_SERVICE_NUM} -lt ${NUMBER_OF_IDG_SERVICES_TO_HANDLE} ]
	do
		IDG_SERVICE_NUM=$(( IDG_SERVICE_NUM + 1 ))
		info "Generating IDG${IDG_SERVICE_NUM} Volumes"
		echo "`current_date_time`: INFO: Generating IDG${IDG_SERVICE_NUM} Volumes"
		NEW_IDG_VOLUME="${IDG_VOLUME_DIR_PREFIX}${IDG_SERVICE_NUM}"
		NEW_IDG_INITIAL_CONF="${IDG_INITIAL_CONF_PREFIX}${IDG_SERVICE_NUM}/*"
		mkdir -p ${NEW_IDG_VOLUME}
		cp -R ${NEW_IDG_INITIAL_CONF} ${NEW_IDG_VOLUME} >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to generate IDG${IDG_SERVICE_NUM} Volumes. Abort"
			echo "`current_date_time`: ERROR: Failed to generate IDG${IDG_SERVICE_NUM} Volumes. Abort"
			GENERATE_IDG_VOLUMES_RC=1
			ABORT=1
		fi
	done

	return ${GENERATE_IDG_VOLUMES_RC}
}

function set_num_of_idg_services_to_handle() {

	if [[ ${ADDITIONAL_IDG_FLAG_UP} -eq 1 ]]; then
		NUMBER_OF_IDG_SERVICES_TO_HANDLE=2
	else
		NUMBER_OF_IDG_SERVICES_TO_HANDLE=1
	fi
}


function build_simulator() {

	local BUILD_SIMULATOR_RC=0
	local USER_RESPONSE="noreply"

	if [ -f ${USER_INPUT_FILE} ] && [ ${IGNORE_HW_FLAG_UP} -ne 1 ] ; then
		warn "DPODSimulator was already built on this host before. "
		echo "`current_date_time`: WARN: DPODSimulator was already built on this host before. "
		echo "`current_date_time`: WARN: A new 'build' will remove all DPODSimulator-related objects, and build a new environment. "

		while [ ! "${USER_RESPONSE}" == "y" ] && [ ! "${USER_RESPONSE}" == "n" ]
		do
			echo -n "Do you want to continue ? [y/n]: "
			read USER_RESPONSE
			echo " "
		done

		if [[ "${USER_RESPONSE}" == "n" ]]; then
			info "User chose to abort."
			echo "`current_date_time`: INFO: User chose to abort."
			BUILD_SIMULATOR_RC=1
			ABORT=1
		else
			info "User chose to continue."
			echo "`current_date_time`: INFO: User chose to continue."
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		info "Enabling ipv4 forwarding"
		echo "`current_date_time`: INFO: Enabling ipv4 forwarding"
		if [[ "mac" = "${OS}" ]]; then
			sudo sysctl -w net.inet.ip.forwarding=1 >> ${LOG_FILE} 2>&1
		else
			sudo sysctl -w net.ipv4.ip_forward=1 >> ${LOG_FILE} 2>&1
		fi

		if [ $? -ne 0 ]; then
			warn "Could not set enable ipv4 forwarding"
			echo "`current_date_time`: WARN: Could not set enable ipv4 forwarding"
		fi
	fi

	if [ ${BUILD_SIMULATOR_RC} -eq 0 ] && [ ${IGNORE_HW_FLAG_UP} -ne 1 ] ; then

		info "Performing hardware requirements checks :"
		info "========================================"
		echo "`current_date_time`: INFO: Performing hardware requirements checks :"
		echo "`current_date_time`: INFO: ========================================"

		memory_check
		if [ $? -ne 0 ]; then
			ask_for_hw_req_user_override
			if [ $? -ne 0 ]; then
				BUILD_SIMULATOR_RC=1
				ABORT=1
			fi
		fi

		if [[ ${ABORT} -eq 0 ]]; then
			cpu_check
			if [ $? -ne 0 ]; then
				ask_for_hw_req_user_override
				if [ $? -ne 0 ]; then
					BUILD_SIMULATOR_RC=1
					ABORT=1
				fi
			fi
		fi

		if [[ ${ABORT} -eq 0 ]]; then
			disk_space_check
			if [ $? -ne 0 ]; then
				BUILD_SIMULATOR_RC=1
				ABORT=1
			fi
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		ntp_check
		if [ $? -ne 0 ]; then
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		if [[ ${LEG_FLAG_UP} -eq 1 ]]; then
			CHOSEN_HOST_ADDRESS=${HOST_LEG_PARAMETER}
			info "Using host address that user passed as a parameter: ${CHOSEN_HOST_ADDRESS}"
			echo "`current_date_time`: INFO: Using host address that user passed as a parameter: ${CHOSEN_HOST_ADDRESS}"
		else
			choose_host_address
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		if [[ ! "mac" = "${OS}" ]]; then
			handle_iptables
			if [ $? -ne 0 ]; then
				error "Failed to handle iptables rules. Abort"
				echo "`current_date_time`: ERROR: Failed to handle iptables rules. Abort"
				BUILD_SIMULATOR_RC=1
				ABORT=1
			fi
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then

		# we download to this folder and need to know it exists
		mkdir -p ${RESOURCES_DIR}/dpod

		info "The 'build' action may take up to an hour. Please make sure you have a fast and steady internet connection !"
		echo "`current_date_time`: INFO: The 'build' action may take up to an hour. Please make sure you have a fast and steady internet connection !"

		info "Simulator environment will be cleaned before build"
		echo "`current_date_time`: INFO: Simulator environment will be cleaned before build"
		clean_simulator
		if [ $? -ne 0 ]; then
			error "Failed to clean simulator environment. Abort"
			echo "`current_date_time`: ERROR: Failed to clean simulator environment. Abort"
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		download_with_md5_test ${LOCAL_DPOD_MD5_FILE} ${DPOD_MD5_URL} ${LOCAL_DPOD_IMAGE_FILE} ${DPOD_IMAGE_URL} dpod
		if [ $? -ne 0 ]; then
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		info "Loading dpod image to docker"
		echo "`current_date_time`: INFO: Loading dpod image to docker"
		docker load < ${LOCAL_DPOD_IMAGE_FILE} >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to Load local dpod image to docker. "
			echo "`current_date_time`: ERROR: Failed to Load local dpod image to docker. "
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		generate_idg_volumes
		if [ $? -ne 0 ]; then
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		info "Adding permissions to datapower config dir"
		echo "`current_date_time`: INFO: Adding permissions to datapower config dir"
		chmod -R 777 ${RESOURCES_DIR}/datapower >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to add permissions to datapower config dir. Abort"
			echo "`current_date_time`: ERROR: Failed to add permissions to datapower config dir. Abort"
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
			info "Generating Splunk Volumes"
			echo "`current_date_time`: INFO: Generating Splunk Volumes"
			mkdir -p ${SPLUNK_VOLUME_DIR}
			cp -R ${SPLUNK_INITIAL_CONF} ${SPLUNK_VOLUME_DIR} >> ${LOG_FILE} 2>&1
			if [ $? -ne 0 ]; then
				error "Failed to generate Splunk Volumes. Abort"
				echo "`current_date_time`: ERROR: Failed to generate Splunk Volumes. Abort"
				BUILD_SIMULATOR_RC=1
				ABORT=1
			fi

			if [[ "mac" = "${OS}" ]]; then
				sed -i '' "s/__HOST_ADDRESS__/${CHOSEN_HOST_ADDRESS}/g" ${SPLUNK_DPOD_TRAN_VIEW_FILE}
				SED_RESP=$?
			else
				sed -i "s/__HOST_ADDRESS__/${CHOSEN_HOST_ADDRESS}/g" ${SPLUNK_DPOD_TRAN_VIEW_FILE}
				SED_RESP=$?
			fi

			if [ ${SED_RESP} -ne 0 ]; then
				error "Failed to update Splunk views configuration files. Abort"
				echo "`current_date_time`: ERROR: Failed to update Splunk views configuration files. Abort"
				BUILD_SIMULATOR_RC=1
				ABORT=1
			fi
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
			info "Adding permissions to Splunk config dir"
			echo "`current_date_time`: INFO: Adding permissions to Splunk config dir"
			chmod -R 777 ${RESOURCES_DIR}/splunk >> ${LOG_FILE} 2>&1
			if [ $? -ne 0 ]; then
				error "Failed to add permissions to Splunk config dir. Abort"
				echo "`current_date_time`: ERROR: Failed to add permissions to Splunk config dir. Abort"
				BUILD_SIMULATOR_RC=1
				ABORT=1
			fi
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		info "Building and Starting simulator. may take about 30 minutes !"
		echo "`current_date_time`: INFO: Building and Starting simulator. may take about 30 minutes !"
		if [[ "mac" = "${OS}" ]]; then
			cp /etc/localtime ${RESOURCES_DIR}/dpod/
			cp /etc/localtime ${RESOURCES_DIR}/centos/
			cp /etc/localtime ${RESOURCES_DIR}/splunk/
			/usr/local/bin/docker-compose -f ${COMPOSE_FILE_PATH_MAC} up -d ${DOCKER_SERVICE_TO_START} >> ${LOG_FILE} 2>&1
		else
			/usr/local/bin/docker-compose -f ${COMPOSE_FILE_PATH} up -d ${DOCKER_SERVICE_TO_START} >> ${LOG_FILE} 2>&1
		fi
		if [ $? -ne 0 ]; then
			error "Failed to build simulator. This may be due to a network issue. Please try to run the 'build' action again, after fixing the network issues raised in the log file. Abort"
			echo "`current_date_time`: ERROR: Failed to build simulator . Please try to run the 'build' action again, after fixing the network issues raised in the log file. Abort"
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		if [[ "mac" = "${OS}" ]]; then
			set_system_time_from_hw_clock
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		info "Installing container tools"
		echo "`current_date_time`: INFO: Installing container tools"
		docker exec -it ${CENTOS_CONTAINER} ${INSTALL_CONTAINER_TOOLS_SCRIPT} -a ${DPOD_ADDRESS} -p ${DPOD_SSH_PASS}  >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to install container tools. Abort"
			echo "`current_date_time`: ERROR: Failed to install container tools. Abort"
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		info "Starting DPOD app"
		echo "`current_date_time`: INFO: Starting DPOD app"
		docker exec -d -u root ${DPOD_CONTAINER} /bin/su - -c "export ACCEPT_LICENSE=true; export TIME_ZONE=${TIME_ZONE} ;export DPOD_EXT_HOST_IP=${CHOSEN_HOST_ADDRESS} ;/app/scripts/app-init.sh" >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to Start DPOD app. Abort"
			echo "`current_date_time`: ERROR: Failed to Start DPOD app. Abort"
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		info "Waiting for dpod to start"
		echo "`current_date_time`: INFO: Waiting for dpod to start"
		wait_for_ui_to_start
		if [ $? -ne 0 ]; then
			error "System startup failed after ${MAX_UI_WAIT_SECONDS} seconds."
			error "This is probably due to insufficiant system resources. "
			error "Please try building the dpodSimulator again, without the additional idg and without splunk :"
			error "====> ${LOCAL_FILES_DIR}/${SCRIPT_BASE_NAME} -a build"
			error "If the problem persists, please contact us at: contactus@mon-tier.com ."
			echo "`current_date_time`: ERROR: System startup failed after ${MAX_UI_WAIT_SECONDS} seconds."
			echo "`current_date_time`: ERROR: This is probably due to insufficiant system resources. "
			echo "`current_date_time`: ERROR: Please try building the dpodSimulator again, without the additional idg and without splunk :"
			echo "`current_date_time`: ERROR: 		${LOCAL_FILES_DIR}/${SCRIPT_BASE_NAME} -a build"
			echo "`current_date_time`: ERROR: If the problem persists, please contact us at: contactus@mon-tier.com ."
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		add_all_idg_to_dpod_and_run_trans true
		if [ $? -ne 0 ]; then
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi
	
	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		info "Running timesync tasks"
		echo "`current_date_time`: INFO: Running timesync tasks"
		docker exec -it ${CENTOS_CONTAINER} crond >> ${LOG_FILE} 2>&1		
		if [ $? -ne 0 ]; then
			error "Failed to run timesync tasks. Abort."
			echo "`current_date_time`: ERROR: Failed to run timesync tasks. Abort."
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi
	
	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
			info "Running splunk operations"
			echo "`current_date_time`: INFO: Running splunk operations"
			docker exec -d -u root ${SPLUNK_CONTAINER} sh -c "${SPLUNK_OPERATIONS_SCRIPT} -c -l ${SLEEP_BETWEEN_SPLUNK_RESTARTS} > ${SPLUNK_OPERATIONS_ACTIVATION_LOG_FILE} 2> ${SPLUNK_OPERATIONS_ACTIVATION_LOG_FILE}" >> ${LOG_FILE} 2>&1
			if [ $? -ne 0 ]; then
				error "Failed to run splunk operations. Abort."
				echo "`current_date_time`: ERROR: Failed to run splunk operations. Abort."
				BUILD_SIMULATOR_RC=1
				ABORT=1
			fi
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		save_user_input_to_file
		if [ $? -ne 0 ]; then
			BUILD_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${BUILD_SIMULATOR_RC} -eq 0 ]]; then
		show_links
	fi

	return ${BUILD_SIMULATOR_RC}
}


function stop_simulator() {

	local STOP_SIMULATOR_RC=0

	if [[ ! -f ${USER_INPUT_FILE} ]]; then
		error "DPODSimulator was never built on this host. Nothing to stop."
		error "To build DPODSimulator, please run the 'build' action."
		echo "`current_date_time`: ERROR: DPODSimulator was never built on this host. Nothing to stop."
		echo "`current_date_time`: ERROR: To build DPODSimulator, please run the 'build' action."
		STOP_SIMULATOR_RC=1
		ABORT=1
	fi

	if [[ ${STOP_SIMULATOR_RC} -eq 0 ]]; then
		info "Stopping simulator"
		echo "`current_date_time`: INFO: Stopping simulator"
		if [[ "mac" = "${OS}" ]]; then
			/usr/local/bin/docker-compose -f ${COMPOSE_FILE_PATH_MAC} stop -t ${STOP_TIMEOUT} >> ${LOG_FILE} 2>&1
		else
		  /usr/local/bin/docker-compose -f ${COMPOSE_FILE_PATH} stop -t ${STOP_TIMEOUT} >> ${LOG_FILE} 2>&1
		fi
		if [ $? -ne 0 ]; then
			error "Failed to stop simulator. Abort"
			echo "`current_date_time`: ERROR: Failed to stop simulator. Abort"
			STOP_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	return ${STOP_SIMULATOR_RC}
}

function start_simulator() {

	local START_SIMULATOR_RC=0

	if [[ ${START_SIMULATOR_RC} -eq 0 ]]; then
		ntp_check
		if [ $? -ne 0 ]; then
			START_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${START_SIMULATOR_RC} -eq 0 ]]; then
		if [[ ! "mac" = "${OS}" ]]; then
			resync_ntp
		fi
	fi

	if [[ ${START_SIMULATOR_RC} -eq 0 ]]; then
		info "Starting simulator"
		echo "`current_date_time`: INFO: Starting simulator"
		if [[ "mac" = "${OS}" ]]; then
			/usr/local/bin/docker-compose -f ${COMPOSE_FILE_PATH_MAC} up -d ${DOCKER_SERVICE_TO_START} >> ${LOG_FILE} 2>&1
		else
			/usr/local/bin/docker-compose -f ${COMPOSE_FILE_PATH} up -d ${DOCKER_SERVICE_TO_START} >> ${LOG_FILE} 2>&1
		fi

		if [ $? -ne 0 ]; then
			error "Failed to start simulator. Abort"
			echo "`current_date_time`: ERROR: Failed to start simulator. Abort"
			START_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${START_SIMULATOR_RC} -eq 0 ]]; then
		if [[ ! "mac" = "${OS}" ]]; then
			handle_iptables
			if [ $? -ne 0 ]; then
				error "Failed to handle iptables rules. Abort"
				echo "`current_date_time`: ERROR: Failed to handle iptables rules. Abort"
				START_SIMULATOR_RC=1
				ABORT=1
			fi
		fi
	fi

	if [[ ${START_SIMULATOR_RC} -eq 0 ]]; then
		if [[ "mac" = "${OS}" ]]; then
			set_system_time_from_hw_clock
		fi
	fi

	if [[ ${START_SIMULATOR_RC} -eq 0 ]]; then
		info "Starting DPOD app"
		echo "`current_date_time`: INFO: Starting DPOD app"
		docker exec -d -u root ${DPOD_CONTAINER} /bin/su - -c "export ACCEPT_LICENSE=true; export TIME_ZONE=${TIME_ZONE} ;export DPOD_EXT_HOST_IP=${CHOSEN_HOST_ADDRESS} ;/app/scripts/app-init.sh" >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to Start DPOD app. Abort"
			echo "`current_date_time`: ERROR: Failed to Start DPOD app. Abort"
			START_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${START_SIMULATOR_RC} -eq 0 ]]; then
		info "Waiting for dpod to start"
		echo "`current_date_time`: INFO: Waiting for dpod to start"
		wait_for_ui_to_start
		if [ $? -ne 0 ]; then
			error "System startup failed after ${MAX_UI_WAIT_SECONDS} seconds."
			error "This is probably due to insufficiant system resources. "
			error "Please try stopping the dpodSimulator, and starting it again without the additional idg and without splunk :"
			error "====> ${LOCAL_FILES_DIR}/${SCRIPT_BASE_NAME} -a stop"
			error "====> ${LOCAL_FILES_DIR}/${SCRIPT_BASE_NAME} -a start"
			error "If the problem persists, please contact us at: contactus@mon-tier.com ."
			echo "`current_date_time`: ERROR: System startup failed after ${MAX_UI_WAIT_SECONDS} seconds."
			echo "`current_date_time`: ERROR: This is probably due to insufficiant system resources. "
			echo "`current_date_time`: ERROR: Please try stopping the dpodSimulator, and starting it again without the additional idg and without splunk :"
			echo "`current_date_time`: ERROR: 		${LOCAL_FILES_DIR}/${SCRIPT_BASE_NAME} -a stop"
			echo "`current_date_time`: ERROR: 		${LOCAL_FILES_DIR}/${SCRIPT_BASE_NAME} -a start"
			echo "`current_date_time`: ERROR: If the problem persists, please contact us at: contactus@mon-tier.com ."
			START_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	if [[ ${START_SIMULATOR_RC} -eq 0 ]]; then
		add_all_idg_to_dpod_and_run_trans false
		if [ $? -ne 0 ]; then
			START_SIMULATOR_RC=1
			ABORT=1
		fi
	fi
	
	if [[ ${START_SIMULATOR_RC} -eq 0 ]]; then
		info "Running timesync tasks"
		echo "`current_date_time`: INFO: Running timesync tasks"
		docker exec -it ${CENTOS_CONTAINER} crond >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			START_SIMULATOR_RC=1
			ABORT=1
		fi
	fi
	
	if [[ ${START_SIMULATOR_RC} -eq 0 ]]; then 
		if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
			info "Running splunk operations"
			echo "`current_date_time`: INFO: Running splunk operations"
			docker exec -d -u root ${SPLUNK_CONTAINER} sh -c "${SPLUNK_OPERATIONS_SCRIPT} -c -l ${SLEEP_BETWEEN_SPLUNK_RESTARTS} > ${SPLUNK_OPERATIONS_ACTIVATION_LOG_FILE} 2> ${SPLUNK_OPERATIONS_ACTIVATION_LOG_FILE}" >> ${LOG_FILE} 2>&1
			if [ $? -ne 0 ]; then
				error "Failed to run splunk operations. Abort."
				echo "`current_date_time`: ERROR: Failed to run splunk operations. Abort."
				START_SIMULATOR_RC=1
				ABORT=1
			fi
		fi
	fi

	return ${START_SIMULATOR_RC}
}

function run_transactions() {

	local RUN_TRANSACTIONS_RC=0
	local IDG_SERVICE_NAME=$1
	local IDG_ADDRESS=$2

	IDG_NAME=`docker ps -aqf "name=${IDG_SERVICE_NAME}_"`

	info "Starting Transactions on ${IDG_SERVICE_NAME}"
	echo "`current_date_time`: INFO: Starting Transactions on ${IDG_SERVICE_NAME}"
	docker exec -d -u root ${CENTOS_CONTAINER} sh -c "${TRANSACTIONS_ACTIVATION_SCRIPT} -i ${IDG_SERVICE_NAME} -n ${IDG_NAME} -a ${IDG_ADDRESS} -p ${SOMA_PORT} -u ${SOMA_USER} -s ${SOMA_PASS} -d ${DPOD_ADDRESS} -r ${DPOD_USER} -o ${DPOD_PASS} > ${TRANSACTIONS_ACTIVATION_LOG_FILE} 2> ${TRANSACTIONS_ACTIVATION_LOG_FILE}" >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		error "Failed to run transactions on ${IDG_SERVICE_NAME}. Abort"
		echo "`current_date_time`: ERROR: Failed to run transactions on ${IDG_SERVICE_NAME}. Abort"
		RUN_TRANSACTIONS_RC=1
		ABORT=1
	fi

	return ${RUN_TRANSACTIONS_RC}
}

function clean_simulator() {

	local CLEAN_SIMULATOR_RC=0
	local DOCKER_COMPOSE_DOWN_ERRORS_FILE="${LOG_DIR}/dockerComposeDownErrors.txt"
	local OBJECTS_NOT_REMOVED=0

	info "Cleaning simulator environment"
	echo "`current_date_time`: INFO: Cleaning simulator environment"
	if [[ "mac" = "${OS}" ]]; then
	    /usr/local/bin/docker-compose -f ${COMPOSE_FILE_PATH_MAC} down --rmi all -v -t ${CLEAN_SIMULATOR_TIMEOUT} >> ${LOG_FILE} 2> ${DOCKER_COMPOSE_DOWN_ERRORS_FILE}
    else
		/usr/local/bin/docker-compose -f ${COMPOSE_FILE_PATH} down --rmi all -v -t ${CLEAN_SIMULATOR_TIMEOUT} >> ${LOG_FILE} 2> ${DOCKER_COMPOSE_DOWN_ERRORS_FILE}
    fi

	if [ $? -ne 0 ]; then
		warn "Failed to remove simulator objects."
		echo "`current_date_time`: WARN: Failed to remove simulator objects."
		manual_clean_simulator
		if [ $? -ne 0 ]; then
			CLEAN_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	OBJECTS_NOT_REMOVED=`cat ${DOCKER_COMPOSE_DOWN_ERRORS_FILE} | grep "Failed to remove" | grep -v "No such image" | wc -l`
	if [[ ${OBJECTS_NOT_REMOVED} -ne 0 ]]; then
		warn "Simulator objects removal returned errors."
		echo "`current_date_time`: WARN: Simulator objects removal returned errors."
		manual_clean_simulator
		if [ $? -ne 0 ]; then
			CLEAN_SIMULATOR_RC=1
			ABORT=1
		fi
	fi

	info "Removing IDG volumes"
	echo "`current_date_time`: INFO: Removing IDG volumes"
	rm -Rf ${IDG_VOLUME_DIR_PREFIX}*/*
	if [ $? -ne 0 ]; then
		error "Failed to remove IDG volumes. Abort"
		echo "`current_date_time`: ERROR: Failed to remove IDG volumes. Abort"
		CLEAN_SIMULATOR_RC=1
		ABORT=1
	fi

	info "Removing Splunk volumes"
	echo "`current_date_time`: INFO: Removing Splunk volumes"
	rm -Rf ${SPLUNK_VOLUME_DIR}/*
	if [ $? -ne 0 ]; then
		error "Failed to remove Splunk volumes. Abort"
		echo "`current_date_time`: ERROR: Failed to remove Splunk volumes. Abort"
		CLEAN_SIMULATOR_RC=1
		ABORT=1
	fi

	return ${CLEAN_SIMULATOR_RC}
}


function lowercase(){
    echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}


function discover_os_type() {

	OS=`lowercase \`uname\``
	KERNEL=`uname -r`
	MACH=`uname -m`

	if [ "${OS}" == "windowsnt" ]; then
		OS=windows
	elif [ "${OS}" == "darwin" ]; then
		OS=mac
	else
		OS=`uname`
		if [ "${OS}" = "SunOS" ] ; then
			OS=Solaris
			ARCH=`uname -p`
			OSSTR="${OS} ${REV}(${ARCH} `uname -v`)"
		elif [ "${OS}" = "AIX" ] ; then
			OSSTR="${OS} `oslevel` (`oslevel -r`)"
		elif [ "${OS}" = "Linux" ] ; then
			if [ -f /etc/redhat-release ] ; then
				DistroBasedOn='RedHat'
				DIST=`cat /etc/redhat-release |sed s/\ release.*//`
				PSUEDONAME=`cat /etc/redhat-release | sed s/.*\(// | sed s/\)//`
				REV=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*//`
			elif [ -f /etc/SuSE-release ] ; then
				DistroBasedOn='SuSe'
				PSUEDONAME=`cat /etc/SuSE-release | tr "\n" ' '| sed s/VERSION.*//`
				REV=`cat /etc/SuSE-release | tr "\n" ' ' | sed s/.*=\ //`
			elif [ -f /etc/mandrake-release ] ; then
				DistroBasedOn='Mandrake'
				PSUEDONAME=`cat /etc/mandrake-release | sed s/.*\(// | sed s/\)//`
				REV=`cat /etc/mandrake-release | sed s/.*release\ // | sed s/\ .*//`
			elif [ -f /etc/debian_version ] ; then
				DistroBasedOn='Debian'
				DIST=`cat /etc/lsb-release | grep '^DISTRIB_ID' | awk -F=  '{ print $2 }'`
				PSUEDONAME=`cat /etc/lsb-release | grep '^DISTRIB_CODENAME' | awk -F=  '{ print $2 }'`
				REV=`cat /etc/lsb-release | grep '^DISTRIB_RELEASE' | awk -F=  '{ print $2 }'`
			fi
			if [ -f /etc/UnitedLinux-release ] ; then
				DIST="${DIST}[`cat /etc/UnitedLinux-release | tr "\n" ' ' | sed s/VERSION.*//`]"
			fi
			OS=`lowercase $OS`
			DistroBasedOn=`lowercase $DistroBasedOn`
			readonly OS
			readonly DIST
			readonly DistroBasedOn
			readonly PSUEDONAME
			readonly REV
			readonly KERNEL
			readonly MACH
		fi
	fi
}


function manual_clean_simulator() {

	local MANUAL_CLEAN_SIMULATOR_RC=0

	info "Cleaning 'dpodsimulator' objects manually"
	echo "`current_date_time`: INFO: Cleaning 'dpodsimulator' objects manually"

	# remove containers with dpodsimulator names
	info "Checking for existing 'dpodsimulator' docker containers"
	echo "`current_date_time`: INFO: Checking for existing 'dpodsimulator' docker containers"
	NUM_OF_EXISTING_CONTAINERS=`docker ps -a -f name=dpodsimulator -q | wc -l`

	if [[ ${NUM_OF_EXISTING_CONTAINERS} -ne 0 ]]; then
		info "Stopping all existing 'dpodsimulator' docker containers"
		echo "`current_date_time`: INFO: Stopping all existing 'dpodsimulator' docker containers"
		docker stop $(docker ps -a -f name=dpodsimulator -q) >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "'docker stop' command failed."
			echo "`current_date_time`: ERROR: 'docker stop' command failed."
			MANUAL_CLEAN_SIMULATOR_RC=1
		fi

		if [[ ${MANUAL_CLEAN_SIMULATOR_RC} -eq 0 ]]; then
			info "Removing all existing docker 'dpodsimulator' containers"
			echo "`current_date_time`: INFO: Removing all existing docker 'dpodsimulator' containers"
			docker rm -v -f $(docker ps -a -f name=dpodsimulator -q) >> ${LOG_FILE} 2>&1
			if [ $? -ne 0 ]; then
				error "'docker rm' command failed."
				echo "`current_date_time`: ERROR: 'docker rm' command failed."
				MANUAL_CLEAN_SIMULATOR_RC=1
			fi
		fi
	else
		info "There are no 'dpodsimulator' docker containers on the server"
		echo "`current_date_time`: INFO: There are no 'dpodsimulator' docker containers on the server"
	fi

	# remove images with dpodsimulator tags
	if [[ ${MANUAL_CLEAN_SIMULATOR_RC} -eq 0 ]]; then
		info "Checking for existing 'dpodsimulator' docker images"
		echo "`current_date_time`: INFO: Checking for existing 'dpodsimulator' docker images"
		NUM_OF_EXISTING_IMAGES=`docker images -f reference=*:dpodsimulator -q | wc -l`

		if [[ ${NUM_OF_EXISTING_IMAGES} -ne 0 ]]; then
			info "Removing all existing docker images"
			echo "`current_date_time`: INFO: Removing all existing docker images"
			docker rmi -f $(docker images -f reference=*:dpodsimulator -q) >> ${LOG_FILE} 2>&1
			if [ $? -ne 0 ]; then
				error "'docker rmi' command failed."
				echo "`current_date_time`: ERROR: 'docker rmi' command failed."
				MANUAL_CLEAN_SIMULATOR_RC=1
			fi
		else
			info "There are no 'dpodSimulator' images on the server"
			echo "`current_date_time`: INFO: There are no 'dpodSimulator' images on the server"
		fi
	fi

	# remove networks with dpodsimulator names
	if [[ ${MANUAL_CLEAN_SIMULATOR_RC} -eq 0 ]]; then
		info "Checking for 'dpodSimulator' docker networks"
		echo "`current_date_time`: INFO: Checking for 'dpodSimulator' docker networks"
		NUM_OF_EXISTING_NETWOKRS=`docker network ls | grep dpodsimulator_net | wc -l`

		if [[ ${NUM_OF_EXISTING_NETWOKRS} -ne 0 ]]; then
			info "Removing all 'dpodsimulator' docker networks"
			echo "`current_date_time`: INFO: Removing all 'dpodsimulator' docker networks"
			docker network rm dpodsimulator_net >> ${LOG_FILE} 2>&1
			if [ $? -ne 0 ]; then
				error "'docker network rm' command failed."
				echo "`current_date_time`: ERROR: 'docker network rm' command failed."
				MANUAL_CLEAN_SIMULATOR_RC=1
			fi
		else
			info "There are no 'dpodSimulator' networks on the server"
			echo "`current_date_time`: INFO: There are no 'dpodSimulator' networks on the server"
		fi
	fi

	if [[ ${MANUAL_CLEAN_SIMULATOR_RC} -ne 0 ]]; then
		error "Please clean the object manually :"
		error "===> Check for existing 'dpodSimulator' containers: 'docker ps -a -f name=dpodsimulator' "
		error "===> If 'dpodSimulator' exist, run the following commands : "
		error "=======> Stop all 'dpodsimulator' containers:     'docker stop \$(docker ps -a -f name=dpodsimulator -q)' "
		error "=======> Remove all 'dpodsimulator' containers:   'docker rm -v -f \$(docker ps -a -f name=dpodsimulator -q)' "
		error "===> Check for existing 'dpodSimulator' images: 'docker images -f reference=*:dpodsimulator' "
		error "===> If 'dpodSimulator' images exist, run the following command : "
		error "=======> Remove all 'dpodsimulator' images:       'docker rmi -f \$(docker images -f reference=*:dpodsimulator -q)' "
		error "===> Check for existing 'dpodSimulator' networks: 'docker network ls | grep dpodsimulator "
		error "===> If 'dpodSimulator' networks exist, tun the following command : "
		error "=======> Remove 'dpodsimulator' network:          'docker network rm dpodsimulator_net' "
		error "After manual removal succeeds, run the build action with dpodSimulator script."
		echo "`current_date_time`: ERROR: Please clean the object manually :"
		echo "`current_date_time`: ERROR: 		Check for existing 'dpodSimulator' containers: 'docker ps -a -f name=dpodsimulator' "
		echo "`current_date_time`: ERROR: 		If 'dpodSimulator' exist, run the following commands : "
		echo "`current_date_time`: ERROR: 			Stop all 'dpodsimulator' containers:     'docker stop \$(docker ps -a -f name=dpodsimulator -q)' "
		echo "`current_date_time`: ERROR: 			Remove all 'dpodsimulator' containers:   'docker rm -v -f \$(docker ps -a -f name=dpodsimulator -q)' "
		echo "`current_date_time`: ERROR: 		Check for existing 'dpodSimulator' images: 'docker images -f reference=*:dpodsimulator' "
		echo "`current_date_time`: ERROR: 		If 'dpodSimulator' images exist, run the following command : "
		echo "`current_date_time`: ERROR:	 		Remove all 'dpodsimulator' images:       'docker rmi -f \$(docker images -f reference=*:dpodsimulator -q)' "
		echo "`current_date_time`: ERROR:		Check for existing 'dpodSimulator' networks: 'docker network ls | grep dpodsimulator "
		echo "`current_date_time`: ERROR:		If 'dpodSimulator' networks exist, tun the following command : "
		echo "`current_date_time`: ERROR:			Remove 'dpodsimulator' network:          'docker network rm dpodsimulator_net' "
		echo "`current_date_time`: ERROR: After manual removal succeeds, run the build action with dpodSimulator script."
	fi

	return ${MANUAL_CLEAN_SIMULATOR_RC}
}


function show_links() {

	local DPOD_LOCAL_URL="https://<hostIp>:4433/op"
	local SPLUNK_LOCAL_URL="http://<hostIp>:8000"
	local IDG1_LOCAL_URL="https://<hostIp>:9090"
	local IDG2_LOCAL_URL="https://<hostIp>:9091"

	DPOD_LOCAL_URL="https://${CHOSEN_HOST_ADDRESS}:4433/op"
	IDG1_LOCAL_URL="https://${CHOSEN_HOST_ADDRESS}:9090"
	IDG2_LOCAL_URL="https://${CHOSEN_HOST_ADDRESS}:9091"

	if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
		SPLUNK_LOCAL_URL="http://${CHOSEN_HOST_ADDRESS}:8000"
	fi

	info "To access Operations Dashboard Web UI use the following URL: ${DPOD_LOCAL_URL}"
	if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
		info "To access Splunk Web Interface use the following URL: ${SPLUNK_LOCAL_URL}"
	fi
	info "To access DataPower Web UI use the following URL: ${IDG1_LOCAL_URL}"
	if [[ ${ADDITIONAL_IDG_FLAG_UP} -eq 1 ]]; then
		info "To access the additional DataPower Web UI use the following URL: ${IDG2_LOCAL_URL}"
	fi
	echo " "
	echo "`current_date_time`: INFO: To access Operations Dashboard Web UI use the following URL: "
	echo "`current_date_time`: INFO: 		${DPOD_LOCAL_URL}"
	if [[ ${SPLUNK_FLAG_UP} -eq 1 ]]; then
		echo "`current_date_time`: INFO: To access Splunk Web Interface use the following URL: "
		echo "`current_date_time`: INFO: 		${SPLUNK_LOCAL_URL}"
	fi
	echo " "
}


function cpu_check {

	local CORES=`getconf _NPROCESSORS_ONLN`
	CPU_CHECK_RC=0

	info "CPU check - CRITICAL"
	info "===================="
	echo "`current_date_time`: INFO: CPU check - CRITICAL"
	echo "`current_date_time`: INFO: ===================="

	if [[ ${CORES} -lt ${MIN_CPU_NO} ]]; then
		warn "Checking for ${MIN_CPU_NO} cores, found ${CORES}. check: Failed"
		echo "`current_date_time`: WARN: System resources warning: Minimal required number of CPU cores is ${MIN_CPU_NO}, Found ${CORES}. "
		CPU_CHECK_RC=1
	else
		info "Checking for ${MIN_CPU_NO} cores, found ${CORES}. check: Passed"
		echo "`current_date_time`: INFO: Checking for ${MIN_CPU_NO} cores, found ${CORES}. check: Passed"
		CPU_CHECK_RC=0
	
		# check macOS docker resource limit
		if [[ "mac" = "${OS}" ]]; then
			info "Checking Docker CPUs limitation in Dokcer advanced preferences"
			echo "`current_date_time`: INFO: Checking Docker CPUs limitation in Dokcer advanced preferences"
			MACOS_RESOURCE_LIMIT_CPU=`docker system info | grep "CPUs" | awk -F': ' '{print $2}'`
			if [[ ${MACOS_RESOURCE_LIMIT_CPU} -lt ${MIN_CPU_NO} ]]; then
				warn "Dokcer CPUs limitation is set to ${MACOS_RESOURCE_LIMIT_CPU} in docker advanced preferences. check: Failed"
				echo "`current_date_time`: WARN: Dokcer CPUs limitation is set to ${MACOS_RESOURCE_LIMIT_CPU} in docker advanced preferences. check: Failed"
				CPU_CHECK_RC=1
			else
				info "Dokcer CPUs limitation is set to ${MACOS_RESOURCE_LIMIT_CPU} in docker advanced preferences. check: Passed"
				echo "`current_date_time`: INFO: Dokcer CPUs limitation is set to ${MACOS_RESOURCE_LIMIT_CPU} in docker advanced preferences. check: Passed"
				CPU_CHECK_RC=0
			fi
		fi
	fi
		
	echo " "

  return ${CPU_CHECK_RC}
}

function memory_check {

	MEMORY_CHECK_RC=0

	info "Memory check - CRITICAL"
	info "======================="
	echo "`current_date_time`: INFO: Memory check - CRITICAL"
	echo "`current_date_time`: INFO: ======================="

	if [[ "mac" = "${OS}" ]]; then
		ram=`system_profiler SPHardwareDataType | grep "  Memory:" | awk -F': ' '{print $2}' | awk -F' GB' '{print $1}'`
	else
		ram=`awk '{ printf "%.0f", $2/1024/1024 ; exit}' /proc/meminfo`
	fi


	if [[ ${ram} -lt ${MIN_MEMORY_SIZE} ]]; then
		warn "Checking for minimum of ${MIN_MEMORY_SIZE}GB RAM, found ${ram}GB RAM, check: Failed"
		echo "`current_date_time`: WARN: System resources warning: Minimal required memory is ${MIN_MEMORY_SIZE}GB, Found ${ram}GB. "
		MEMORY_CHECK_RC=1
	else
		info "Checking for minimum of ${MIN_MEMORY_SIZE}GB RAM, found ${ram}GB RAM, check: Passed"
		echo "`current_date_time`: INFO: Checking for minimum of ${MIN_MEMORY_SIZE}GB RAM, found ${ram}GB RAM, check: Passed"
		MEMORY_CHECK_RC=0

		# check macOS docker resource limit
		if [[ "mac" = "${OS}" ]]; then
			info "Checking Docker Total Memory limitation in Dokcer advanced preferences"
			echo "`current_date_time`: INFO: Checking Docker Total Memory limitation in Dokcer advanced preferences"
			MACOS_RESOURCE_LIMIT_MEM=`docker system info | grep "Total Memory" | awk -F': ' '{print $2}' | awk -F'G' '{print $1}'`
			MACOS_RESOURCE_LIMIT_MEM_INT=`docker system info | grep "Total Memory" | awk -F': ' '{print $2}' | awk -F'G' '{print $1}' | awk -F'.' '{print $1}'`
			if [[ ${MACOS_RESOURCE_LIMIT_MEM_INT} -lt ${MIN_MEMORY_SIZE} ]]; then
				warn "Dokcer Total Memory limitation is set to ${MACOS_RESOURCE_LIMIT_MEM}GB in docker advanced preferences. check: Failed"
				echo "`current_date_time`: WARN: Dokcer Total Memory limitation is set to ${MACOS_RESOURCE_LIMIT_MEM}GB in docker advanced preferences. check: Failed"
				MEMORY_CHECK_RC=1
			else
				info "Dokcer Total Memory limitation is set to ${MACOS_RESOURCE_LIMIT_MEM}GB in docker advanced preferences. check: Passed"
				echo "`current_date_time`: INFO: Dokcer Total Memory limitation is set to ${MACOS_RESOURCE_LIMIT_MEM}GB in docker advanced preferences. check: Passed"
				MEMORY_CHECK_RC=0
			fi
		fi
	fi
	
	echo " "
	

	return ${MEMORY_CHECK_RC}

}

function ask_for_hw_req_user_override() {

	local USER_RESPONSE="q"
	local ASK_FOR_HW_REQ_USER_OVERRIDE_RC=0

	while [ ! "${USER_RESPONSE}" == "y" ] && [ ! "${USER_RESPONSE}" == "n" ]
	do
		echo -n "Do you want to continue ? [y/n]: "
		read USER_RESPONSE
		echo " "
	done

	if [[ ${USER_RESPONSE} == "n" ]]; then
		error "The minimal hardware requirements are not met. Abort."
		echo "`current_date_time`: ERROR: The minimal hardware requirements are not met. Abort."
		ASK_FOR_HW_REQ_USER_OVERRIDE_RC=1
		ABORT=1
	else
		info "User chose to continue."
		echo "`current_date_time`: INFO: User chose to continue."
	fi

	return ${ASK_FOR_HW_REQ_USER_OVERRIDE_RC}
}


function upgrade_simulator() {

	local RESOURCES_LOG_DIR="${RESOURCES_DIR}/logs"
	local LOG_FILE_NAME="versionUpdateActivation_${NOW}.log"
	local LOG_FILE=${RESOURCES_LOG_DIR}/${LOG_FILE_NAME}
	local DEST_DIR=`echo ${RESOURCES_DIR} | sed "s/\/resources\/${SIMULATOR_VERSION}/\/src/g"`
	local SCRIPT_NAME="versionUpgrade.sh"

	local RESOURCES_DIR_NO_VERSION=`echo ${RESOURCES_DIR} | sed "s/\/${SIMULATOR_VERSION}//g"`
	local UPGRADE_LOG_FILE_DIR="${RESOURCES_DIR_NO_VERSION}/logs"
	local UPGRADE_LOG_FILE="${UPGRADE_LOG_FILE_DIR}/versionUpgrade.log"

	info "Copying upgrade script '${LOCAL_FILES_DIR}/${SCRIPT_NAME}' to external directory ${DEST_DIR}"
	echo "`current_date_time`: INFO: Copying upgrade script '${LOCAL_FILES_DIR}/${SCRIPT_NAME}' to external directory ${DEST_DIR}"
	\cp ${LOCAL_FILES_DIR}/${SCRIPT_NAME} ${DEST_DIR} >> ${LOG_FILE} 2>&1

	info "Executing upgrade script"
	info "See log file for details: ${UPGRADE_LOG_FILE}"
	echo "`current_date_time`: INFO: Executing upgrade script"
	echo "`current_date_time`: INFO: See log file for details: ${UPGRADE_LOG_FILE}"
	cd ${DEST_DIR} >> ${LOG_FILE} 2>&1
	nohup ./${SCRIPT_NAME} >> /dev/null 2>&1 &

}
