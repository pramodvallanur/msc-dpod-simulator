#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

echo "Disabling IPv6..."
sysctl -w net.ipv6.conf.all.disable_ipv6=1
sysctl -w net.ipv6.conf.default.disable_ipv6=1
echo "---------------------------------------------------------------------------"

echo "Installing NTP..."
systemctl stop chronyd
systemctl disable chronyd
yum -y -q install ntp
systemctl enable ntpd
systemctl start ntpd
echo "---------------------------------------------------------------------------"

echo "Installing EPEL-repositories & X Window system..."
yum -y -q install epel-release
yum -y -q groupinstall "X Window system"
echo "---------------------------------------------------------------------------"

echo "Installing MATE Desktop..."
yum -y -q groupinstall mate-desktop
echo "---------------------------------------------------------------------------"

echo "Configuring system to start graphical insterface..."
systemctl set-default graphical.target
echo "---------------------------------------------------------------------------"

echo "Adding Google Chrome Repository..."
cp $SCRIPT_DIR/*.repo /etc/yum.repos.d/
echo "---------------------------------------------------------------------------"

echo "Installing managed applications..."
yum -y -q install open-vm-tools open-vm-tools-desktop open-vm-tools-devel google-chrome-stable culmus-* alef-fonts* dejavu-* google-noto-sans-hebrew-fonts
echo "---------------------------------------------------------------------------"

echo "Adding desktop launchers..."
mkdir ~/Desktop
cp $SCRIPT_DIR/launchers/* ~/Desktop/
echo "---------------------------------------------------------------------------"

echo "Setting up Docker environment..."
~/msc-dpod-simulator/src/msc-dpod-simulator/setupDockerEnv.sh
echo "---------------------------------------------------------------------------"

echo "Downloading DPOD image..."
source ~/msc-dpod-simulator/src/msc-dpod-simulator/stable/scripts/buildProperties.sh
mkdir -p ~/msc-dpod-simulator/resources/stable/dpod
wget -q --output-document=/root/msc-dpod-simulator/resources/stable/dpod/dpod.tgz ${DPOD_IMAGE_URL}
echo "---------------------------------------------------------------------------"

echo "OVA is ready"
echo "Press [ENTER] to shut down:"
read confirm_continue
shutdown -h now
