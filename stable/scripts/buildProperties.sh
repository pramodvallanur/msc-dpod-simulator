#!/bin/bash

# Parameters for DPOD image files
DPOD_IMAGE_URL="https://www.dropbox.com/s/em0uyn0n46wc9pl/ibm-DPOD-DOCKER_LIGHT-ibmnologo-1.0.8.6__prod-2018-12-11_06_01_09_436-IST-BLD119-latest.tgz?dl=0"
DPOD_MD5_URL="https://www.dropbox.com/s/f9qhpo457bkj7hq/ibm-DPOD-DOCKER_LIGHT-ibmnologo-1.0.8.6__prod-2018-12-11_06_01_09_436-IST-BLD119-latest.tgz.md5?dl=0"

# version of idg2 
export IDG2_IMAGE_TAG="7.7.1"



