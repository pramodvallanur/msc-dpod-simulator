#!/bin/bash

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0

#################################################################################################################


#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}

	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo $"Usage: $0 "
	echo "	Or :  $0 -h|--help"
	echo " "
	return 0
}


###########################
# variables and constants #
###########################


NOW=$( date +"%F_%H-%M-%S")

LOCAL_FILES_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)" 

RESOURCES_DIR=`echo ${LOCAL_FILES_DIR} | sed "s/\/src/\/resources/g"`

LOG_DIR="${RESOURCES_DIR}/logs"
LOG_FILE_NAME="versionUpgrade.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

HOME_DIR=`echo ~`
DIR_TO_REMOVE="${HOME_DIR}/msc-dpod-simulator/src/msc-dpod-simulator"
DPOD_SIMULATOR_URL="https://bitbucket.org/montier/msc-dpod-simulator/get/master.tar.gz"
DPOD_SIMULATOR_LOCAL_FILE="msc-dpod-simulator.tgz"

GIT_DIR_EXIST=0


########################
#      Main Logic      #
########################

mkdir -p ${LOG_DIR}

GIT_DIR_EXIST=`ls -la ${DIR_TO_REMOVE} | grep -w .git | wc -l`

info "======================================================"

if [[ ! ${GIT_DIR_EXIST} -eq 0 ]]; then

	info "Removing directory ${DIR_TO_REMOVE}"
	echo "`current_date_time`: INFO: Removing directory ${DIR_TO_REMOVE}"
	rm -Rf ${DIR_TO_REMOVE} >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		error "Failed to remove directory. "
		echo "`current_date_time`: INFO: Failed to clone from git. "
		ABORT=1
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Clonning new 'dpodSimulator' from 'git'"
		echo "`current_date_time`: INFO: Clonning new 'dpodSimulator' from 'git'"
		git clone https://bitbucket.org/montier/msc-dpod-simulator.git >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to clone from git. "
			echo "`current_date_time`: INFO: Failed to clone from git. "
			ABORT=1
		fi
	fi

else

	info "Downloading new 'dpodSimulator' version, using 'curl'"
	echo "`current_date_time`: INFO: Downloading new 'dpodSimulator' version, using 'curl'"
	curl -s -L -o ${DPOD_SIMULATOR_LOCAL_FILE} ${DPOD_SIMULATOR_URL} >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		error "Failed to download new version. "
		echo "`current_date_time`: INFO: Failed to clone from git. "
		ABORT=1
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Removing files from directory ${DIR_TO_REMOVE}"
		echo "`current_date_time`: INFO: Removing files from directory ${DIR_TO_REMOVE}"
		rm -Rf ${DIR_TO_REMOVE}/* >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to remove directory. "
			echo "`current_date_time`: INFO: Failed to clone from git. "
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Extracting new version"
		echo "`current_date_time`: INFO: Extracting new version"
		tar -zxvf ${DPOD_SIMULATOR_LOCAL_FILE} -C ${DIR_TO_REMOVE} --strip-components=1 >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to extract new version."
			echo "`current_date_time`: INFO: Failed to extract new version."
			ABORT=1
		fi
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Granting execution permissions to new version"
	echo "`current_date_time`: INFO: Granting execution permissions to new version"
	chmod +x ${DIR_TO_REMOVE}/*.sh >> ${LOG_FILE} 2>&1
	chmod +x ${DIR_TO_REMOVE}/*/scripts/*.sh >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		error "Failed grant permissions."
		echo "`current_date_time`: INFO: Failed grant permissions."
		ABORT=1
	fi
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Cleaning.."
	echo "`current_date_time`: INFO: Cleaning.."
	rm -f ${DPOD_SIMULATOR_LOCAL_FILE} >> ${LOG_FILE} 2>&1

	info "Version upgraded successfully !"
	echo "`current_date_time`: INFO: Version upgraded successfully !"
else
	error "Please try to upgrade version manually: "
	error "====> Using git : "
	error "==========> rm -Rf ${DIR_TO_REMOVE} "
	error "==========> git clone https://bitbucket.org/montier/msc-dpod-simulator.git"
	error "==========> chmod +x ${DIR_TO_REMOVE}/*.sh"
	error "==========> chmod +x ${DIR_TO_REMOVE}/*/srcipts/*.sh"
	error "====> Using curl : "
	error "==========> curl -s -L -o ${DPOD_SIMULATOR_LOCAL_FILE} ${DPOD_SIMULATOR_URL} "
	error "==========> rm -Rf ${DIR_TO_REMOVE} "
	error "==========> tar -zxvf ${DPOD_SIMULATOR_LOCAL_FILE}"
	error "==========> chmod +x ${DIR_TO_REMOVE}/*.sh"
	error "==========> chmod +x ${DIR_TO_REMOVE}/*/srcipts/*.sh"
	echo "`current_date_time`: ERROR: Please try to upgrade version manually: "
	echo "`current_date_time`: ERROR: 		Using git : "
	echo "`current_date_time`: ERROR: 			rm -Rf ${DIR_TO_REMOVE} "
	echo "`current_date_time`: ERROR: 			git clone https://bitbucket.org/montier/msc-dpod-simulator.git"
	echo "`current_date_time`: ERROR: 			chmod +x ${DIR_TO_REMOVE}/*.sh"
	echo "`current_date_time`: ERROR: 			chmod +x ${DIR_TO_REMOVE}/*/scripts/*.sh"
	echo "`current_date_time`: ERROR: 		Using curl : "
	echo "`current_date_time`: ERROR: 			curl -s -L -o ${DPOD_SIMULATOR_LOCAL_FILE} ${DPOD_SIMULATOR_URL} "
	echo "`current_date_time`: ERROR: 			rm -Rf ${DIR_TO_REMOVE} "
	echo "`current_date_time`: ERROR: 			tar -zxvf ${DPOD_SIMULATOR_LOCAL_FILE}"
	echo "`current_date_time`: ERROR: 			chmod +x ${DIR_TO_REMOVE}/*.sh"
	echo "`current_date_time`: ERROR: 			chmod +x ${DIR_TO_REMOVE}/*/scripts/*.sh"
fi

exit ${ABORT}
