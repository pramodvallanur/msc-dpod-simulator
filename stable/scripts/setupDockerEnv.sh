#!/bin/bash

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0

#################################################################################################################


#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}

	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo $"Usage: $0 "
	echo "	Or :  $0 -h|--help"
	echo " "

	return 0
}


function install_prerequisites_ubuntu() {

	# Update the apt package index
	if [[ ${ABORT} -eq 0 ]]; then
		info "Updating the apt package index"
		echo "`current_date_time`: INFO: Updating the apt package index"
		apt-get update >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to update the apt package index. Abort"
			echo "`current_date_time`: ERROR: Failed to update the apt package index. Abort"
			ABORT=1
		fi
	fi

	# Install packages to allow apt to use a repository over HTTPS
	if [[ ${ABORT} -eq 0 ]]; then
		info "Install packages to allow apt to use a repository over HTTPS"
		echo "`current_date_time`: INFO: Install packages to allow apt to use a repository over HTTPS"
		${PACKAGE_INSTALLER_COMMAND} apt-transport-https ca-certificates curl software-properties-common wget >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to install packages. Abort"
			echo "`current_date_time`: ERROR: Failed to install packages. Abort"
			ABORT=1
		fi
	fi

	# Add Docker’s official GPG key
	if [[ ${ABORT} -eq 0 ]]; then
		info "Add Docker’s official GPG key"
		echo "`current_date_time`: INFO: Add Docker’s official GPG key"
		curl -fsSL https://download.docker.com/linux/ubuntu/gpg |  apt-key add - >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to add Docker’s official GPG key. Abort"
			echo "`current_date_time`: ERROR: Failed to add Docker’s official GPG key. Abort"
			ABORT=1
		fi
	fi

	# Verify that the key with the fingerprint was added
	if [[ ${ABORT} -eq 0 ]]; then
		info "Verifying that the key was added"
		echo "`current_date_time`: INFO: Verifying that the key was added"
		KEY_ADDED=`apt-key fingerprint 0EBFCD88 2>/dev/null | wc -l`
		if [ $? -ne 0 ]; then
			error "Could not verify that key was added. Abort"
			echo "`current_date_time`: ERROR: Could not verify that key was added. Abort"
			ABORT=1
		else
			if [[ ${KEY_ADDED} -eq 0 ]]; then
				error "key was not added. Abort"
				echo "`current_date_time`: ERROR: key was not added. Abort"
				ABORT=1
			fi
		fi
	fi

	# Add repository
	if [[ ${ABORT} -eq 0 ]]; then
		info "Adding docker-ce repository"
		echo "`current_date_time`: INFO: Adding docker-ce repository"
		add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu artful stable" >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to add docker-ce repository. Abort"
			echo "`current_date_time`: ERROR: Failed to add docker-ce repository. Abort"
			ABORT=1
		fi
	fi

	# Update the apt package index again
	if [[ ${ABORT} -eq 0 ]]; then
		info "Updating the apt package index"
		echo "`current_date_time`: INFO: Updating the apt package index"
		apt-get update >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to update the apt package index. Abort"
			echo "`current_date_time`: ERROR: Failed to update the apt package index. Abort"
			ABORT=1
		fi
	fi
}


function install_prerequisites_centos_redhat() {

	info "Installing yum-utils"
	echo "`current_date_time`: INFO: Installing yum-utils"
	${PACKAGE_INSTALLER_COMMAND} yum-utils >> ${LOG_FILE} 2>&1
	if [ $? -ne 0 ]; then
		error "Failed to install yum-utils. Abort"
		echo "`current_date_time`: ERROR: Failed to install yum-utils. Abort"
		ABORT=1
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Adding docker-ce repository to yum"
		echo "`current_date_time`: INFO: Adding docker-ce repository to yum"
		yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to add docker-ce repository to yum. Abort"
			echo "`current_date_time`: ERROR: Failed to add docker-ce repository to yum. Abort"
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Executing 'yum makecache' command"
		echo "`current_date_time`: INFO: Executing 'yum makecache' command"
		yum makecache fast >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Command 'yum makecache' returned a bad exit code. Abort"
			echo "`current_date_time`: ERROR: Command 'yum makecache' returned a bad exit code. Abort"
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Installing 'curl'"
		echo "`current_date_time`: INFO: Installing 'curl'"
		${PACKAGE_INSTALLER_COMMAND} curl >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to install 'curl'. Abort"
			echo "`current_date_time`: ERROR: Failed to install 'curl'. Abort"
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Installing 'wget'"
		echo "`current_date_time`: INFO: Installing 'wget'"
		${PACKAGE_INSTALLER_COMMAND} wget >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to install 'wget'. Abort"
			echo "`current_date_time`: ERROR: Failed to install 'wget'. Abort"
			ABORT=1
		fi
	fi
}


function install_docker_ce() {

	if [[ ${ABORT} -eq 0 ]]; then
		info "Installing docker-ce"
		echo "`current_date_time`: INFO: Installing docker-ce"
		${PACKAGE_INSTALLER_COMMAND} docker-ce >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to install docker-ce. Abort"
			echo "`current_date_time`: ERROR: Failed to install docker-ce. Abort"
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Starting docker service"
		echo "`current_date_time`: INFO: Starting docker service"
		systemctl start docker >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to Start docker service. Abort"
			echo "`current_date_time`: ERROR: Failed to Start docker service. Abort"
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Configuring docker service to auto-start at server startup"
		echo "`current_date_time`: INFO: Configuring docker service to auto-start at at server startup"
		systemctl enable docker >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to configure docker service to auto-start. Abort"
			echo "`current_date_time`: ERROR: Failed to configure docker service to auto-start. Abort"
			ABORT=1
		fi
	fi
}


function install_docker_compose() {

	if [[ ${ABORT} -eq 0 ]]; then
		info "Installing docker-compose"
		echo "`current_date_time`: INFO: Installing docker-compose"
		sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to Install docker-compose. Abort"
			echo "`current_date_time`: ERROR: Failed to Install docker-compose. Abort"
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Granting execution privileges to docker-compose"
		echo "`current_date_time`: INFO: Granting execution privileges to docker-compose"
		sudo chmod +x /usr/local/bin/docker-compose >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to grant execution privileges to docker-compose. Abort"
			echo "`current_date_time`: ERROR: Failed to grant execution privileges to docker-compose. Abort"
			ABORT=1
		fi
	fi
}


function handle_iptables() {

	if [[ ! ${LOW_DIST} == "ubuntu" ]]; then
		info "Installing iptables-services"
		echo "`current_date_time`: INFO: Installing iptables-services"
		${PACKAGE_INSTALLER_COMMAND} iptables-services >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to Install iptables-services. Abort"
			echo "`current_date_time`: ERROR: Failed to Install iptables-services. Abort"
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Adding DPOD syslog agents ports to iptables"
		echo "`current_date_time`: INFO: Adding DPOD syslog agents ports to iptables"
		iptables -I INPUT 1  -p tcp -m tcp -s 0.0.0.0/0 -d 0.0.0.0/0 --dport 60000 -j ACCEPT >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to Add DPOD syslog agents ports to iptables. Abort"
			echo "`current_date_time`: ERROR: Failed to Add DPOD syslog agents ports to iptables. Abort"
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Adding DPOD wsm agents ports to iptables"
		echo "`current_date_time`: INFO: Adding DPOD wsm agents ports to iptables"
		iptables -I INPUT 1 -p tcp -m tcp -s 0.0.0.0/0 -d 0.0.0.0/0 --dport 60020 -j ACCEPT >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to Add DPOD wsm agents ports to iptables. Abort"
			echo "`current_date_time`: ERROR: Failed to Add DPOD wsm agents ports to iptables. Abort"
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Saving iptables config"
		echo "`current_date_time`: INFO: Saving iptables config"
		iptables-save >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to save iptables config. Abort"
			echo "`current_date_time`: ERROR: Failed to save iptables config. Abort"
			ABORT=1
		fi
	fi
}


function handle_redhat_repositories() {

	info "Running subscription-manager autoattach: 'subscription-manager attach --auto'"
	echo "`current_date_time`: INFO: Running subscription-manager autoattach: 'subscription-manager attach --auto'"
	subscription-manager attach --auto
	if [ $? -ne 0 ]; then
		error "Failed to autoattach subscriptions. Abort"
		echo "`current_date_time`: ERROR: Failed to autoattach subscriptions. Abort"
		ABORT=1
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Enabling 'rhel-7-server-extras-rpms' repository"
		echo "`current_date_time`: INFO: Enabling to 'rhel-7-server-extras-rpms' repository"
		subscription-manager repos --enable rhel-7-server-extras-rpms
		if [ $? -ne 0 ]; then
			error "Failed to Enable 'rhel-7-server-extras-rpms' repository. Abort"
			echo "`current_date_time`: ERROR: Failed to Enable 'rhel-7-server-extras-rpms' repository. Abort"
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Enabling 'rhel-7-server-optional-rpms' repository"
		echo "`current_date_time`: INFO: Enabling 'rhel-7-server-optional-rpms' repository"
		subscription-manager repos --enable rhel-7-server-optional-rpms
		if [ $? -ne 0 ]; then
			error "Failed to Enable 'rhel-7-server-optional-rpms' repository. Abort"
			echo "`current_date_time`: ERROR: Failed to Enable 'rhel-7-server-optional-rpms' repository. Abort"
			ABORT=1
		fi
	fi
}


function install_epel() {

	if [[ ${ABORT} -eq 0 ]]; then
		info "Downloading 'epel-release' RPM"
		echo "`current_date_time`: INFO: Downloading 'epel-release' RPM"
		wget http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to download 'epel-release'. Abort"
			echo "`current_date_time`: ERROR: Failed to download 'epel-release'. Abort"
			ABORT=1
		fi
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		info "Installing 'epel-release' RPM"
		echo "`current_date_time`: INFO: Installing 'epel-release' RPM"
		rpm -ivh --force epel-release-latest-7.noarch.rpm >> ${LOG_FILE} 2>&1
		if [ $? -ne 0 ]; then
			error "Failed to install 'epel-release'. Abort"
			echo "`current_date_time`: ERROR: Failed to install 'epel-release'. Abort"
			ABORT=1
		fi
	fi
}


function lowercase(){
    echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}

function discover_os_type() {
	OS=`lowercase \`uname\``
	KERNEL=`uname -r`
	MACH=`uname -m`

	if [ "${OS}" == "windowsnt" ]; then
		OS=windows
	elif [ "${OS}" == "darwin" ]; then
		OS=mac
	else
		OS=`uname`
		if [ "${OS}" = "SunOS" ] ; then
			OS=Solaris
			ARCH=`uname -p`
			OSSTR="${OS} ${REV}(${ARCH} `uname -v`)"
		elif [ "${OS}" = "AIX" ] ; then
			OSSTR="${OS} `oslevel` (`oslevel -r`)"
		elif [ "${OS}" = "Linux" ] ; then
			if [ -f /etc/redhat-release ] ; then
				DistroBasedOn='RedHat'
				DIST=`cat /etc/redhat-release |sed s/\ release.*//`
				PSUEDONAME=`cat /etc/redhat-release | sed s/.*\(// | sed s/\)//`
				REV=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*//`
			elif [ -f /etc/SuSE-release ] ; then
				DistroBasedOn='SuSe'
				PSUEDONAME=`cat /etc/SuSE-release | tr "\n" ' '| sed s/VERSION.*//`
				REV=`cat /etc/SuSE-release | tr "\n" ' ' | sed s/.*=\ //`
			elif [ -f /etc/mandrake-release ] ; then
				DistroBasedOn='Mandrake'
				PSUEDONAME=`cat /etc/mandrake-release | sed s/.*\(// | sed s/\)//`
				REV=`cat /etc/mandrake-release | sed s/.*release\ // | sed s/\ .*//`
			elif [ -f /etc/debian_version ] ; then
				DistroBasedOn='Debian'
				DIST=`cat /etc/lsb-release | grep '^DISTRIB_ID' | awk -F=  '{ print $2 }'`
				PSUEDONAME=`cat /etc/lsb-release | grep '^DISTRIB_CODENAME' | awk -F=  '{ print $2 }'`
				REV=`cat /etc/lsb-release | grep '^DISTRIB_RELEASE' | awk -F=  '{ print $2 }'`
			fi
			if [ -f /etc/UnitedLinux-release ] ; then
				DIST="${DIST}[`cat /etc/UnitedLinux-release | tr "\n" ' ' | sed s/VERSION.*//`]"
			fi
			OS=`lowercase $OS`
			DistroBasedOn=`lowercase $DistroBasedOn`
			readonly OS
			readonly DIST
			readonly DistroBasedOn
			readonly PSUEDONAME
			readonly REV
			readonly KERNEL
			readonly MACH
		fi
	fi
}

function handle_ubuntu() {

	PACKAGE_INSTALLER_COMMAND="apt-get install -y"

	install_prerequisites_ubuntu

	if [[ ${ABORT} -eq 0 ]]; then
		install_docker_ce
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		install_docker_compose
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		handle_iptables
	fi
}

function handle_centos() {

	PACKAGE_INSTALLER_COMMAND="yum -y -q install"

	install_prerequisites_centos_redhat

	if [[ ${ABORT} -eq 0 ]]; then
		install_docker_ce
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		install_docker_compose
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		handle_iptables
	fi
}

function handle_redhat() {

	PACKAGE_INSTALLER_COMMAND="yum -y -q install"

	handle_redhat_repositories

	if [[ ${ABORT} -eq 0 ]]; then
		install_prerequisites_centos_redhat
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		install_epel
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		install_docker_ce
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		install_docker_compose
	fi

	if [[ ${ABORT} -eq 0 ]]; then
		handle_iptables
	fi
}


###########################
# variables and constants #
###########################


NOW=$( date +"%F_%H-%M-%S")

## TODO - CHECK ON UBUNTO AND MACOS
LOCAL_FILES_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

ACTUAL_SIMULATOR_VERSION=`basename $(dirname ${LOCAL_FILES_DIR})`
RESOURCES_DIR=`echo ${LOCAL_FILES_DIR} | sed "s/\/src\/msc-dpod-simulator\/${ACTUAL_SIMULATOR_VERSION}\/scripts/\/resources\/${ACTUAL_SIMULATOR_VERSION}/g"`

LOG_DIR="${RESOURCES_DIR}/logs"
LOG_FILE_NAME="setupDockerEnv_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		*)
			shift 1
	esac
done


############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}

info "Checking host OS type"
echo "`current_date_time`: INFO: Checking host OS type"
discover_os_type
info "The OS type is: ${OS}, Distribution is: ${DIST}"
echo "`current_date_time`: INFO: The OS type is: ${OS}, Distribution is: ${DIST}"

LOW_DIST=`lowercase ${DIST}`

if [ ${OS} == "linux" ] && [ ${LOW_DIST} == "ubuntu" ] ; then
	handle_ubuntu
elif [ ${OS} == "linux" ] && [ ${LOW_DIST} == "centos" ] ; then
	handle_centos
elif [ ${OS} == "linux" ] && [ ${LOW_DIST} == "red" ] ; then
	handle_redhat
else
	error "This script only supports the following distributions of Linux: CentOS/Red-Hat/Ubuntu. Abort."
	echo "`current_date_time`: ERROR: This script only supports the following distributions of Linux: CentOS/Red-Hat/Ubuntu. Abort."
fi

if [[ ${ABORT} -eq 0 ]]; then
	info "Setup completed successfully ! "
	echo "`current_date_time`: INFO: Setup completed successfully ! "
else
	error "Setup Failed. See log file for details: ${LOG_FILE}"
	echo "`current_date_time`: ERROR: Setup failed ! See log file for details: ${LOG_FILE}"
fi


exit ${ABORT}
