#!/bin/bash

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0

#################################################################################################################


#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}

	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}

function show_help {
	echo $"Usage: $0 "
	echo "	Or :  $0 -h|--help"
	echo " "
	return 0
}



###########################
# variables and constants #
###########################


NOW=$( date +"%F_%H-%M-%S")

#LOCAL_FILES_DIR=`pwd`
## TODO - CHECK ON UBUNTO AND MACOS
LOCAL_FILES_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"


ACTUAL_SIMULATOR_VERSION=`basename $(dirname ${LOCAL_FILES_DIR})`
RESOURCES_DIR=`echo ${LOCAL_FILES_DIR} | sed "s/\/src\/msc-dpod-simulator\/${ACTUAL_SIMULATOR_VERSION}\/scripts/\/resources\/${ACTUAL_SIMULATOR_VERSION}/g"`

LOG_DIR="${RESOURCES_DIR}/logs"
LOG_FILE_NAME="cleanDPODsimulatorEnv_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

IDG_VOLUME_DIR="${RESOURCES_DIR}/datapower/volumes*/*"

USER_RESPONSE="empty"

DOCUMENTATION_URL="https://bitbucket.org/montier/msc-dpod-simulator/src/master/"




#############
# 	Main	#
#############


mkdir -p ${LOG_DIR}

echo "`current_date_time`: WARN: This action will remove all docker components from this server."
echo "`current_date_time`: WARN: ### Even non related containers to DPOD will be removed !!! ### "
echo "`current_date_time`: WARN: Please see 'dpodSimulator' documentation for more details : ${DOCUMENTATION_URL}"

while [ ! "${USER_RESPONSE}" == "y" ] && [ ! "${USER_RESPONSE}" == "n" ]
do
	echo -n "Do you want to continue ? [y/n]: "; read USER_RESPONSE
	echo " "
done

if [[ ${USER_RESPONSE} == "n" ]]; then
	info "User aborted execution."
	echo "`current_date_time`: INFO: User aborted execution."
	ABORT=1
else
	info "User chose to continue script execution."
	echo "`current_date_time`: INFO: User chose to continue script execution."

	info "Deleting IDG volumes"
	echo "`current_date_time`: INFO: Deleting IDG volumes"
	rm -Rf ${IDG_VOLUME_DIR} >> ${LOG_FILE} 2>&1

	info "Checking for existing docker containers"
	echo "`current_date_time`: INFO: Checking for existing docker containers"
	NUM_OF_EXISTING_CONTAINERS=`docker ps -a -q | wc -l`

	if [[ ${NUM_OF_EXISTING_CONTAINERS} -ne 0 ]]; then
		info "Stopping all existing docker containers"
		echo "`current_date_time`: INFO: Stopping all existing docker containers"
		docker stop $(docker ps -a -q) >> ${LOG_FILE} 2>&1

		info "Removing all existing docker containers"
		echo "`current_date_time`: INFO: Removing all existing docker containers"
		docker rm $(docker ps -a -q) >> ${LOG_FILE} 2>&1
	else
		info "There are no docker containers on the server"
		echo "`current_date_time`: INFO: There are no docker containers on the server"
	fi

	info "Checking for existing docker images"
	echo "`current_date_time`: INFO: Checking for existing docker images"
	NUM_OF_EXISTING_IMAGES=`docker images -q | wc -l`

	if [[ ${NUM_OF_EXISTING_IMAGES} -ne 0 ]]; then
		info "Removing all existing docker images"
		echo "`current_date_time`: INFO: Removing all existing docker images"
		docker rmi $(docker images -q) -f >> ${LOG_FILE} 2>&1
	else
		info "There are no docker images on the server"
		echo "`current_date_time`: INFO: There are no docker images on the server"
	fi

	info "Removing all existing docker networks"
	echo "`current_date_time`: INFO: Removing all existing docker networks"
	docker network prune -f >> ${LOG_FILE} 2>&1
fi
