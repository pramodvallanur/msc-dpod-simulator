#!/bin/bash

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0

#################################################################################################################


#############
# functions #
#############


function show_help {
	echo $"Usage: $0 -a|--action (action) [-i|--additional-idg] [-k|--add-splunk]"
	echo "   Or: $0 -h|--help"
	echo " "
	echo "       -a|--action : The action to take. This is a mandatory parameter that must contain one of the following values:"
	echo "                build    - build simulator"
	echo "                stop     - stop simulator"
	echo "                start    - start simulator"
	echo "                clean    - clean all simulator containers and images"
	echo "                upgrade  - get the latest version of the dpodSimulator from the web"
	echo "                timesync - resync time of all containers to the time of the host"
    echo " "
	echo "       -i|--additional-idg : If this flag if raised, a second idg will be created. "
	echo "                This is an optional parameter. Parameter is valid only for 'build' action."
    echo " "
    echo "       -k|--add-splunk : If this flag if raised, a splunk container will be created."
	echo "                This is an optional parameter. Parameter is valid only for 'build' action."
	echo "       -l|--leg : The address of the network interface that will be used to connect to the Operations Dashboard Web UI"
	echo "                This is an optional parameter. Parameter is valid only for 'build' action."
	echo "					If this parameter is not given and there are multiple interfaces on the host, the simulator will present the user with the available interfaces and user will choose one."
    echo " "
	echo "       -h|--help : Display help"
	echo " "
	echo "For Example: "
	echo "       $0 -a build -i -k"
	echo "   Or : "
	echo "       $0 -a start"
	echo "   Or : "
	echo "       $0 --action upgrade"
	return 0
}

###########################
# variables and constants #
###########################


NOW=$( date +"%F_%H-%M-%S")

LOCAL_FILES_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

#echo "`date +"%F_%H-%M-%S"`: INFO: Obtaining build propetries"
source ${LOCAL_FILES_DIR}/buildProperties.sh
if [ $? -ne 0 ]; then
	echo "`date +"%F_%H-%M-%S"`: ERROR: Could not obtain build properties. Abort."
	exit 1
fi


SCRIPT_BASE_NAME=`basename $0`

ACTUAL_SIMULATOR_VERSION=`basename $(dirname ${LOCAL_FILES_DIR})`
SIMULATOR_VERSION="stable"

RESOURCES_DIR=`echo ${LOCAL_FILES_DIR} | sed "s/\/src\/msc-dpod-simulator\/${ACTUAL_SIMULATOR_VERSION}\/scripts/\/resources\/${ACTUAL_SIMULATOR_VERSION}/g"`

LOG_DIR="${RESOURCES_DIR}/logs"
LOG_FILE_NAME="dpodSimulator_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

LOCAL_DPOD_MD5_FILE="${RESOURCES_DIR}/dpod/dpod.md5"
LOCAL_DPOD_IMAGE_FILE="${RESOURCES_DIR}/dpod/dpod.tgz"

STOP_TIMEOUT="60"
SLEEP_BETWEEN_UI_TESTS=15
MAX_UI_TESTS=30
MAX_UI_WAIT_SECONDS=$(( SLEEP_BETWEEN_UI_TESTS * MAX_UI_TESTS ))
SEC_TO_WAIT_AFTER_DEVICE_ADD="60"

ADD_IDG_TO_DPOD_RC=0

MD5_SIG_MATCH=0

# Parameters for idg setup in dpod
DPOD_USER=admin
DPOD_PASS=adminuser
DPOD_ADDRESS=172.77.77.4
IDG1_ADDRESS=172.77.77.3
IDG2_ADDRESS=172.77.77.6
SOMA_PORT=5550
SOMA_USER=admin
SOMA_PASS=admin
LOG_TAR_ADDRESS_1=172.77.77.3
LOG_TAR_ADDRESS_2=172.77.77.6
MONITOR_RESOURCES=true
MONITOR_SERVICES=true
SYS_AGNT_NAME=MonTier-SyslogAgent-1
SETUP_SYS_AGNT=true
AUTO_SETUP_AGNT_NAME=MonTier-SyslogAgent-1
WSM_AGNT_NAME=MonTier-WsmAgent-1
SETUP_WSM=true
SETUP_CERT_MON=true
AUTO_SETUP_PATTERN=APIMgmt*

GENERATE_SIMULATION_SCRIPT="/MonTier/scripts/GenerateSimulationData.sh"
GENERATE_SIMULATION_LOG="/MonTier/scripts/GenerateSimulationData"
TX_GENERATOR_SLEEP_TIME=300

TRANSACTIONS_ACTIVATION_LOG_FILE="/MonTier/scripts/dpodSimulatorTransActivation.log"
TRANSACTIONS_ACTIVATION_SCRIPT="/MonTier/scripts/ActivateAllTransactions.sh"

IDG_INITIAL_CONF_PREFIX="../datapower/initialConfig_idg"

IDG_VOLUME_DIR_PREFIX="${RESOURCES_DIR}/datapower/volumes_idg"

SPLUNK_INITIAL_CONF="../splunk/initialConfig_splunk/*"
SPLUNK_VOLUME_DIR="${RESOURCES_DIR}/splunk/volumes_splunk"

COMPOSE_FILE_PATH_MAC="../docker-compose_mac.yml"
COMPOSE_FILE_PATH="../docker-compose.yml"

CLEAN_SIMULATOR_TIMEOUT="120"

DOCKER_SERVICE_TO_START=centos
ADDITIONAL_IDG_FLAG_UP=0
HTTP_PROXY_FLAG_UP=0
HTTPS_PROXY_FLAG_UP=0
SPLUNK_FLAG_UP=0
IGNORE_HW_FLAG_UP=0
LEG_FLAG_UP=0

SPLUNK_DPOD_TRAN_VIEW_FILE="${SPLUNK_VOLUME_DIR}/dpod.app/local/data/ui/views/dpod_transactions_list.xml"

MIN_MEMORY_SIZE=16
MIN_CPU_NO=8

# screen text color
NC='\033[0m' # No Color
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
RED="\033[0;31m"

CENTOS_CONTAINER="centos_dpodsimulator"
DPOD_CONTAINER="dpod_dpodsimulator"
SPLUNK_CONTAINER="splunk_dpodsimulator"

CHOSEN_HOST_ADDRESS="<hostIp>"
USER_INPUT_FILE="${RESOURCES_DIR}/buildProperties.txt"

INSTALL_CONTAINER_TOOLS_SCRIPT="/MonTier/scripts/InstallPrereq.sh"
DPOD_SSH_PASS="dpod"

NUMBER_OF_IDG_SERVICES_TO_HANDLE=1

SPLUNK_OPERATIONS_SCRIPT="/MonTier/scripts/ActivateSplunkOperations.sh"
SLEEP_BETWEEN_SPLUNK_RESTARTS="86400"
SPLUNK_OPERATIONS_ACTIVATION_LOG_FILE="/MonTier/scripts/splunkOperationsActivation.log"


#############
# 	Main	#
#############


######## 	source functions  	#########

#echo "`date +"%F_%H-%M-%S"`: INFO: Sourcing simulator functions"
source ${LOCAL_FILES_DIR}/dopdSimulatorFunctions.sh
if [ $? -ne 0 ]; then
	echo "`date +"%F_%H-%M-%S"`: ERROR: Could not obtain build properties. Abort."
	exit 1
fi


######## 	process parameters  	#########


while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-a|--action)
			ACTION=$2
			if [[ -z ${ACTION} ]]; then
				echo "ERROR : Invalid parameters. Action is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-p|--http-proxy)
			HTTP_PROXY_FLAG_UP=1
			HTTP_PROXY=$2
			if [[ -z ${HTTP_PROXY} ]]; then
				echo "ERROR : Invalid parameters. HTTP proxy is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-s|--https-proxy)
			HTTPS_PROXY_FLAG_UP=1
			HTTPS_PROXY=$2
			if [[ -z ${HTTPS_PROXY} ]]; then
				echo "ERROR : Invalid parameters. HTTPS proxy is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-i|--additional-idg)
			ADDITIONAL_IDG_FLAG_UP=1
			shift 1
			;;
		-k|--add-splunk)
			SPLUNK_FLAG_UP=1
			shift 1
			;;
		-r|--ignore-hardware-req)
			IGNORE_HW_FLAG_UP=1
			shift 1
			;;
		-v|--version)
			SIMULATOR_VERSION=$2
			if [[ -z ${SIMULATOR_VERSION} ]]; then
				echo "ERROR : Invalid parameters. Version is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		-l|--leg)
			LEG_FLAG_UP=1
			HOST_LEG_PARAMETER=$2
			if [[ -z ${HOST_LEG_PARAMETER} ]]; then
				echo "ERROR : Invalid parameters. Host leg is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! "
			show_help
			exit 1
	esac
done


if [[ -z ${ACTION} ]]; then
	echo "ERROR : Invalid parameters. Mandatory Parameter '--action' is missing "
	exit 2
fi

if [ ! ${ACTION} == "build" ] && [ ! ${ACTION} == "start" ] && [ ! ${ACTION} == "stop" ] && [ ! ${ACTION} == "clean" ] && [ ! ${ACTION} == "timesync" ] && [ ! ${ACTION} == "upgrade" ]; then
	echo "ERROR : Invalid parameters. Mandatory parameter '--action' value '${ACTION}' is not valid. "
	exit 2
fi

if [ ${ADDITIONAL_IDG_FLAG_UP} -eq 1 ] && [ ! ${ACTION} == "build" ] ; then
	echo "ERROR : Invalid parameters. Optional parameter '-i' is valid only for the 'build' action. "
	exit 2
fi

if [ ${SPLUNK_FLAG_UP} -eq 1 ] && [ ! ${ACTION} == "build" ] ; then
	echo "ERROR : Invalid parameters. Optional parameter '-k' is valid only for the 'build' action. "
	exit 2
fi

if [[ ! -z ${SIMULATOR_VERSION} ]]; then
	if [ ! ${SIMULATOR_VERSION} == "stable" ] && [ ! ${SIMULATOR_VERSION} == "beta" ]; then
		echo "ERROR : Invalid parameters. Mandatory parameter '--version' value '${SIMULATOR_VERSION}' is not valid. "
		exit 2
	fi
fi

if [[ ! ${ACTUAL_SIMULATOR_VERSION} == ${SIMULATOR_VERSION} ]]; then
	echo "ERROR: Wrong version of simulator activated. Abort."
	exit 2
fi


############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}
mkdir -p ${RESOURCES_DIR}/centos
mkdir -p ${RESOURCES_DIR}/datapower
mkdir -p ${RESOURCES_DIR}/dpod
mkdir -p ${RESOURCES_DIR}/splunk

if [ ${ACTION} == "start" ] || [ ${ACTION} == "stop" ] || [ ${ACTION} == "timesync" ] || [ ${ACTION} == "clean" ] ; then
	if [[ ! -f ${USER_INPUT_FILE} ]]; then
		error "DPODSimulator was never built on this host. "
		error "To start the DPODSimulator, please run a 'build' action."
		echo "`current_date_time`: ERROR: DPODSimulator was never built on this host. "
		echo "`current_date_time`: ERROR: To start the DPODSimulator, please run a 'build' action."
		ABORT=1
	else
		read_user_input_from_file
	fi
elif [[ ${ACTION} == "build" ]]; then
	set_num_of_idg_services_to_handle
fi


export SIMULATOR_VERSION

# This is because the default value (60) is sometimes not enougth for 'docker-compose down' command and it times-out.
export COMPOSE_HTTP_TIMEOUT=300

if [ ${HTTPS_PROXY_FLAG_UP} -eq 1 ] || [ ${HTTP_PROXY_FLAG_UP} -eq 1 ] ; then
	export CENTOS_DOCKERFILE="Dockerfile_withProxy"
	export HTTP_PROXY_URL=${HTTPS_PROXY}
	export HTTPS_PROXY_URL=${HTTPS_PROXY}
else
	export CENTOS_DOCKERFILE="Dockerfile"
fi


if [[ ${ABORT} -eq 0 ]]; then

	if [ ${SPLUNK_FLAG_UP} -eq 1 ] && [ ${ADDITIONAL_IDG_FLAG_UP} -eq 0 ] ; then
		export SPLUNK_DEPENDENCY="centos"
		DOCKER_SERVICE_TO_START=splunk
	elif [ ${SPLUNK_FLAG_UP} -eq 0 ] && [ ${ADDITIONAL_IDG_FLAG_UP} -eq 1 ] ; then
		export SPLUNK_DEPENDENCY="idg2"
		DOCKER_SERVICE_TO_START=idg2
	elif [ ${SPLUNK_FLAG_UP} -eq 1 ] && [ ${ADDITIONAL_IDG_FLAG_UP} -eq 1 ] ; then
		export SPLUNK_DEPENDENCY="idg2"
		DOCKER_SERVICE_TO_START=splunk
	else
		export SPLUNK_DEPENDENCY="idg2"
		DOCKER_SERVICE_TO_START=centos
	fi

	discover_os_type
	info "Operting System is ${OS}"
	info "Distribution is: ${DIST}"
	echo "`current_date_time`: INFO: Operting System is: ${OS}"
	echo "`current_date_time`: INFO: Distribution is: ${DIST}"

	if [[ "mac" = "${OS}" ]]; then
		TIME_ZONE=`ls -l /etc/localtime | awk -F'zoneinfo/' '{print $2}'`
	else
		TIME_ZONE=`timedatectl | grep -i "time zone" | awk '{print $3}'`
	fi

	info "Server Time Zone is: ${TIME_ZONE}"
	echo "`current_date_time`: INFO: Server Time Zone is: ${TIME_ZONE}"

	# if action is 'build'
	if [[ ${ACTION} == "build" ]]; then
		build_simulator
	fi

	# if action is 'start'
	if [[ ${ACTION} == "start" ]]; then
		start_simulator
	fi

	# if action is 'stop'
	if [[ ${ACTION} == "stop" ]]; then
		stop_simulator
	fi

	# if action is 'clean'
	if [[ ${ACTION} == "clean" ]]; then
		clean_simulator
	fi
	
	# if action is 'timesync'
	if [[ ${ACTION} == "timesync" ]]; then
		set_system_time_from_hw_clock
	fi

	# if action is 'upgrade'
	if [[ ${ACTION} == "upgrade" ]]; then
		upgrade_simulator
	fi
fi


if [[ ! ${ACTION} == "upgrade" ]]; then
	if [[ ${ABORT} -eq 0 ]]; then
		if [[ ${ADD_IDG_TO_DPOD_RC} -eq 0 ]]; then
			info "Action completed successfully ! "
			echo -e "`current_date_time`: INFO: ${GREEN}Action completed successfully !${NC} See detailed log file: ${LOG_FILE} "
		else
			info "Action completed successfully, with warnings ! "
			echo -e "`current_date_time`: INFO: ${YELLOW}Action completed successfully, with warnings !${NC} See detailed log file: ${LOG_FILE} "
		fi
	else
		error "Action Failed. "
		echo -e "`current_date_time`: ERROR: ${RED}Action failed !${NC} See detailed log file: ${LOG_FILE} "
	fi
fi

exit ${ABORT}
