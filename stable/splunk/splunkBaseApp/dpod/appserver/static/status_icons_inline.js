require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/simplexml/ready!'
], function(_, $, mvc, TableView) {
    var CustomIconRenderer = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            return cell.field === 'Status';
        },
        render: function($td, cell) {
            var Status = cell.value.trim();
            // Compute the icon based on the field value
            var icon;
            if(Status == "Error") {
                icon = 'alert-circle';
            } else if(Status == "OK"){
                icon = 'check-circle';
            } else {
                icon = 'question-circle'
            }
            // Create the icon element and add it to the table cell
            //$td.addClass('icon-inline').html(_.template('<i class="icon-<%-icon%>"> <%- text %></i>', {
            $td.addClass('icon-inline').html(_.template('<i class="icon-<%-icon%>"><span style="color:black;"> <%- text %></span></i>', {
              icon: icon,
              text: cell.value
            }));
        }
    });
    mvc.Components.get('trx').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new CustomIconRenderer());
    });
});
