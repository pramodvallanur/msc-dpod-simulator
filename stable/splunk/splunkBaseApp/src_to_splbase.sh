#!/bin/bash
# Script creates dpod.tar.gz for uploading to Splunkbase
# NB! Changes that were made by user via Splunk WebUI are not included

LOCAL_FILES_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
SRC_DIR="${LOCAL_FILES_DIR}/../initialConfig_splunk/dpod.app"
DST_DIR="${LOCAL_FILES_DIR}"

rpm -q dos2unix || echo -e "\n You do not have dos2unix package installed\n It will be required for converting Windows-style EOL's in files (if such found) into Unix\n You may proceed, if you are sure that all files in $SRC_DIR were created/edited with Unix-style EOLs" 

if [ ! -d "$DST_DIR"/dpod ]; then 
	mkdir "$DST_DIR"/dpod
fi

rsync -av --progress "$SRC_DIR"/ "$DST_DIR"/dpod/ --exclude local --exclude lookups/* --exclude metadata/local.meta --exclude bin

find "$DST_DIR/dpod" -type d -exec chmod 755 {} \;
find "$DST_DIR/dpod" -type f -exec chmod 644 {} \;
find . -type f -exec file {} \; | grep "with CRLF line terminators"  |  awk -F":" '{print $1}' | xargs dos2unix

tar -zcvf /tmp/dpod.tar.gz -C "$DST_DIR" dpod
#tar -zcvf /tmp/dpod.tar.gz -C /home/montier/montier/git/msc-dpod-simulator/stable/splunk/splunkBaseApp/ dpod
echo "splunk app file is located at /tmp/dpod.tar.gz"