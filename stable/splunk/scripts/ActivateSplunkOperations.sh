#!/bin/bash


#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0
											
################################################################################################################# 


#############
# functions #
#############

function current_date_time() {
	echo `date +%F_%H-%M-%S`
}

function log()
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}
		
	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug()
{
	log DEBUG $@
}

function info()
{
	log INFO $@
}

function warn()
{
	log WARN $@
}

function error()
{
	log ERROR $@
}

function critic()
{
	log CRITIC $@
}

function show_help() {
	echo " "
	echo $"Usage: $0 -l|--loop-restart -c|--clean-data "
	echo "	Or :  $0 -h|--help"
	echo ""
	echo "-l|--loop-restart			: The number of seconds to sleep between restarts."
	echo "									This is an optional parameter. "
	echo "                                  If this parameter is not raised, a one-time restart will be performed. If raised, the restart will accur in an endless loop."
	echo "-c|--clean-data			: Clean all data. This is an optional flag."
	echo "-h| --help                : Display help                      "	
	echo " "
	echo "For Example: "
	echo "		$0 -c"
	echo "Or: 	$0 -l 86400 -c"
	echo "Or: 	$0"
	echo " "
	return 0
}

###########################
# variables and constants #
###########################

NOW=$( date +"%F_%H-%M-%S")

SCRIPTS_DIR="/MonTier/scripts"

LOG_DIR="${SCRIPTS_DIR}/logs"
LOG_FILE_NAME="ActivateSplunkOperations_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

CLEAN_DATA_FLAG_UP=0
LOOP_RESTART_FLAG_UP=0
RESTART_ARGS=""

ABORT=0

#############
# 	Main	#
#############

######## 	process parameters  	#########

while [[ -n "$1" ]]; do
	case "$1" in
		-h|--help)
			show_help
			exit 0
			;;
		-c|--clean-data)
			CLEAN_DATA_FLAG_UP=1
			shift 1
			;;
		-l|--loop-restart)
			LOOP_RESTART_FLAG_UP=1
			TIME_TO_SLEEP_BETWEEN_RESTARTS=$2
			if [[ -z ${TIME_TO_SLEEP_BETWEEN_RESTARTS} ]]; then
				echo "ERROR : Invalid parameters. sleep duration is missing"
				show_help
				exit 1
			fi
			shift 2
			;;
		*)
			echo "ERROR : Invalid parameter : $1 ! " 	
			show_help	
			exit 1
	esac
done



############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}

if [[ ${CLEAN_DATA_FLAG_UP} -eq 1 ]]; then
	RESTART_ARGS="${RESTART_ARGS} -c"
fi

if [[ ${LOOP_RESTART_FLAG_UP} -eq 1 ]]; then
	RESTART_ARGS="${RESTART_ARGS} -l ${TIME_TO_SLEEP_BETWEEN_RESTARTS}"
fi

info "Activating splunk restart looper"
echo "`current_date_time`: INFO: Activating splunk restart looper"
cd ${SCRIPTS_DIR}/RestartSplunk
nohup ./RestartSplunk.sh ${RESTART_ARGS} > "${LOG_DIR}/RestartSplunk_${NOW}.log" 2>&1 &
if [ $? -ne 0 ]; then 
	error "Failed to activating splunk restart looper. Abort"
	echo "`current_date_time`: ERROR: Failed to activating splunk restart looper. Abort"
	ABORT=1
fi

exit ${ABORT}