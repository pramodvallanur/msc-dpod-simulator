#!/bin/bash

#################################################################################################################

##  Copyright (c) 2015 by MonTier Software (2015) LTD.  All rights reserved.
##
##  Verbatim copies of this source file may be used and
##  distributed without restriction.
##
##  This source file is free software; you can redistribute it
##  and/or modify it under the terms of the ARTISTIC License
##  as published by The Perl Foundation; either version 2.0 of
##  the License, or (at your option) any later version.
##
##  This source is distributed in the hope that it will be
##  useful, but WITHOUT ANY WARRANTY; without even the implied
##  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE. See the Artistic License for details.
##
##  You should have received a copy of the license with this source.
##  If not download it from,
##     http://www.perlfoundation.org/artistic_license_2_0

#################################################################################################################

#############
# functions #
#############

function current_date_time {
	echo `date +%F_%H-%M-%S`
}

function log
{
	# Invoke an error for invalid input params
	if [ "$#" -lt 2 ]; then
		echo $"Usage: $FUNCNAME <priority> <message>" 1>&2
		return 9
	fi

	local priority=$1
	local message=${@:2}

	echo  "`current_date_time`: ${priority} ${message}" >> ${LOG_FILE}
}

function debug
{
	log DEBUG $@
}

function info
{
	log INFO $@
}

function warn
{
	log WARN $@
}

function error
{
	log ERROR $@
}

function critic
{
	log CRITIC $@
}
LOCAL_FILES_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
function show_help {
	echo $"Usage: $0 [-v|--version (version)] -a|--action (action) [-i|--additional-idg] [-k|--add-splunk (host address)]"
	echo "	Or :  $0 -h|--help"
	echo " "
	echo "-v|--version   : The DPODSimulator version to activate (stable/beta). This is an option parameter. "
	echo "					stable - This is the default value. Activate the DPODSimulator stable release"
	echo "					beta - Activate the DPODSimulator beta release"
	echo "-a|--action    : The action to take (build/stop/start/clean/upgrade). This is a mandatory parameter. "
	echo "                  build - Build simulator"
	echo "                  stop - Stop simulator"
	echo "                  start - Start simulator"
	echo "                  clean - Clean all simulator containers and images"
	echo "					upgrade - Get the latest version of the dpodSimulator from the web"
	echo "-i|--additional-idg    : If this flag if raised, a second idg will be created. "
	echo "								This is an optional parameter. Parameter is valid only for build/start actions."
	echo "-k|--add-splunk    	 : If this flag if raised, a splunk container will be created. This flag should be given with the address of the host on which this script is activated."
	echo "								This is an optional parameter. Parameter is valid only for build/start actions."
	echo "-h|--help      : Display help                      "
	echo " "
	echo "	For Example: "
	echo "			${LOCAL_FILES_DIR} -a build -i -k 172.17.100.142"
	echo "	Or : "
	echo "			${LOCAL_FILES_DIR} -a stop"
	return 0
}

###########################
# variables and constants #
###########################


NOW=$( date +"%F_%H-%M-%S")

#LOCAL_FILES_DIR=`pwd`

RESOURCES_DIR=`echo ${LOCAL_FILES_DIR} | sed "s/\/src\/msc-dpod-simulator/\/resources/g"`

LOG_DIR="${RESOURCES_DIR}/logs"
LOG_FILE_NAME="dpodSimulator_${NOW}.log"
LOG_FILE=${LOG_DIR}/${LOG_FILE_NAME}

DPOD_SIMULATOR_SCRIPT="dpodSimulator.sh"
SIMULATOR_VERSION="stable"


#############
# 	Main	#
#############

######## 	process parameters  	#########

PARAMS=$@

if [[ ${PARAMS} == *"-v beta"* ]]; then
	SIMULATOR_VERSION="beta"
fi


############# 	Main Logic   ##############

mkdir -p ${LOG_DIR}

if [[ ${SIMULATOR_VERSION} == "beta" ]]; then
	DPOD_SIMULATOR_DIR="${LOCAL_FILES_DIR}/beta/scripts"
else
	DPOD_SIMULATOR_DIR="${LOCAL_FILES_DIR}/stable/scripts"
fi

cd ${DPOD_SIMULATOR_DIR}
chmod -R 777 ${DPOD_SIMULATOR_DIR}/*.sh

info "Activating ${SIMULATOR_VERSION} version of DPODSimulator. "
echo "`current_date_time`: INFO: Activating ${SIMULATOR_VERSION} version of DPODSimulator. Log file is: ${LOG_FILE}"
./${DPOD_SIMULATOR_SCRIPT} ${PARAMS} | tee ${LOG_FILE}
